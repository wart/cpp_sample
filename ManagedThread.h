// -----------------------------------------------------------------
#pragma once
// -----------------------------------------------------------------
#include "runtimer.h"

#include <lcommonA.h>
#include <MP.h>
#include <st_darr.hpp>
#include <GC.hpp>
#include "parcing/_ident.h"
#include <functional>
// -----------------------------------------------------------------
	#define ManagedThreadLockByRef(ref)	MP::MutexLock mtlbr(ref->lock);
	#define ManagedThreadLockSelf		MP::MutexLock mtls(lock);
	#define ManagedThreadSelfLock_Enter	lock.enter();
	#define ManagedThreadSelfLock_Leave	lock.leave();

// -----------------------------------------------------------------
namespace Managed
{
	// -------------------------------------------------------------
	class Thread:protected MP::Thread
	{
		// ========================================================= GC PART
	public:
		typedef Parcing::GCTempl::ReferenceTemplate<Thread,Parcing::_ident>	Reference;
//		typedef Base::int_16u  (__stdcall	UpdatedStatusProcedure)(Base::int_16u tid, Base::int_16u clas,Base::int_16u st,Base::pchar_8 desc);
//		typedef Base::int_16u  (__stdcall	UpdatedNameProcedure  )(Base::int_16u tid, Base::int_16u clas,Base::pchar_8 nam);
		typedef std::function<PROCEDURE(Base::int_16u tid, DWORD parentid,Base::int_16u clas, Base::int_16u st, Base::pchar_8 desc)>	UpdatedStatusProcedure;
		typedef std::function<PROCEDURE(Base::int_16u tid, Base::int_16u clas, Base::pchar_8 nam)> UpdatedNameProcedure;
		//=======================================================
		static		PROCEDURE			Init();
		static		PROCEDURE			Done();
		friend class Reference;
	private:
		//--------------------------------------------
					Base::int_32u		RefCnt;
		//--------------------------------------------
					Base::int_16u		GetRefCnt		();
					Base::int_16u		AddRef			();
					Base::int_16u		Release			();
		// ========================================================= MANAGER PART
	private:
		//--------------------------------------------
		static		Reference::Array&	GetThreadArray	();
		// ========================================================= SINGLE THREAD PART
					MP::Mutex			lock;
		// ---------------------------------------------------------
//		static		UpdatedStatusProcedure*	&getUpdatedStatusProcedure	();
//		static		UpdatedNameProcedure*	&getUpdatedNameProcedure	();
					static		PROCEDURE CallUpdatedStatusProcedure(Base::int_16u tid, DWORD parentid, Base::int_16u clas, Base::int_16u st, Base::pchar_8 desc);
		static		PROCEDURE CallUpdatedNameProcedure(Base::int_16u tid, Base::int_16u clas, Base::pchar_8 nam);
		// ---------------------------------------------------------
					PROCEDURE			PreStart_initVars();
	protected:
		#define							ThreadClass_Abstract		0x0000	
		#define							ThreadClass_ContextThread	0x0001
//		#define							ThreadClass_SFIFODIspatch	0x0002
					Base::int_16u		ThreadClass;
		volatile	Base::pchar_8		name;
		// ---------------------------------------------------------
		volatile	Base::int_32		SuspendCount;
		volatile	Base::int_32		SleepTime;
		// ---------------------------------------------------------
					DWORD				parentId;
		volatile	DWORD				waitingId;
		volatile	Base::int_mem		ActiveChilds;
		volatile	Base::int_mem		ActiveWaiters;
		#define							MaxWaitEventCount				0x30
		volatile	MP::Event*			waitEventptr[MaxWaitEventCount+1];	// +1 for killevent
		volatile	Base::int_mem		waitEventTime;
		volatile	Base::int_8u		waitEventCount;
		volatile	Base::pchar_8		waitEventDesc;
					MP::Event			killEvent;
//					MP::Event			ActionEvent;
//					Base::int_mem		ActionEventCode;
					MP::Mutex*			TryLockMutex;
		// ---------------------------------------------------------
					Base::int_mem		ManagedFlags;
		#define							ManagedFlag_WaitingChilds		0x0001
		#define			                ManagedFlag_ONAIR_SETS          0x0001
		#define							ManagedFlag_LogTimeStart		0x0002
		#define							ManagedFlag_AllowPostStatus		0x0004
										//   may use for other jobs free flagbits
					Base::int_bool		setManagedFlags (Base::int_mem mask,Base::int_mem val);
		// ---------------------------------------------------------
					PROCEDURE			RemoveFromPublic();
		// ---------------------------------------------------------
		virtual		PROCEDURE			print_info		()=0;
		virtual		PROCEDURE			code			()=0;
		virtual		PROCEDURE			OnThreadBegin	();
		virtual		PROCEDURE			OnThreadEnd		();
		virtual		PROCEDURE			OnHandleWaitEventResult(Base::int_32u res);
		// ---------------------------------------------------------
					CONSTRUCTOR			Thread			();
		virtual		DESTRUCTOR			~Thread			();
		// ---------------------------------------------------------
					Base::int_bool		SelfCall		();
		// ---------------------------------------------------------

		#define							DEADLOCK_SEARCH_DEPTH			0x0100
		#define							SLEEPS_BEFORE_DEADLOCK_SEARCH	0x0200

		static		Base::int_bool		isDeadLocked	(DWORD id,MP::Mutex * mut);	
					Base::int_bool		TryLock(MP::Mutex &mut,Base::pchar_8 desc);
		// ---------------------------------------------------------
					PROCEDURE			handle_sleep	(); /* On Job  */
					PROCEDURE			handle_suspend	(); /* On Job  */
					PROCEDURE			handle_parent	();	/* On Exit */
					PROCEDURE			handle_childs	();	/* On Job  */
					PROCEDURE			handle_waiters	();	/* On Exit */
					PROCEDURE			handle_wait		(); /* On Job  */
					PROCEDURE			handle_waitEvent(); /* On Job  */
		// ---------------------------------------------------------
					PROCEDURE			SetParented		();
		// ---------------------------------------------------------
					Base::int_bool		IsAlive			();
					Base::int_bool		IsManagedRunning();							//  UnSafe !!! call only when locked
					PROCEDURE			setManagedState(Base::int_8u ns,Base::pchar_8 desc=nullptr);//  UnSafe !!! call only when locked
					Base::int_8u		Managed_state;
		#define							Managed_state_Burn			0x00
		#define							Managed_state_Run			0x01
		#define							Managed_state_Sleep			0x02
		#define							Managed_state_Suspend		0x03
		#define							Managed_state_wait			0x04
		#define							Managed_state_waitChilds	0x05
		#define							Managed_state_waitEvent		0x06
		#define							Managed_state_TryLock		0x07
		#define							Managed_state_Finishing		0x08
		// ---------------------------------------------------------
					PROCEDURE			OnFinishing		();
					Base::int_bool		HandleEvents	();				// when true no signal to stop
		virtual		PROCEDURE			HandleOthers	();
		virtual		PROCEDURE			OnKill			();
		// ---------------------------------------------------------
					Base::int_32u       waitManyEventInPlace(MP::Event **ev, Base::int_8u cnt, DWORD msec,Base::pchar_8 desc);//=INFINITE
	public:
		typedef		Reference			Ref;
		// ---------------------------------------------------------
		virtual		Base::int_bool		GetChild		(Parcing::_ident &arg, Base::int_32u md, GCObjRef &ret);
		virtual		Base::int_bool		GetChildNS		(GCObjRef &arg, Base::int_32u	md, GCObjRef &ret);
		virtual		PROCEDURE			OperateSetId	(Parcing::_ident &trg, Base::int_32u md, GCObjRef &right, GCObjRef &ret);
		virtual		PROCEDURE			OperateSetNS	(GCObjRef &trg, Base::int_32u md, GCObjRef &right, GCObjRef &ret);
		// ---------------------------------------------------------
		static		PROCEDURE			AddUpdateHandlers(Parcing::_ident name,UpdatedStatusProcedure usp, UpdatedNameProcedure unp);
		static		PROCEDURE			RemoveUpdateHandlers(Parcing::_ident name);
		// ---------------------------------------------------------
					Base::int_bool		IsAliveWithClass(Base::int_16u _class);
					DWORD				GetId			();
					HANDLE				GetHandle		();
					Base::int_16u		GetThreadClass  ();
					Base::int_8u		getState		();
		// ---------------------------------------------------------
//					PROCEDURE			SetActionEvent	(Base::int_mem cod);
//					PROCEDURE			GoWaitActionEvent(Base::int_mem cod,DWORD msec,Base::pchar_8 desc);
		// ---------------------------------------------------------
					Base::int_mem		getManagedFlags ();
		// ---------------------------------------------------------
		static		PROCEDURE			Sleep			(DWORD msec);
		static		PROCEDURE			Suspend			(DWORD id);
		static		PROCEDURE			Resume			(DWORD id);
					PROCEDURE			ManagedResume	();
		static		PROCEDURE			Kill			(DWORD id);	
					PROCEDURE			Kill			();	
		// ---------------------------------------------------------
					PROCEDURE			waitchilds		();
					PROCEDURE			wait			(DWORD id);
					PROCEDURE			CancelwaitEvent ();  //for not started waiting
					PROCEDURE			waitEvent		(MP::Event &ev,DWORD msec,Base::pchar_8 desc);//=INFINITE
					Base::int_bool		waitManyEvent	(MP::Event **ev, Base::int_8u cnt, DWORD msec,Base::pchar_8 desc);//=INFINITE
					
		// ---------------------------------------------------------
		static		Reference			getMe			();
		static		Reference			getById			(DWORD id);
		static		Reference			getByHandle		(HANDLE hnd);

		static		Base::int_32u       SelfwaitManyEventInPlace(MP::Event **ev, Base::int_8u cnt, DWORD msec,Base::pchar_8 desc);
		// ---------------------------------------------------------
		static		Base::int_bool		imAlive			();
		static		PROCEDURE			print_info		(DWORD id);
		// ---------------------------------------------------------
		static		PROCEDURE			setName			(DWORD id,Base::pchar_8 newname);
					PROCEDURE			Rename			(Base::pchar_8 newname);
		virtual		Base::int_bool		getUsr			(GCObjRef &ret);
					PROCEDURE			getName			(GCObjRef &ret);
					PROCEDURE			getState		(GCObjRef &ret);
					PROCEDURE			getStateName	(GCObjRef &ret);
					PROCEDURE			getParent		(GCObjRef &ret);
					PROCEDURE			getChilds		(GCObjRef &ret);
					PROCEDURE			getWaiters		(GCObjRef &ret);
					PROCEDURE			GetThreadClass	(GCObjRef &ret);
		// ---------------------------------------------------------
	};
	// -------------------------------------------------------------
};
// -----------------------------------------------------------------
