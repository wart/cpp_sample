// ==================================================================================
#include "_operator.h"
// ==================================================================================
#include "_lexem.h"
#include "TBasket.h"
#include "error_code.h"
#include "TCharParce.h"
#include "TLexManager.h"
#include "_expression.h"
#include "_vglink.h"
#include "_headArgList.h"
// ==================================================================================
INIT_OBJ_COUNTER(Parcing::CItem::_operator);
INIT_OBJ_COUNTER(Parcing::CItem::_operator_block);
INIT_OBJ_COUNTER(Parcing::CItem::_operator_if);
INIT_OBJ_COUNTER(Parcing::CItem::_operator_for);
INIT_OBJ_COUNTER(Parcing::CItem::_operator_foreach);
INIT_OBJ_COUNTER(Parcing::CItem::_operator_while);
INIT_OBJ_COUNTER(Parcing::CItem::_operator_try);
INIT_OBJ_COUNTER(Parcing::CItem::_operator_switch);
INIT_OBJ_COUNTER(Parcing::CItem::_operator_switch::switch_case);
INIT_OBJ_COUNTER(Parcing::CItem::_operator_syncro);
INIT_OBJ_COUNTER(Parcing::CItem::_operator_var);
// ==================================================================================
namespace Parcing
{
	// ==============================================================================
	namespace CItem
	{
		// ==========================================================================
		CONSTRUCTOR			header_operator::header_operator():type(CJ_OPR_Null),_BreakPointInfo(nullptr)
		{

		};
		// ==========================================================================
		DESTRUCTOR			header_operator::~header_operator()
		{
			if(_BreakPointInfo)
			{
				delete _BreakPointInfo;
				_BreakPointInfo=nullptr;
			};
		};
		// ==========================================================================
		PROCEDURE			header_operator::keep_as_text(TBasket &b)
		{
			if(_BreakPointInfo)
				if (b.GetOpts() & texk_keep_show_putBreakPoints)
					_BreakPointInfo->keep_as_text(b);
		};
		// ==========================================================================
		operator_type header_operator::TYPE()
		{ 
			return type;
		};
		// ==========================================================================
		CONSTRUCTOR _operator::_operator():_data(nullptr)
		{
			OBJ_COUNTER_INC;
		};
		// ==========================================================================
		DESTRUCTOR  _operator::~_operator()
		{
			switch (type & CJ_OPR_MASK){
						case CJ_OPR_Null    :
						case CJ_OPR_break   :
						case CJ_OPR_continue:break;
						case CJ_OPR_return  :
						case CJ_OPR_exit    :
						case CJ_OPR_Throw   :
						case CJ_OPR_yield   :
						case CJ_OPR_Expr    :if (_data){delete ((_expression*)_data);};
			};
			_data=nullptr;
			OBJ_COUNTER_DEC;
		};
		// ==========================================================================
		PROCEDURE   _operator::_detach()
		{
			type=CJ_OPR_Null;
			_data=nullptr;
		};
		// ==========================================================================
		PROCEDURE	_operator		::keep_as_text	(TBasket &b)
		{
			header_operator::keep_as_text(b);
			Base::int_bool rte=0;
			switch (type & CJ_OPR_MASK)
			{
				case CJ_OPR_Null      :
					b.putLex(CJ_LEX_opr_bs ,0,"{",0);
					b.putLex(CJ_LEX_opr_be ,0,"}",0);
					break;
				case CJ_OPR_return   :b.putLex(CJ_LEX_whiteword3, CJ_OPR_return,CJ_OPRN_return,0);rte=1;break;
				case CJ_OPR_Throw    :b.putLex(CJ_LEX_whiteword3, CJ_OPR_Throw,CJ_OPRN_throw  ,0);rte=1;break;
				case CJ_OPR_exit     :b.putLex(CJ_LEX_whiteword3, CJ_OPR_exit,CJ_OPRN_exit    ,0);rte=1;break;
				case CJ_OPR_yield    :b.putLex(CJ_LEX_whiteword3, CJ_OPR_yield,CJ_OPRN_yield   ,0);rte=1;break;

				case CJ_OPR_break    :
					b.putLex(CJ_LEX_whiteword3, CJ_OPR_break,CJ_OPRN_break,0);
					b.add(' ');
					break;
				case CJ_OPR_continue  :
					b.putLex(CJ_LEX_whiteword3, CJ_OPR_continue,CJ_OPRN_continue,0);
					b.add(' ');
					break;
				case CJ_OPR_Expr      :
					if (_data){_data->keep_as_text(b);} else {COMMS;	b.printf("O_o must be expression");COMME;};
					break;
				default:{
					COMMS;
					b.printf("unknown operator");
					COMME;
					};
			};
			if(rte)
			{
				b.add(' ');
				if(type&CJ_OPR_STRUC_ALT)
				{
					b.putLex(CJ_LEX_arg_bs,0,"(",0);
				}
				else
					b.add(' ');
				if (_data)
				{
					_data->keep_as_text(b);
				}
				else
					if(type&CJ_OPR_STRUC_ALT)
					{
						COMMS;
						b.printf("O_o must be argument");
						COMME;
					};
				if(type&CJ_OPR_STRUC_ALT)
				{
					b.putLex(CJ_LEX_arg_be,0,")",0);
				};
			};
		};
		// ==========================================================================
		PROCEDURE	_operator		::keep_as_textSUB		(TBasket &b,_operator* o)
		{
			if(o)
			{
				if ((o->TYPE()& CJ_OPR_MASK)!=CJ_OPR_Block)
					b.incTab();
				b.putLn();
				_operator::keep_as_text(b,o);
				if ((o->TYPE()& CJ_OPR_MASK)!=CJ_OPR_Block)
					b.decTab();
			};
		};
		// ==========================================================================
		PROCEDURE   _operator::_clear(_operator* &o)
		{
			if (o)
			{
				switch (o->type & CJ_OPR_MASK)
				{
					case CJ_OPR_Null      :;
					case CJ_OPR_return    :;
					case CJ_OPR_exit      :;
					case CJ_OPR_break     :;
					case CJ_OPR_continue  :;
					case CJ_OPR_Throw	  :;
					case CJ_OPR_yield     :;
					case CJ_OPR_Expr      :delete                  (o) ;break;
					case CJ_OPR_Block     :delete ((_operator_block*)o);break;
					case CJ_OPR_If        :delete ((_operator_if   *)o);break;
					case CJ_OPR_While     :delete ((_operator_while*)o);break;
					case CJ_OPR_Do        :delete ((_operator_while*)o);break;
					case CJ_OPR_For       :delete ((_operator_for  *)o);break;
					case CJ_OPR_Foreach   :delete ((_operator_foreach*)o);break;
					case CJ_OPR_Try		  :delete ((_operator_try	*)o);break;
					case CJ_OPR_switch	  :delete ((_operator_switch*)o);break;
					case CJ_OPR_Syncro	  :delete ((_operator_syncro*)o);break;
					case CJ_OPR_var  	  :delete ((_operator_var*)o);break;
						
				};
			};
			o=nullptr;
		};
		// ==========================================================================
		 PROCEDURE	_operator::ResetFC				(_operator* o)
		 {
			if (o)
			{
				switch (o->type & CJ_OPR_MASK)
				{
				case CJ_OPR_Null      :;
				case CJ_OPR_return    :;
				case CJ_OPR_exit      :;
				case CJ_OPR_break     :;
				case CJ_OPR_continue  :;
				case CJ_OPR_Throw	  :;
				case CJ_OPR_yield     :;
				case CJ_OPR_Expr      :_expression::ResetFC(o->_data);break;
				case CJ_OPR_Block     :
										if(((_operator_block  *)o)->type & CJ_OPR_BLK_ONCE)
										{
											((_operator_block  *)o)->type &= ~ CJ_OPR_BLK_ONCEPASS;
										}else
											if(((_operator_block  *)o)->DATA && ((_operator_block  *)o)->ACOUNT)
												for (Base::int_mem i=0;i<((_operator_block  *)o)->ACOUNT;i++)
													ResetFC(((_operator_block  *)o)->DATA[i]);
										break;
				case CJ_OPR_If        : _expression::ResetFC(((_operator_if      *)o)->expr);
													 ResetFC(((_operator_if      *)o)->body);
													 ResetFC(((_operator_if      *)o)->elsbody);
										break;
				case CJ_OPR_While     : _expression::ResetFC(((_operator_while   *)o)->expr);
													 ResetFC(((_operator_while   *)o)->body);
										break;
				case CJ_OPR_Do        : _expression::ResetFC(((_operator_while   *)o)->expr);
													 ResetFC(((_operator_while   *)o)->body);
										break;
				case CJ_OPR_For       : _expression::ResetFC(((_operator_for     *)o)->expr);
													 ResetFC(((_operator_for     *)o)->body);
													 ResetFC(((_operator_for     *)o)->init);
													 ResetFC(((_operator_for     *)o)->turn);
										break;
				case CJ_OPR_Foreach   : _expression::ResetFC(((_operator_foreach *)o)->expr);
													 ResetFC(((_operator_foreach *)o)->body);
										break;
				case CJ_OPR_Try		  :				 ResetFC(((_operator_try     *)o)->body);
													 ResetFC(((_operator_try     *)o)->_catch);
										break;
				case CJ_OPR_switch	  : _expression::ResetFC(((_operator_switch *)o)->_arg);
													 ResetFC(((_operator_switch *)o)->_default);
											if(((_operator_switch  *)o)->DATA && ((_operator_switch  *)o)->ACOUNT)
												for (Base::int_mem i=0;i<((_operator_switch *)o)->ACOUNT;i++)
													ResetFC(((_operator_switch  *)o)->DATA[i]->oper);
										break;
				case CJ_OPR_Syncro	  :	ResetFC(((_operator_syncro *)o)->body);
										break;
				case CJ_OPR_var		  : if(((_operator_var*)o)->_decls) ((_operator_var*)o)->_decls->ResetFC();
										break;
				};
			};
		 };
		// ==========================================================================
		Base::int_bool   _operator::HaveSyncBlockMode(_operator* o)
		{
			if (o)
			{
				switch (o->type & CJ_OPR_MASK)
				{
				case CJ_OPR_Null      :;
				case CJ_OPR_return    :;
				case CJ_OPR_exit      :;
				case CJ_OPR_break     :;
				case CJ_OPR_continue  :;
				case CJ_OPR_Throw	  :;
				case CJ_OPR_yield     :;
				case CJ_OPR_Expr      :return _expression::HaveSyncBlockMode(o->_data);break;
				case CJ_OPR_Block     :return ((_operator_block  *)o)->HaveSyncBlockMode();break;
				case CJ_OPR_If        :return ((_operator_if     *)o)->HaveSyncBlockMode();break;
				case CJ_OPR_While     :return ((_operator_while  *)o)->HaveSyncBlockMode();break;
				case CJ_OPR_Do        :return ((_operator_while  *)o)->HaveSyncBlockMode();break;
				case CJ_OPR_For       :return ((_operator_for    *)o)->HaveSyncBlockMode();break;
				case CJ_OPR_Foreach   :return ((_operator_foreach*)o)->HaveSyncBlockMode();break;
				case CJ_OPR_Try		  :return ((_operator_try	 *)o)->HaveSyncBlockMode();break;
				case CJ_OPR_switch	  :return ((_operator_switch *)o)->HaveSyncBlockMode();break;
				case CJ_OPR_Syncro	  :return ((_operator_syncro *)o)->HaveSyncBlockMode();break;
				case CJ_OPR_var  	  :return  (((_operator_var*)o)->_decls)?((_operator_var*)o)->_decls->HaveSyncBlockMode():false;break;
				};
			};
			return false;
		};
		// ==========================================================================
		PROCEDURE						_operator::ApplyLetFunArgs		(_operator* src,_headArgList * ha)
		{
			if(!( src && ha)) return;
			switch (src->type & CJ_OPR_MASK)
			{
				case CJ_OPR_Null      :;
				case CJ_OPR_return    :;
				case CJ_OPR_exit      :;
				case CJ_OPR_break     :;
				case CJ_OPR_continue  :;
				case CJ_OPR_Throw	  :;
				case CJ_OPR_yield     :;
				case CJ_OPR_Expr      : _expression::ApplyLetFunArgs(src->_data,ha);break;
				case CJ_OPR_If        : ApplyLetFunArgs(((_operator_if    *)src)->body,ha);
										ApplyLetFunArgs(((_operator_if    *)src)->elsbody,ha);
										_expression::ApplyLetFunArgs(((_operator_if     *)src)->expr,ha);break;
				case CJ_OPR_While     :
				case CJ_OPR_Do        :
										ApplyLetFunArgs(((_operator_while *)src)->body,ha);
										_expression::ApplyLetFunArgs(((_operator_while  *)src)->expr,ha);break;
				case CJ_OPR_For       : ApplyLetFunArgs(((_operator_for   *)src)->init,ha);
										ApplyLetFunArgs(((_operator_for   *)src)->turn,ha);
										ApplyLetFunArgs(((_operator_for   *)src)->body,ha);
										_expression::ApplyLetFunArgs(((_operator_for    *)src)->expr,ha);break;
				case CJ_OPR_Try		  : ApplyLetFunArgs(((_operator_try   *)src)->body,ha);
										ApplyLetFunArgs(((_operator_try   *)src)->_catch,ha);break;
				case CJ_OPR_Syncro	  : ApplyLetFunArgs(((_operator_syncro*)src)->body,ha);	break;
				case CJ_OPR_Block     :((_operator_block  *)src)->ApplyLetFunArgs(ha);break;
				case CJ_OPR_Foreach   :((_operator_foreach*)src)->ApplyLetFunArgs(ha);break;
				case CJ_OPR_switch	  :((_operator_switch *)src)->ApplyLetFunArgs(ha);break;
				case CJ_OPR_var  	  :((_operator_var    *)src)->_decls->ApplyLetFunArgs_for_var_oper(ha);break;
			};
		};
		// ==========================================================================
		PROCEDURE		_operator_block::ApplyLetFunArgs		(_headArgList * ha)
		{
			auto haa=ha->clone(ha);
			if(haa && DATA && ACOUNT)
			{
				for(Base::int_mem i=0;i<ACOUNT;i++)
					_operator::ApplyLetFunArgs(DATA[i],haa);
				delete haa;
			};
		};
		// ==========================================================================
		PROCEDURE	_operator_foreach::ApplyLetFunArgs		(_headArgList * ha)
		{
			auto haa=ha->clone(ha);
			if(haa)
			{
				_expression::ApplyLetFunArgs(expr,haa);
				_operator::ApplyLetFunArgs(elsbody,haa);
				haa->Remove(storage);
				_operator::ApplyLetFunArgs(body,haa);
				delete haa;
			};
		};
		// ==========================================================================
		PROCEDURE   _operator_switch::ApplyLetFunArgs(_headArgList * ha)
		{
			_expression::ApplyLetFunArgs(_arg,ha);
			if(DATA && ACOUNT && ha)
				for(Base::int_mem i=0;i<ACOUNT;i++)
					if(DATA[i])
						_operator::ApplyLetFunArgs(DATA[i]->oper,ha);
			_operator::ApplyLetFunArgs(_default,ha);
		};
		// ==========================================================================
		PROCEDURE	_operator		::keep_as_text	(TBasket &b,_operator* o)
		{
			if (o){
				switch (o->type & CJ_OPR_MASK)
				{
					case CJ_OPR_Null      :;
					case CJ_OPR_return    :;
//					case CJ_OPR_eval      :;
					case CJ_OPR_exit      :;
					case CJ_OPR_break     :;
					case CJ_OPR_continue  :;
					case CJ_OPR_Throw	  :;
					case CJ_OPR_yield     :;
					case CJ_OPR_Expr      :					     o ->keep_as_text(b);break;
					case CJ_OPR_Block     :((_operator_block   *)o)->keep_as_text(b);break;
					case CJ_OPR_If        :((_operator_if      *)o)->keep_as_text(b);break;
					case CJ_OPR_While     :((_operator_while   *)o)->keep_as_text(b);break;
					case CJ_OPR_For       :((_operator_for     *)o)->keep_as_text(b);break;
					case CJ_OPR_Do        :((_operator_while   *)o)->keep_as_text(b);break;
					case CJ_OPR_switch    :((_operator_switch  *)o)->keep_as_text(b);break;
					case CJ_OPR_Foreach   :((_operator_foreach *)o)->keep_as_text(b);break;
					case CJ_OPR_Try		  :((_operator_try     *)o)->keep_as_text(b);break;
					case CJ_OPR_Syncro	  :((_operator_syncro  *)o)->keep_as_text(b);break;
					case CJ_OPR_var	      :((_operator_var     *)o)->keep_as_text(b);break;
				};
			}else{
				COMMS;
				b.printf("empty operator pointer");
				COMME;
			}
		};
		// ==========================================================================
		PROCEDURE	_operator		::build_lexem(TCharParce &_p, _lexem &_l,Base::int_bool no_expr)
		{
		# define GETGO first=(CItem::_operator*)_l._detachB();
		//.....................................
			_operator*  first = nullptr;
			Base::ERROR_code ok = ERROR_Done;
			TBuffer::TJ_Buff_position    lp =_l.getPos();
			CItem::_operator* MyNULL=nullptr;                     

			switch (_l.MAJOR()){
				case CJ_LEX_whiteword2://-------------------------------------
					switch (_l.MINOR()){
						//   MyNULL
						case CJ_OPR_If	  :
							CItem::_operator_if::build_lexem(_p,_l,MyNULL);
							if (_l.IsObj(CJ_LOBJ_operator))
							{
								GETGO;
							};
							break;
						case CJ_OPR_While :
							CItem::_operator_while::build_lexem(_p,_l,MyNULL);
							if (_l.IsObj(CJ_LOBJ_operator))
							{
								GETGO;
							};
							break;
						case CJ_OPR_Do :
							CItem::_operator_while::build_lexem_Do(_p,_l,MyNULL);
							if (_l.IsObj(CJ_LOBJ_operator))
							{
								GETGO;
							};
							break;
						case CJ_OPR_Foreach :
							CItem::_operator_foreach::build_lexem(_p,_l,MyNULL);
							if (_l.IsObj(CJ_LOBJ_operator))
							{
								GETGO;
							};
							break;
						case CJ_OPR_For   :
							CItem::_operator_for::build_lexem(_p,_l,MyNULL);
							if (_l.IsObj(CJ_LOBJ_operator))
							{
								GETGO;
							};
							break;
						case CJ_OPR_Try   :
							CItem::_operator_try::build_lexem(_p,_l);
							if (_l.IsObj(CJ_LOBJ_operator))
							{
								GETGO;
							};
							break;
						case CJ_OPR_switch:
							CItem::_operator_switch::build_lexem(_p,_l);
							if (_l.IsObj(CJ_LOBJ_operator))
							{
								GETGO;
							};
							break;
						case CJ_OPR_Syncro:
							_operator_syncro::build_lexem(_p,_l,MyNULL);
							if (_l.IsObj(CJ_LOBJ_operator))
								{
									GETGO;
								};
							break;
						case CJ_OPR_var:
							_operator_var::build_lexem(_p,_l);
							if (_l.IsObj(CJ_LOBJ_operator))
								{
									GETGO;
								};
							break;
	//todo				case CJ_OPR_using :break;
						default:
						ok=ERROR_OPERATOR_WrWW2;
					};


					break;

				case CJ_LEX_whiteword3://-------------------------------------

					if(first= new CItem::_operator())
					{
						switch (_l.MINOR()){
							case CJ_OPR_return  :
							case CJ_OPR_exit	:
							case CJ_OPR_Throw   :
							case CJ_OPR_yield   :
								{
									first->type=_l.MINOR();
									Base::int_8u bse=0;
									NEXT_LEX;
									_expression::build_lexem(_p,_l);
									if (_l.IsObj(CJ_LOBJ_exprsn))
									{
										first->_data=(CItem::_expression*)_l._detachB();
									}else
										if (first->type==CJ_OPR_Throw)
											ok=ERROR_OPERATOR_WrSmOpDt;
								};
								break;
							case CJ_OPR_break   ://--------------
							case CJ_OPR_continue:
								first->type=_l.MINOR();
								break;
							default:
							ok=ERROR_OPERATOR_WrWW3;
						};
					}else
					{
						ok=ERROR_OPERATOR_OOM1;
					};
					break;
				case CJ_LEX_opr_bs://-------------------------------------
					_operator_block::build_lexemB(_p,_l);
					if (_l.IsObj(CJ_LOBJ_operator))
					{
						GETGO;
					}
					else
					{
						ok=ERROR_OPERATOR_CMPOPR;
					};
					break;
				case CJ_LEX_whiteword4:
					if((_l.MINOR()==CJ_OPR_SUB_default)||(_l.MINOR()==CJ_OPR_SUB_case))
					{
						ok=ERROR_OPERATOR_case_found;
						_p.returnTo(_l);
					}else ok=ERROR_OPERATOR_ww4_found;
					break;
				default://-------------------------------------
					if(no_expr)
					{
						ok=ERROR_OPERATOR_ErExpNE;
					}else
					{
						CItem::_expression::build_lexem(_p,_l);
						if (_l.IsObj(CJ_LOBJ_exprsn)){
							if(first= new CItem::_operator())
							{
								first->type=CJ_OPR_Expr;
								first->_data=(CItem::_expression*)_l._detachB();
								first->_BreakPointInfo=_p.Detach_BreakPointInfo();
							}else{
								ok=ERROR_OPERATOR_OOM2;
							};
						}else{
							ok=ERROR_OPERATOR_ErExpG;
						};
					};
			};
			if (ok==ERROR_Done)
			{
				if (first)
				{
					NEXT_LEX;
					while((_l.MAJOR()==CJ_LEX_whiteword2)&&(ok==ERROR_Done))
					{
						switch (_l.MINOR()){
							case CJ_OPR_If	  :
								CItem::_operator_if::build_lexem(_p,_l,first);
								if (_l.IsObj(CJ_LOBJ_operator))
								{
									CItem::_operator::_clear(first);
									GETGO;
									NEXT_LEX;
								};
								break;
							case CJ_OPR_While :
								CItem::_operator_while::build_lexem(_p,_l,first);
								if (_l.IsObj(CJ_LOBJ_operator))
								{
									CItem::_operator::_clear(first);
									GETGO;
									NEXT_LEX;
								};
								break;

/*							case CJ_OPR_Do :
								CItem::_operator_while::build_lexem_Do(_p,_l,first);
								if (_l.IsObj(CJ_LOBJ_operator))
								{
									CItem::_operator::_clear(first);
									GETGO;
									NEXT_LEX;
								};
								break;/**/
							case CJ_OPR_For   :
								CItem::_operator_for::build_lexem(_p,_l,first);
								if (_l.IsObj(CJ_LOBJ_operator))
								{
									CItem::_operator::_clear(first);
									GETGO;
									NEXT_LEX;
								};
								break;
							case CJ_OPR_Foreach   :
								CItem::_operator_foreach::build_lexem(_p,_l,first);
								if (_l.IsObj(CJ_LOBJ_operator))
								{
									CItem::_operator::_clear(first);
									GETGO;
									NEXT_LEX;
								};
								break;
							case CJ_OPR_Syncro :
								CItem::_operator_syncro::build_lexem(_p,_l,first);
								if (_l.IsObj(CJ_LOBJ_operator))
								{
									CItem::_operator::_clear(first);
									GETGO;
									NEXT_LEX;
								};
								break;
							default:ok=ERROR_OPERATOR_NOTALTWW;   // ����� �� �������� ��������� �������� ��������� ���������,
									// � ����� ������ ������ ��� ��������
						};
					};
					_p.returnTo(_l);
				}
				else
				{
					ok=ERROR_OPERATOR_SMWR;
				};
			};

			if ((ok==ERROR_Done)&&(first))
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	CItem::_operator::_clear(first);

				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};

		//.....................................
		#undef		GETGO
		};
		// ==========================================================================
		CONSTRUCTOR     _operator_block::_operator_block(Base::int_32 ic):ACOUNT(0),TCOUNT(ic)
		{
			OBJ_COUNTER_INC;
			type=CJ_OPR_Block;
			if (ic>0)
			{
				DATA=(_operator**) MMMalloc(ic*sizeof(_operator*),1);
			}else
				DATA=nullptr;
		};
		// ==========================================================================
		DESTRUCTOR      _operator_block::~_operator_block()
		{
			for(Base::int_16u i=0;i<ACOUNT;i++)
				_operator::_clear(DATA[i]);
			DATA=(_operator **)MMMfree(DATA,TCOUNT*sizeof(_operator*));
			OBJ_COUNTER_DEC;
		};
		// ==========================================================================
		PROCEDURE       _operator_block::_normalize()
		{
			if(MMMresize((Base::_pvoid*)&DATA,sizeof(_operator*)*TCOUNT,sizeof(_operator*)*ACOUNT))
				TCOUNT=ACOUNT;
		};
		// ==========================================================================
		Base::int_bool       _operator_block::_request_volume(Base::int_mem c)                      // ��������� �� ����������� ��������
		{                                                                 // ����� ��������� ���-�� ���������
			if (ACOUNT+c>TCOUNT)                                      // � ������, ���� �� ��� �� ��������
			{
				Base::int_mem ncnt=Base::int_mem(TCOUNT*2);                // ����� ������� ������������ ��� ������
				while (ACOUNT+c>ncnt)                             // �������� ���������� ������
					ncnt=Base::int_mem(ncnt*_OPRBAPtr_incpercdef);
				if(MMMresize((Base::_pvoid*)&DATA,sizeof(Base::_pvoid)*TCOUNT,sizeof(Base::_pvoid)*ncnt))  // �������� ������������
				{
					TCOUNT=ncnt;                                      // ��������� ��������
				}else return 0;
			};
			return 1;
		};
		// ==========================================================================
		Base::int_bool       _operator_block::_add(Base::_pvoid i)                              
		{                                                           
			if (i==nullptr) return 0;
			if (_request_volume(1))                                       // � ����������� ������� ���-�� ��������
			{
				DATA[ACOUNT]=(_operator*)i;								  // � �������, ���� ����� ��� ��� ������
				ACOUNT++;                                                 // �� ��� �����������
				return 1;
			};
			return 0;
		};
		// ==========================================================================
		Base::int_mem          _operator_block::count()
		{
			return ACOUNT;                 // ���������� ���������� �������� ���������
		};
		// ==========================================================================
		PROCEDURE	_operator_block	::keep_as_text	(TBasket &b)
		{
			header_operator::keep_as_text(b);
			Base::int_16u t1=b.addTab();
			if (this->TYPE()& CJ_OPR_BLK_ONCE)
			{
				b.putLex(CJ_LEX_CMP_DIR_CS,CJCP_CODESTATE_once,CJ_OPRN_BLK_ONCE);
				b.putLn();
			};
			b.putLex(CJ_LEX_opr_bs ,0,"{",0);
			b.incTab();
			if (this->TYPE()& CJ_OPR_BLK_ONCE)
			{
				COMMS;
				if (!(this->TYPE()& CJ_OPR_BLK_ONCEPASS))
				{
					b.printf("NOT ");
				};
				b.printf("PASSED");
				COMME;
			};

			b.putLn();
			keep_as_textB(b);
			b.decTab();
			b.putLn();
			b.putLex(CJ_LEX_opr_be ,0,"}",0);
			b.addTabOff(t1);
		};
		// ==========================================================================
		PROCEDURE	_operator_block	::keep_as_textB	(TBasket &b)
		{
			Base::int_16u t1=b.addTab();
			if ((!DATA)||(!ACOUNT))
			{
				COMMS;
				b.printf("TODO:\tO_o\toperator block data empty");
				COMME;
			}
			else
			{
				for (Base::int_mem i=0;i<ACOUNT;i++)
				{
					if(i){
						b.putLex(CJ_LEX_opr_dv ,0,";",0);
						b.putLn();
					};
					_operator::keep_as_text(b,DATA[i]);
				};
			};
			b.addTabOff(t1);
		};
		// ==========================================================================
		Base::int_bool   _operator_block::HaveSyncBlockMode()
		{
			if(DATA && ACOUNT)
			{
				for(Base::int_mem i=0;i<ACOUNT;i++)
					if(_operator::HaveSyncBlockMode(DATA[i]))
						return true;
			};
			return false;
		};
		// ==========================================================================
		PROCEDURE	_operator_block::Onced()
		{
			type|=CJ_OPR_BLK_ONCEPASS;
		};
		// ==========================================================================
		PROCEDURE	_operator_block::build_lexem(TCharParce &_p, _lexem     &_l,Base::int_bool once)
		{
		// ���� ������ ����� ��� ���� ����� ���������� ��� �������� �������������� ���� ��
		// ������ ";" ����� �� ��������� ������ ���������� � �� ������������ � ������ �� ��������
			#define PASS_EMPTY while(_l.MAJOR()==CJ_LEX_opr_dv){lpc=_l.getPos();NEXT_LEX;};
			TBuffer::TJ_Buff_position lp=_l.getPos();
			TBuffer::TJ_Buff_position lpc;
			Base::ERROR_code  ok=ERROR_NoError;
			CItem::_operator_block* first= new CItem::_operator_block();
			if (!first)
			{
				ok=ERROR_OPER_BLOCK_OOM1;
			}else
			{
				first->_BreakPointInfo=_p.Detach_BreakPointInfo();
				if(once)
				{
					first->type|=CJ_OPR_BLK_ONCE;
				};
			};
			Base::int_8u getnext=1;
			_operator* op=nullptr;
			while (ok==ERROR_NoError)
			{
				PASS_EMPTY
				CItem::_operator::build_lexem(_p,_l);
				if (_l.IsObj(CJ_LOBJ_operator))
				{
					op=(_operator*)_l._detachB();
					if(first->_add(op))
					{
						op=nullptr;
						NEXT_LEX;
						if (NEMA(CJ_LEX_opr_dv))
						{
							ok=ERROR_Done;
							_p.returnTo(_l);
						}else{
							PASS_EMPTY
							if ((EQMA(CJ_LEX_opr_be))||(EQMA(CJ_LEX_eof)))
							{
								ok=ERROR_Done;
								_p.returnTo(_l);
							};
						}
					}else
					{
						ok=ERROR_OPER_BLOCK_OOM2;
						_operator::_clear(op);
					};
				}else{
					if((_l.MAJOR()==CJ_LEX_Lerror)&&(_l.MINOR()==ERROR_OPERATOR_case_found))
					{
						_p.setPos(lpc.g);
						ok=ERROR_Done;
					}else ok=ERROR_OPER_BLOCK_GOPR;
				};
			};
			if (ok==ERROR_Done)
			{	
				first->_normalize();
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{
				delete(first);
				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
			#undef PASS_EMPTY
		};
		// ==========================================================================
		PROCEDURE	_operator_block::build_lexemB(TCharParce &_p, _lexem     &_l)
		{
			#define PASS_EMPTY while(_l.MAJOR()==CJ_LEX_opr_dv){NEXT_LEX;};
			CItem::_operator*  first = nullptr;
			Base::ERROR_code ok = ERROR_Done;
			Base::int_bool is_firstCall =false;
			TBuffer::TJ_Buff_position    lp =_l.getPos();

			if ((ok==ERROR_Done)&&(EQMA(CJ_LEX_opr_bs)))
			{
				if(_p.getCodeStateFlag(CJCP_CODESTATE_once)==CJCP_CODESTATE_once)
				{
					if(_p.getCodeStateFlag(CJCP_CODESTATE_once_takken)==CJCP_CODESTATE_once_takken)
					{
						ok=ERROR_OPER_BLOCK_reFirst;
					}else
					{
						is_firstCall=true;
						_p.setCodeStateFlag(CJCP_CODESTATE_once,0);
						_p.setCodeStateFlag(CJCP_CODESTATE_once_takken,1);
					};
				};
				NEXT_LEX;
				PASS_EMPTY
				if (ok==ERROR_Done)
				{
					if (EQMA(CJ_LEX_opr_be))
					{
						first=new _operator();
					}else
					{
						CItem::_operator_block::build_lexem(_p,_l,is_firstCall);
						if (_l.IsObj(CJ_LOBJ_operator))
						{
							first=(CItem::_operator*)_l._detachB();
							NEXT_LEX;
							if (NEMA(CJ_LEX_opr_be))
								ok=ERROR_OPER_BLOCK_END;
						}
						else
						{
							ok=ERROR_OPER_BLOCK_LIST;
						};
					};
				};
			};
			if(is_firstCall)
				_p.setCodeStateFlag(CJCP_CODESTATE_once_takken,0);


			if ((ok==ERROR_Done)&&(first))
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	_operator::_clear(first);

				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
			#undef PASS_EMPTY
		};
		// ==========================================================================
		CONSTRUCTOR	_operator_syncro::_operator_syncro():synObj(nullptr),body(nullptr),flags(0)
		{
			OBJ_COUNTER_INC;
			type=CJ_OPR_Syncro;
		};
		// ==========================================================================
		DESTRUCTOR	_operator_syncro::~_operator_syncro()
		{
			_operator::_clear(body);
			if(synObj)
			{
				if(flags & CJ_OPRSYNC_OBJ)
				{
					delete ((_expression*)synObj);
				}else{
					delete ((MP::Mutex*)synObj);
				};
				synObj=nullptr;
			};
			flags=0;
		};
		// ==========================================================================
		PROCEDURE   _operator_syncro::keep_as_text(TBasket &b)
		{
			if(type&CJ_OPR_STRUC_ALT){
				_operator::keep_as_text(b,body);
				b.add(' ');
			};
			header_operator::keep_as_text(b);

			b.putLex(CJ_LEX_whiteword2, CJ_OPR_Syncro,CJ_OPRN_syncro,0);
			b.add(' ');

			if ((flags & CJ_OPRSYNC_OBJ)&&(synObj)) { ((_expression*)synObj)->keep_as_textARG(b);};

			if(!(type&CJ_OPR_STRUC_ALT))
				_operator::keep_as_textSUB(b,body);
		};
		// ==========================================================================
		Base::int_bool   _operator_syncro::HaveSyncBlockMode()
		{
			if(!(flags & CJ_OPRSYNC_OBJ))
				return true;
			return _operator::HaveSyncBlockMode(body);
		};
		// ==========================================================================
		PROCEDURE	_operator_syncro::build_lexem(TCharParce &_p, _lexem     &_l,_operator* &_b,Base::int_bool funcmode)
		{
			CItem::_operator_syncro*	first	= new CItem::_operator_syncro();
			Base::ERROR_code			ok		= ERROR_Done;
			TBuffer::TJ_Buff_position   lp		=_l.getPos();

			if ((_b)&&(funcmode)){														ok=ERROR_OPER_SYNC_CALLERR;
			}else	if (!first){														ok=ERROR_OPER_SYNC_OOM1;
				}else
				if((EQMA(CJ_LEX_whiteword2))&&(_l.MINOR()==CJ_OPR_Syncro)){
						first->_BreakPointInfo=_p.Detach_BreakPointInfo();
					
					NEXT_LEX;  
					if(EQMA(CJ_LEX_arg_bs)){NEXT_LEX;									// -----------get args if have
						CItem::_expression::build_lexem(_p,_l);
						if (_l.IsObj(CJ_LOBJ_exprsn)){
							if (first->synObj=(CItem::_expression*)_l._detachB()){
								first->flags|=CJ_OPRSYNC_OBJ;
								NEXT_LEX;
								if(EQMA(CJ_LEX_arg_be))
								{
									NEXT_LEX;
								}else													ok=ERROR_OPER_SYNC_NOABE;
							}else														ok=ERROR_OPER_SYNC_OOM3;
						}else															ok=ERROR_OPER_SYNC_WREXPR;
					}else if(!(first->synObj= new MP::Mutex()))							ok=ERROR_OPER_SYNC_OOM2;
					//---
					if(ok==ERROR_Done)
					{
						if(!(first->flags & CJ_OPRSYNC_OBJ)){	// if not arg check resync
							if(_p.getCodeStateFlag(CJCP_CODESTATE_syncro)==CJCP_CODESTATE_syncro)
																						ok=ERROR_OPER_SYNC_RESYNC;
							else
								_p.setCodeStateFlag(CJCP_CODESTATE_syncro,1);
						};
						if(ok==ERROR_Done){			// getting body
								if ( _b )
								{			
									first->body=_b;	first->type|=CJ_OPR_STRUC_ALT;	_b=nullptr;
									if(!(first->flags & CJ_OPRSYNC_OBJ))
												if(_operator::HaveSyncBlockMode(first->body))
																						ok=ERROR_OPER_SYNC_RESYNC2;
								}else{
									if(funcmode)	  _operator_block::build_lexemB(_p,_l);
									else			  _operator::build_lexem(_p,_l);
									if (_l.IsObj(CJ_LOBJ_operator))
									{
										if(!(first->body=(CItem::_operator*)_l._detachB()))      ok=ERROR_OPER_SYNC_OOM4;
									}else														 ok=ERROR_OPER_SYNC_WrBOPR;
								};
						};
						if(!(first->flags & CJ_OPRSYNC_OBJ))	_p.setCodeStateFlag(CJCP_CODESTATE_syncro,0);
					};
					//---
				}else																		ok=ERROR_OPER_SYNC_NOWW;
			if (ok==ERROR_Done)
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	
				_operator* p=(_operator*)first;
				first=nullptr;
				_operator::_clear(p);
				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
		};
		// ==========================================================================
		CONSTRUCTOR   _operator_if::_operator_if():expr(nullptr),body(nullptr),elsbody(nullptr)
		{
			type=CJ_OPR_If;
			OBJ_COUNTER_INC;
		};
		// ==========================================================================
		DESTRUCTOR   _operator_if::~_operator_if()
		{
			if ((type & CJ_OPR_MASK)==CJ_OPR_If)
			{
				if (expr)
				{
					delete (expr);
					expr=nullptr;
				};
				_operator::_clear(body);
				_operator::_clear(elsbody);
			};
			OBJ_COUNTER_DEC;
		};
		// ==========================================================================
		PROCEDURE	_operator_if	::keep_as_text	(TBasket &b,Base::int_8u elsif)
		{
			if(elsif==0)
			{
				if(type&CJ_OPR_STRUC_ALT)
				{
					_operator::keep_as_text(b,body);
					b.putLn();
				};
				header_operator::keep_as_text(b);
				b.putLex(CJ_LEX_whiteword2, CJ_OPR_If,CJ_OPRN_If,0);
				b.add(' ');
			}else
				header_operator::keep_as_text(b);

			if (expr)
			{
				expr->keep_as_textARG(b);
			};
			if(!(type&CJ_OPR_STRUC_ALT))
				_operator::keep_as_textSUB(b,body);
			if (elsbody)
			{
				Base::int_8u alt=0;
				b.putLn();
				switch	(type & CJ_OPR_ELSE_ALTMASK)
				{
					case	CJ_OPR_ELSE_Def:
						{
							b.putLex(CJ_LEX_whiteword4, CJ_OPR_SUB_else,CJ_OPRN_SUB_else,0);
							_operator::keep_as_textSUB(b,elsbody);
						};
						break;
					case	CJ_OPR_ELSE_ALT1:	b.putLex(CJ_LEX_whiteword4, CJ_OPR_SUB_elsif ,CJ_OPRN_SUB_elsif,0);alt=1;break;
					case	CJ_OPR_ELSE_ALT2:	b.putLex(CJ_LEX_whiteword4, CJ_OPR_SUB_elseif,CJ_OPRN_SUB_elseif,0);alt=1;break;
					default:
						COMMS;
						b.printf("wrong alt else form");
						COMME;
				};
				if (alt)
				{
					if ((elsbody->TYPE()& CJ_OPR_MASK)==CJ_OPR_If)
					{
						((_operator_if*)elsbody)->keep_as_text(b,1);
					}
					else
					{
						COMMS;
						b.printf("wrong alt else opeartor type");
						COMME;
					};
				};
			};
		};
		// ==========================================================================
		Base::int_bool   _operator_if::HaveSyncBlockMode()
		{
			if(_operator::HaveSyncBlockMode(body))return true;
			if(_operator::HaveSyncBlockMode(elsbody))return true;
			return _expression::HaveSyncBlockMode(expr);
		};
		// ==========================================================================
		PROCEDURE	_operator_if	::build_lexem(TCharParce &_p, _lexem &_l,_operator* &_b,Base::int_8u elsif)
										// _b ����� ��� ����������� ����������� ���� {operator} while(...);
										// ��� ������� ������ ���������� NULL ����� �� ��� ���������� ���� ����������
		{

			Base::ERROR_code ok = ERROR_Done;
			TBuffer::TJ_Buff_position    lp =_l.getPos();
			CItem::_operator_if* first=nullptr;


			if(first = new CItem::_operator_if())
			{
						first->_BreakPointInfo=_p.Detach_BreakPointInfo();

				if(elsif==0)
					if(!((EQMA(CJ_LEX_whiteword2))&&(_l.MINOR()==CJ_OPR_If)))
						ok=ERROR_OPER_IF_NOWW;
				if(ok==ERROR_Done)
				{
					if(elsif==0) NEXT_LEX;
					CItem::_vglink::build_lexem_args(_p,_l);
					if(first->expr=(CItem::_expression*)_l.DetachIfIsObj(CJ_LOBJ_exprsn))
					{
						if (_b){
							first->body=_b;
							first->type|=CJ_OPR_STRUC_ALT;
							_b=nullptr;
						}else{
							NEXT_LEX;
							CItem::_operator::build_lexem(_p,_l);
							if(!(first->body=(CItem::_operator*)_l.DetachIfIsObj(CJ_LOBJ_operator)))
								ok=ERROR_OPER_IF_WrBOPR;
						};
						NEXT_LEX;
						if(EQMA(CJ_LEX_whiteword4))
						{
							Base::int_8u iselif=0;
							switch(_l.MINOR())
							{
								case	CJ_OPR_SUB_else:	break;
								case	CJ_OPR_SUB_elsif:	first->type|=CJ_OPR_ELSE_ALT1;iselif=1;	break;
								case	CJ_OPR_SUB_elseif:	first->type|=CJ_OPR_ELSE_ALT2;iselif=1;	break;
								default:ok=ERROR_OPER_IF_EWW;
							};
							if (ok==ERROR_Done)
							{
								NEXT_LEX;
								if (iselif)
								{
									_operator* b=nullptr;
									build_lexem(_p,_l,b,1);
								}else
									CItem::_operator::build_lexem(_p,_l);
								if(!(first->elsbody=(CItem::_operator*)_l.DetachIfIsObj(CJ_LOBJ_operator)))
									ok=ERROR_OPER_IF_WrEBOPR;
							};
						}else
							_p.returnTo(_l);
					}
					else
						ok=ERROR_OPER_IF_WREXPR;
				};
			}else
				ok=ERROR_OPER_IF_OOM1;
			if (ok==ERROR_Done)
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	
				_operator* p=(_operator*)first;
				first=nullptr;
				_operator::_clear(p);
				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
		};
		// ==========================================================================
		CONSTRUCTOR   _operator_for::_operator_for():init(nullptr),expr(nullptr),turn(nullptr),body(nullptr)
		{
			type=CJ_OPR_For;
			OBJ_COUNTER_INC;
		};
		// ==========================================================================
		DESTRUCTOR   _operator_for::~_operator_for()
		{
			_operator::_clear(init);
			if (expr)
			{
				delete (expr   );
				expr=nullptr;
			};
			_operator::_clear(turn);
			_operator::_clear(body);
			OBJ_COUNTER_DEC;
		};
		// ==========================================================================
		PROCEDURE	_operator_for	::keep_as_text	(TBasket &b)
		{
			if(type&CJ_OPR_STRUC_ALT){
				_operator::keep_as_text(b,body);
				b.add(' ');
			};
			header_operator::keep_as_text(b);
			b.putLex(CJ_LEX_whiteword2, CJ_OPR_For,CJ_OPRN_For,0);
			b.add(' ');
			b.putLex(CJ_LEX_arg_bs,0,"(",0);
			if(init) _operator::keep_as_text(b,init);
			b.putLex(CJ_LEX_opr_dv ,0,";",0);
			if (expr){expr->keep_as_text(b);};
			b.putLex(CJ_LEX_opr_dv ,0,";",0);
			if(turn) _operator::keep_as_text(b,turn);
			b.putLex(CJ_LEX_arg_be,0,")",0);
			if(!(type&CJ_OPR_STRUC_ALT))
				_operator::keep_as_textSUB(b,body);
		};
		// ==========================================================================
		Base::int_bool   _operator_for::HaveSyncBlockMode()
		{
			if(_operator::HaveSyncBlockMode(body))return true;
			if(_operator::HaveSyncBlockMode(turn))return true;
			if(_operator::HaveSyncBlockMode(init))return true;
			return _expression::HaveSyncBlockMode(expr);
		};
		// ==========================================================================
		PROCEDURE	_operator_for	::build_lexem(TCharParce &_p, _lexem &_l,CItem::_operator* &_b)       // _b ����� ��� ����������� ����������� ���� {operator} while(...);
																	 // ��� ������� ������ ���������� NULL ����� �� ��� ���������� ���� ����������
		{
			CItem::_operator_for* first = new CItem::_operator_for();
			Base::ERROR_code ok = ERROR_Done;
			TBuffer::TJ_Buff_position    lp =_l.getPos();
			if (!first)
			{
				ok=ERROR_OPER_FOR_OOM1;
			} 
			else
				if((EQMA(CJ_LEX_whiteword2))&&(_l.MINOR()==CJ_OPR_For))
				{
					first->_BreakPointInfo=_p.Detach_BreakPointInfo();
					NEXT_LEX;
					if(EQMA(CJ_LEX_arg_bs))
					{
						NEXT_LEX;
						if(NEMA(CJ_LEX_opr_dv))									// getting init part
						{
							CItem::_operator::build_lexem(_p,_l);
							if (_l.IsObj(CJ_LOBJ_operator))
							{
								first->init=(_operator*)_l._detachB();
								NEXT_LEX;
								if(NEMA(CJ_LEX_opr_dv))
									ok=ERROR_OPER_FOR_ExpOd1;
							}else ok=ERROR_OPER_FOR_WrIOPR;
						};
						if (ok==ERROR_Done)
						{
							NEXT_LEX;
							if(NEMA(CJ_LEX_opr_dv))								//  getting check part
							{
								CItem::_expression::build_lexem(_p,_l);
								if (_l.IsObj(CJ_LOBJ_exprsn))
								{
									first->expr=(CItem::_expression*)_l._detachB();
									NEXT_LEX;
									if(NEMA(CJ_LEX_opr_dv))
										ok=ERROR_OPER_FOR_ExpOd2;
								}else
									ok=ERROR_OPER_FOR_WREXPR;
							};
						};
						if (ok==ERROR_Done)
						{
							NEXT_LEX;
							if(NEMA(CJ_LEX_arg_be))									// getting init part
							{
								CItem::_operator::build_lexem(_p,_l);
								if (_l.IsObj(CJ_LOBJ_operator))
								{
									first->turn=(_operator*)_l._detachB();
									NEXT_LEX;
									if(NEMA(CJ_LEX_arg_be))
										ok=ERROR_OPER_FOR_NOABE;
								}else ok=ERROR_OPER_FOR_WrTOPR;
							};
						};
						if (ok==ERROR_Done)
						{
							if (_b){
								first->body=_b;
								first->type|=CJ_OPR_STRUC_ALT;
								_b=nullptr;
							}else{
								NEXT_LEX;
								if (ISOPEND){
									if(first->body= new CItem::_operator())
									{
										_p.returnTo(_l);
									}else
									{
										ok=ERROR_OPER_FOR_OOM2;
									};
								}else{
									CItem::_operator::build_lexem(_p,_l);
									if (_l.IsObj(CJ_LOBJ_operator)){
										first->body=(CItem::_operator*)_l._detachB();
									}else{
										ok=ERROR_OPER_FOR_WrBOPR;
									};
								};
							};
						};
				}else{
				  ok=ERROR_OPER_FOR_NOABS;
				};
			}else{
			  ok=ERROR_OPER_FOR_NOWW;
			};
			if (ok==ERROR_Done)
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	
				_operator* p=(_operator*)first;
				first=nullptr;
				_operator::_clear(p);
				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
		};
		// ==========================================================================
		CONSTRUCTOR   _operator_while::_operator_while(Base::int_bool DoMode):expr(nullptr),body(nullptr)
		{
			type=DoMode?CJ_OPR_Do:CJ_OPR_While;
			OBJ_COUNTER_INC;
		};
		// ==========================================================================
		DESTRUCTOR   _operator_while::~_operator_while()
		{
			if (expr)
			{
				delete (expr);
				expr=nullptr;
			};
			_operator::_clear(body);
			OBJ_COUNTER_DEC;
		};
		// ==========================================================================
		PROCEDURE	_operator_while	::keep_as_text	(TBasket &b)
		{
			if(type&CJ_OPR_STRUC_ALT)
			{
				_operator::keep_as_text(b,body);
				b.add(' ');
			};
			header_operator::keep_as_text(b);

			if((type & CJ_OPR_MASK)==CJ_OPR_Do)
			{
				b.putLex(CJ_LEX_whiteword2, CJ_OPR_Do,CJ_OPRN_do,0);
				b.add(' ');
				if(!(type&CJ_OPR_STRUC_ALT))
					_operator::keep_as_textSUB(b,body);
			};
			b.add(' ');
			b.putLex(CJ_LEX_whiteword2, CJ_OPR_While,CJ_OPRN_While,0);
			b.add(' ');
			if (expr)
				expr->keep_as_textARG(b);

			if(((type & CJ_OPR_MASK)!=CJ_OPR_Do)&&(!(type&CJ_OPR_STRUC_ALT)))
				_operator::keep_as_textSUB(b,body);
		};
		// ==========================================================================
		Base::int_bool   _operator_while::HaveSyncBlockMode()
		{
			if(_operator::HaveSyncBlockMode(body))return true;
			return _expression::HaveSyncBlockMode(expr);
		};
		// ==========================================================================
		PROCEDURE	_operator_while	::build_lexem(TCharParce &_p, _lexem &_l,_operator* &_b)     // _b ����� ��� ����������� ����������� ���� {operator} while(...);
																	 // ��� ������� ������ ���������� NULL ����� �� ��� ���������� ���� ����������
		{
			CItem::_operator_while* first = new CItem::_operator_while();
			Base::ERROR_code ok = ERROR_Done;
			TBuffer::TJ_Buff_position    lp =_l.getPos();
			if (!first)
			{
				ok=ERROR_OPER_WHILE_OOM1;
			}
			else
			if((EQMA(CJ_LEX_whiteword2))&&(_l.MINOR()==CJ_OPR_While)){
						first->_BreakPointInfo=_p.Detach_BreakPointInfo();

				NEXT_LEX;
				if(EQMA(CJ_LEX_arg_bs)){
					NEXT_LEX;
					CItem::_expression::build_lexem(_p,_l);
					if (_l.IsObj(CJ_LOBJ_exprsn)){
						if (first->expr=(CItem::_expression*)_l._detachB())
						{
							NEXT_LEX;
							if(EQMA(CJ_LEX_arg_be)){
								if (_b){
									first->body=_b;
									first->type|=CJ_OPR_STRUC_ALT;
									_b=nullptr;
								}else{
									NEXT_LEX;
									if (ISOPEND){
										if(first->body= new CItem::_operator())
										{
										_p.returnTo(_l);
										}
										else
										{
											ok=ERROR_OPER_WHILE_OOM2;
										}
									}else{
										CItem::_operator::build_lexem(_p,_l);
										if (_l.IsObj(CJ_LOBJ_operator)){
											if(!(first->body=(CItem::_operator*)_l._detachB()))
											{
												ok=ERROR_OPER_WHILE_OOM3;
											};
										}else{
											ok=ERROR_OPER_WHILE_WrBOPR;   // ��� ������ �������� ������ �� ���� ��� ����� ���� ���� ��� ����
											// �.�. ������ �������� � ��������� � ����� ����������� � ����� ����������� ���������
										};
									};
								};
							}else{
							  ok=ERROR_OPER_WHILE_NOABE;
							};
						}else{
							ok=ERROR_OPER_WHILE_OOM4;
						};
					}else{
						ok=ERROR_OPER_WHILE_WREXPR;
					};
				}else{
				  ok=ERROR_OPER_WHILE_NOABS;
				};
			}else{
			  ok=ERROR_OPER_WHILE_NOWW;
			};
			if (ok==ERROR_Done)
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	
				_operator* p=(_operator*)first;
				first=nullptr;
				_operator::_clear(p);
				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
		};
		// ==========================================================================
		PROCEDURE	_operator_while	::build_lexem_Do(TCharParce &_p, _lexem &_l,_operator* &_b)
		{
			CItem::_operator_while* first = new CItem::_operator_while(true);
			Base::ERROR_code ok = ERROR_Done;
			TBuffer::TJ_Buff_position    lp =_l.getPos();
			if (!first)
			{
				ok=ERROR_OPER_WHILEDO_OOM1;
			}
			else
				if((EQMA(CJ_LEX_whiteword2))&&(_l.MINOR()==CJ_OPR_Do))
				{
					first->_BreakPointInfo=_p.Detach_BreakPointInfo();
					NEXT_LEX;
					if (_b)
					{
						first->body=_b;
						first->type|=CJ_OPR_STRUC_ALT;
						_b=nullptr;
						if((EQMA(CJ_LEX_whiteword2))&&(_l.MINOR()==CJ_OPR_While))
						{
							NEXT_LEX;
							if(EQMA(CJ_LEX_arg_bs))
							{
								NEXT_LEX;
								CItem::_expression::build_lexem(_p,_l);
								if (_l.IsObj(CJ_LOBJ_exprsn))
								{
									if (first->expr=(CItem::_expression*)_l._detachB())
									{
										NEXT_LEX;
										if(NEMA(CJ_LEX_arg_be))
														ok=ERROR_OPER_WHILEDO_NOABE;
									}else			    ok=ERROR_OPER_WHILEDO_OOM3;
								}else					ok=ERROR_OPER_WHILEDO_WREXPR;
							}else						ok=ERROR_OPER_WHILEDO_NOABS;
						}else							ok=ERROR_OPER_WHILEDO_NOWW2;
					}else
					{
						CItem::_operator::build_lexem(_p,_l);
						if (_l.IsObj(CJ_LOBJ_operator))
						{
							if(!(first->body=(CItem::_operator*)_l._detachB()))
							{
								ok=ERROR_OPER_WHILEDO_OOM2;
							}else
								if((first->body->TYPE() &(CJ_OPR_While|CJ_OPR_STRUC_ALT))==(CJ_OPR_While|CJ_OPR_STRUC_ALT))
								{
									auto s=(CItem::_operator_while*)first->body;
									first->body=s->body;s->body=nullptr;
									first->expr=s->expr;s->expr=nullptr;
									delete s;
								}else
									ok=ERROR_OPER_WHILEDO_BodyExp;
						}else
							ok=ERROR_OPER_WHILEDO_WrBOPR;
					};
				}else
					ok=ERROR_OPER_WHILEDO_NOWW1;

			if (ok==ERROR_Done)
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	
				_operator* p=(_operator*)first;
				first=nullptr;
				_operator::_clear(p);
				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
		};
		// ==========================================================================
		CONSTRUCTOR   _operator_foreach::_operator_foreach():expr(nullptr),
																storage(nullptr),
																body(nullptr),
																elsbody(nullptr)
		{
			type=CJ_OPR_Foreach;
			OBJ_COUNTER_INC;
		};
		// ==========================================================================
		DESTRUCTOR   _operator_foreach::~_operator_foreach()
		{
			if (storage)
			{
				delete storage;
				storage=nullptr;
			};
			if (expr)
			{
				delete (expr   );
				expr=nullptr;
			};
			_operator::_clear(body);
			_operator::_clear(elsbody);
			OBJ_COUNTER_DEC;
		};
		// ==========================================================================
		PROCEDURE		_operator_foreach::keep_as_text(TBasket &b)
		{
			if(type&CJ_OPR_STRUC_ALT)
			{
				_operator::keep_as_text(b,body);
				b.add(' ');
			};
			header_operator::keep_as_text(b);
			b.putLex(CJ_LEX_whiteword2, CJ_OPR_Foreach,CJ_OPRN_Foreach,0);
			b.putLex(CJ_LEX_arg_bs,0,"(",0);
			if (expr){expr->keep_as_text(b);};
			b.add(' ');
			b.putLex(CJ_LEX_extractKey, 0,"=>",0);
			b.add(' ');
			if(storage)storage->keep_as_text(b);
			b.putLex(CJ_LEX_arg_be,0,")",0);

			if(!(type&CJ_OPR_STRUC_ALT))
				_operator::keep_as_textSUB(b,body);
			if(elsbody)
			{
				b.putLn();
				b.putLex(CJ_LEX_whiteword4, CJ_OPR_SUB_elsf,CJ_OPRN_SUB_elsf,0);
				_operator::keep_as_textSUB(b,elsbody);
			};

		};
		// ==========================================================================
		Base::int_bool   _operator_foreach::HaveSyncBlockMode()
		{
			if(_operator::HaveSyncBlockMode(body))return true;
			return _expression::HaveSyncBlockMode(expr);
		};
		// ==========================================================================
		PROCEDURE     _operator_foreach::build_lexem(TCharParce &_p,_lexem     &_l,_operator* &_b)
		{
			CItem::_operator_foreach* first = new CItem::_operator_foreach();
			Base::ERROR_code ok = ERROR_Done;
			TBuffer::TJ_Buff_position    lp =_l.getPos();
			if (!first)
			{
				ok=ERROR_OPER_FOREACH_OOM1;
			}
			else
				if((EQMA(CJ_LEX_whiteword2))&&(_l.MINOR()==CJ_OPR_Foreach))
				{
						first->_BreakPointInfo=_p.Detach_BreakPointInfo();

					NEXT_LEX;
					if(EQMA(CJ_LEX_arg_bs))
					{
						NEXT_LEX;
						CItem::_expression::build_lexem(_p,_l);
						if (_l.IsObj(CJ_LOBJ_exprsn))
						{
							if (first->expr=(CItem::_expression*)_l._detachB())
							{
								NEXT_LEX;
								if(EQMA(CJ_LEX_extractKey))
								{
									NEXT_LEX;
									CItem::_headArgList::build_lexem(_p,_l,0);
									if(_l.IsObj(CJ_LOBJ_HeadArgList))
									{
										first->storage=(CItem::_headArgList*)_l._detachB();
										NEXT_LEX;
										if(EQMA(CJ_LEX_arg_be))
										{
											if (_b)
											{
												first->body=_b;
												first->type|=CJ_OPR_STRUC_ALT;
												_b=nullptr;
											}else{
												NEXT_LEX;
												if (ISOPEND)
												{
													if(first->body= new CItem::_operator())
													{
														_p.returnTo(_l);
													}
													else
													{
														ok=ERROR_OPER_FOREACH_OOM2;
													}
												}else
												{
													CItem::_operator::build_lexem(_p,_l);
													if (_l.IsObj(CJ_LOBJ_operator))
													{
														if(!(first->body=(CItem::_operator*)_l._detachB()))
														{
															ok=ERROR_OPER_FOREACH_OOM3;
														};
													}else{
														ok=ERROR_OPER_FOREACH_WrBOPR;   // ��� ������ �������� ������ �� ���� ��� ����� ���� ���� ��� ����
														// �.�. ������ �������� � ��������� � ����� ����������� � ����� ����������� ���������
													};
												};
											};

											if(ok==ERROR_Done)
											{
												NEXT_LEX;
												if((_l.MAJOR()==CJ_LEX_whiteword4) &&(_l.MINOR()==CJ_OPR_SUB_elsf))
												{
													NEXT_LEX;
													CItem::_operator::build_lexem(_p,_l);
													if (_l.IsObj(CJ_LOBJ_operator))
													{
														if(!(first->elsbody=(CItem::_operator*)_l._detachB()))
														{
															ok=ERROR_OPER_FOREACH_OOM5;
														};
													}else{
														ok=ERROR_OPER_FOREACH_WrBOPR2;
													};
												}else _p.returnTo(_l);
											};

										}else{
											ok=ERROR_OPER_FOREACH_NOARGBE;
										};
									}else{
										ok=ERROR_OPER_FOREACH_NOSTLIST;
									};
								}else{
									ok=ERROR_OPER_FOREACH_NOEXKEY;
								};
							}else{
								ok=ERROR_OPER_FOREACH_OOM4;
							};
						}else{
							ok=ERROR_OPER_FOREACH_WREXPR;
						};
					}else{
						ok=ERROR_OPER_FOREACH_NOARGBS;
					};
				}else{
				   ok=ERROR_OPER_FOREACH_NOWW;
				};
			if (ok==ERROR_Done)
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	
				_operator* p=(_operator*)first;
				first=nullptr;
				_operator::_clear(p);
				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
		};
		// ==========================================================================
		CONSTRUCTOR   _operator_try::_operator_try():_catch(nullptr),body(nullptr)
		{
			type=CJ_OPR_Try;
			OBJ_COUNTER_INC;
		};
		// ==========================================================================
		DESTRUCTOR   _operator_try::~_operator_try()
		{
			_operator::_clear(body);
			_operator::_clear(_catch);
			OBJ_COUNTER_DEC;
		};
		// ==========================================================================
		PROCEDURE		_operator_try::keep_as_text(TBasket &b)
		{
			header_operator::keep_as_text(b);
			b.putLex(CJ_LEX_whiteword2, CJ_OPR_Try,CJ_OPRN_try,0);
			b.putLn();
			_operator::keep_as_text(b,body);
			b.putLn();
			b.putLex(CJ_LEX_whiteword4, CJ_OPR_SUB_catch,CJ_OPRN_SUB_catch,0);
			b.add(' ');
			if(type&CJ_OPR_STRUC_ALT)
				b.putLex(CJ_LEX_arg_bs,0,"(",0);
			as.keep_as_text(b);
			if(type&CJ_OPR_STRUC_ALT)
				b.putLex(CJ_LEX_arg_be,0,")",0);
			b.putLn();
			_operator::keep_as_text(b,_catch);
		};
		// ==========================================================================
		Base::int_bool   _operator_try::HaveSyncBlockMode()
		{
			if(_operator::HaveSyncBlockMode(body))return true;
			return _operator::HaveSyncBlockMode(_catch);
		};
		// ==========================================================================
		PROCEDURE     _operator_try::build_lexem(TCharParce &_p,_lexem     &_l)
		{
			CItem::_operator_try* first = new CItem::_operator_try();
			Base::ERROR_code ok = ERROR_Done;
			TBuffer::TJ_Buff_position    lp =_l.getPos();
			if (!first)
			{
				ok=ERROR_OPER_TRY_OOM;
			}
			else
				if((EQMA(CJ_LEX_whiteword2))&&(_l.MINOR()==CJ_OPR_Try))
				{
						first->_BreakPointInfo=_p.Detach_BreakPointInfo();

					NEXT_LEX;
					_operator_block::build_lexemB(_p,_l);
					if (_l.IsObj(CJ_LOBJ_operator))
					{
						first->body=(_operator*)_l._detachB();
						NEXT_LEX;
						if((EQMA(CJ_LEX_whiteword4))&&(_l.MINOR()==CJ_OPR_SUB_catch))
						{
							NEXT_LEX;
							if(EQMA(CJ_LEX_arg_bs))
							{
								first->type|=CJ_OPR_STRUC_ALT;
								NEXT_LEX;
							};
							if(EQMA(CJ_LEX_ident))
							{
								first->as=_l.IDENT();
								NEXT_LEX;
								if(first->type & CJ_OPR_STRUC_ALT)
									if(EQMA(CJ_LEX_arg_be))
									{
										NEXT_LEX;
									}else
										ok=ERROR_OPER_TRY_ARBE;
								if(ok == ERROR_Done)
								{
									_operator_block::build_lexemB(_p,_l);
									if (_l.IsObj(CJ_LOBJ_operator))
									{
										first->_catch=(_operator*)_l._detachB();
									}else
										ok=ERROR_OPER_TRY_CBODY;
								};
							}else
								ok=ERROR_OPER_TRY_IDENT;
						}else
							ok=ERROR_OPER_TRY_CATCH;
					}else
						ok=ERROR_OPER_TRY_TBODY;
				}else
					ok=ERROR_OPER_TRY_TRY;
			if (ok==ERROR_Done)
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	
				_operator* p=(_operator*)first;
				first=nullptr;
				_operator::_clear(p);
				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
		};
		// ==========================================================================
		CONSTRUCTOR   _operator_switch::switch_case::switch_case():oper(nullptr)
		{

		};
		// ==========================================================================
		DESTRUCTOR   _operator_switch::switch_case::~switch_case()
		{
			ident=nullptr;
			_operator::_clear(oper);
		}
		// ==========================================================================
		CONSTRUCTOR   _operator_switch::_operator_switch(Base::int_32 ic):ACOUNT(0),TCOUNT(ic),mode(CSCAOp_eq),_default(nullptr),_arg(nullptr)
		{
			OBJ_COUNTER_INC;
			type=CJ_OPR_switch;
			if(ic>0)
				DATA=(switch_case**) MMMalloc(16*sizeof(switch_case*),1);
			else
				DATA=nullptr;
		};
		// ==========================================================================
		DESTRUCTOR   _operator_switch::~_operator_switch()
		{
			for(Base::int_16u i=0;i<ACOUNT;i++)
				delete DATA[i];
			DATA=(switch_case **)MMMfree(DATA,TCOUNT*sizeof(switch_case*));
			_operator::_clear(_default);
			if (_arg)
			{
				delete _arg;
				_arg=nullptr;
			};
			ACOUNT=0;
			TCOUNT=0;
			OBJ_COUNTER_DEC;
		};
		// ==========================================================================
		Base::int_bool   _operator_switch::HaveSyncBlockMode()
		{
			if(DATA && ACOUNT)
				for(Base::int_mem i=0;i<ACOUNT;i++)
					if(DATA[i])
						if(_operator::HaveSyncBlockMode(DATA[i]->oper))
							return true;
			if(_operator::HaveSyncBlockMode(_default))return true;
			return _expression::HaveSyncBlockMode(_arg);
		};
		// ==========================================================================
		PROCEDURE            _operator_switch::_normalize()
		{
			if(TCOUNT!=ACOUNT)
			if(MMMresize((Base::_pvoid*)&DATA,sizeof(switch_case*)*TCOUNT,sizeof(switch_case*)*ACOUNT))
				TCOUNT=ACOUNT;
		};
		// ==========================================================================
		Base::int_bool       _operator_switch::_request_volume(Base::int_mem c)
		{
			if (ACOUNT+c>TCOUNT)                                      // � ������, ���� �� ��� �� ��������
			{
				Base::int_mem ncnt=Base::int_mem(TCOUNT*2);                // ����� ������� ������������ ��� ������
				while (ACOUNT+c>ncnt)                             // �������� ���������� ������
					ncnt=Base::int_mem(ncnt*_OPRBAPtr_incpercdef);

				if(MMMresize((Base::_pvoid*)&DATA,sizeof(switch_case*)*TCOUNT,sizeof(switch_case*)*ncnt))  // �������� ������������
				{
					TCOUNT=ncnt;                                      // ��������� ��������
				}else return 0;
			};
			return 1;
		};
		// ==========================================================================
		Base::int_bool       _operator_switch::_add(switch_case* i)
		{
			if (i==nullptr) return 0;
			if (_request_volume(1))                                       // � ����������� ������� ���-�� ��������
			{
				DATA[ACOUNT]=i;		   						  // � �������, ���� ����� ��� ��� ������
				ACOUNT++;                                                 // �� ��� �����������
				return 1;
			};
			return 0;
		};
		// ==========================================================================
		Base::int_mem        _operator_switch::count()
		{
			return ACOUNT;
		};
		// ==========================================================================
		_operator_switch::switch_case*	_operator_switch::handle_case(TCharParce &_p,
											  _lexem     &_l,Base::int_bool skip_ww)
		{
			TCharParce::TJ_LA_options po =_p.GetState();
			TCharParce::TJ_LA_options pn =po | CJLSCN_joinminus_digit;
			_p.SetState(pn);
			if((EQMA(CJ_LEX_whiteword4))&&(_l.MINOR()==CJ_OPR_SUB_case))
			{
				NEXT_LEX;
				_p.SetState(po);
			}else
				if(!skip_ww){
					_p.SetState(po);
					return nullptr;
				};
				GCObjRef	 ident;
				_operator*	 oper=nullptr;
				if(EQMA(CJ_LEX_digit) || EQMA(CJ_LEX_string))
				{
					_l.GetScalar((Base::_pvoid*)&ident);
					NEXT_LEX;
					if((EQMA(CJ_LEX_exprifc))&&(_l.MINOR()==CSCAOp_expc_else))
					{
						NEXT_LEX;
						if(skip_ww)
						{
							_operator::build_lexem(_p,_l);
						}
						else
							_operator_block::build_lexem(_p,_l);
						if (_l.IsObj(CJ_LOBJ_operator))
						{
							if(oper = (CItem::_operator*)_l._detachB())
							{
								if(switch_case* ret= new switch_case())
								{
									ret->ident=ident;
									ret->oper=oper;
									return ret;
								};
								_operator::_clear(oper);
							};
						};
					};
				};

			return nullptr;
		};
		// ==========================================================================
		PROCEDURE		_operator_switch::keep_as_text(TBasket &b)
		{
			header_operator::keep_as_text(b);
			Base::int_16u t1=b.addTab();
			b.putLex(CJ_LEX_whiteword2, CJ_OPR_switch,CJ_OPRN_switch,0);
			if(mode==CSCAOp_streq)
			{
				b.add(' ');
				b.putLex(CJ_LEX_binr_op, CSCAOp_streq,"eq",0);
			};
			_arg->keep_as_textARG(b);
			if(type&CJ_OPR_STRUC_ALT)
				b.putLex(CJ_LEX_pref_op, CSCAOp_notword,"!",0);
			b.putLn();
			b.putLex(CJ_LEX_opr_bs ,0,"{",0);
			b.incTab();
			if ((!DATA)||(!ACOUNT))
			{
				b.putLn();
				COMMS;
				b.printf("TODO:\tO_o\toperator switch data empty");
				COMME;
				b.putLn();
			}
			else
			{
				for (Base::int_mem i=0;i<ACOUNT;i++)
				{
					if(i)
						b.putLex(CJ_LEX_opr_dv ,0,";",0);
					if(DATA[i])
					{
						b.putLn();
						if(!(type&CJ_OPR_STRUC_ALT))
						{
							b.putLex(CJ_LEX_whiteword4, CJ_OPR_SUB_case,CJ_OPRN_SUB_case,0);
							b.add(' ');
						};
						GCT::keep_as_text(DATA[i]->ident,b);
						b.putLex(CJ_LEX_exprifc ,CSCAOp_expc_else,":",0);
						if(type&CJ_OPR_STRUC_ALT)
						{
							_operator::keep_as_text(b,DATA[i]->oper);
							
						}else
							((_operator_block*)DATA[i]->oper)->keep_as_textB(b);
					}else
					{
						COMMS;
						b.printf("TODO:\tO_o\toperator switch case data empty");
						COMME;
						return;
					}
				};
			};
			if(_default)
			{
				b.putLex(CJ_LEX_opr_dv ,0,";",0);
				b.putLn();
				if(!(type&CJ_OPR_STRUC_ALT))
				{
					b.putLex(CJ_LEX_whiteword4, CJ_OPR_SUB_default,CJ_OPRN_SUB_default,0);
				};
				b.putLex(CJ_LEX_exprifc ,CSCAOp_expc_else,":",0);
				_operator::keep_as_text(b,_default);
			};
			b.decTab();
			b.putLn();
			b.putLex(CJ_LEX_opr_be ,0,"}",0);
			b.addTabOff(t1);
		};
		// ==========================================================================
		PROCEDURE     _operator_switch::build_lexem(TCharParce &_p, _lexem     &_l)
		{
			CItem::_operator_switch* first = new CItem::_operator_switch();
			Base::ERROR_code ok = ERROR_NoError;
			TBuffer::TJ_Buff_position    lp =_l.getPos();
			if (first)
			{
						first->_BreakPointInfo=_p.Detach_BreakPointInfo();

				if((EQMA(CJ_LEX_whiteword2))&&(_l.MINOR()==CJ_OPR_switch))
				{
					NEXT_LEX;
					if((EQMA(CJ_LEX_binr_op))&&(_l.MINOR()==CSCAOp_streq))
					{
						NEXT_LEX;
						first->mode=CSCAOp_streq;
					};

					if(EQMA(CJ_LEX_arg_bs))
					{
						NEXT_LEX;
						CItem::_expression::build_lexem(_p,_l);
						if (_l.IsObj(CJ_LOBJ_exprsn))
						{
							if (first->_arg=(CItem::_expression*)_l._detachB())
							{
								NEXT_LEX;
								if(EQMA(CJ_LEX_arg_be))
								{
									NEXT_LEX;
									Base::int_bool skip_ww=0;
									/*
									if((EQMA(CJ_LEX_pref_op))&&(_l.MINOR()==CSCAOp_notword))
									{
										NEXT_LEX;
										first->type|=CJ_OPR_STRUC_ALT;
										skip_ww=1;
									};
									/**/
									if (EQMA(CJ_LEX_opr_bs))
									{
										NEXT_LEX;
										while(ok == ERROR_NoError)
										{
											if(switch_case* sc=handle_case(_p,_l,skip_ww))
											{
												first->_add(sc);
												NEXT_LEX;
												if (EQMA(CJ_LEX_opr_dv))
												{
													while(EQMA(CJ_LEX_opr_dv)) NEXT_LEX;
													if(EQMA(CJ_LEX_whiteword4))
													{
														switch(_l.MINOR())
														{
															case CJ_OPR_SUB_case:break;

															case CJ_OPR_SUB_default:
																NEXT_LEX;
																if((EQMA(CJ_LEX_exprifc))&&(_l.MINOR()==CSCAOp_expc_else))
																{
																	NEXT_LEX;
																	_operator::build_lexem(_p,_l);
																	if (_l.IsObj(CJ_LOBJ_operator))
																	{
																		if(first->_default=(CItem::_operator*)_l._detachB())
																		{
																			NEXT_LEX;
																			while(EQMA(CJ_LEX_opr_dv)) NEXT_LEX;
																			ok=ERROR_Done;
																		}else
																			ok=ERROR_OPER_SWITCH_OOM3;
																	}else ok=ERROR_OPER_SWITCH_EROPR;

																}else ok=ERROR_OPER_SWITCH_expc_else;
																break;
															default: ok=ERROR_OPER_SWITCH_WRWW;
														};
													}else
														if(skip_ww)
														{
															if((EQMA(CJ_LEX_exprifc))&&(_l.MINOR()==CSCAOp_expc_else))
															{
																NEXT_LEX;
																_operator::build_lexem(_p,_l);
																if (_l.IsObj(CJ_LOBJ_operator))
																{
																	if(first->_default=(CItem::_operator*)_l._detachB())
																	{
																		NEXT_LEX;
																		while(EQMA(CJ_LEX_opr_dv)) NEXT_LEX;
																		ok=ERROR_Done;
																	}else
																		ok=ERROR_OPER_SWITCH_OOM3;
																}else ok=ERROR_OPER_SWITCH_EROPR;
															}else
																if(!(EQMA(CJ_LEX_digit) || EQMA(CJ_LEX_string)))
																	ok=ERROR_Done;
														}else
															ok=ERROR_Done;
														
												}else ok=ERROR_Done;
											}else	ok=ERROR_OPER_SWITCH_WRCASE;
										};
										if(ok==ERROR_Done)
											if (NEMA(CJ_LEX_opr_be))
												ok=ERROR_OPER_SWITCH_NOOPRBE;
									}else ok=ERROR_OPER_SWITCH_NOOPRBS;
								}else ok=ERROR_OPER_SWITCH_NOARGBE;
							}else ok=ERROR_OPER_SWITCH_OOM2;
						}else ok=ERROR_OPER_SWITCH_WREXPR;
					}else ok=ERROR_OPER_SWITCH_NOARGBS;
				}else ok=ERROR_OPER_SWITCH_WWEXP;
			}else	ok=ERROR_OPER_SWITCH_OOM1;
			if (ok==ERROR_Done)
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	
				_operator* p=(_operator*)first;
				first=nullptr;
				_operator::_clear(p);
				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
		};
		// ==========================================================================
		CONSTRUCTOR		_operator_var::_operator_var():_decls(nullptr)
		{
			type=CJ_OPR_var;
		};
		// ==========================================================================
		DESTRUCTOR		_operator_var::~_operator_var()
		{
			if(_decls) delete _decls;
			_decls=nullptr;
		};
		// ==========================================================================
		PROCEDURE		_operator_var::keep_as_text(TBasket &b)
		{
			if(_decls)
			{
				b.putLex(CJ_LEX_whiteword2, CJ_OPR_var,CJ_OPRN_var,0);
				b.add(' ');
				_decls->keep_as_text_FVO(b);
			}else
			{
				COMMS;
				b.printf("TODO:\tO_o\toperator var data empty");
				COMME;
			};
		};
		// ==========================================================================
		PROCEDURE		_operator_var::build_lexem(TCharParce &_p,_lexem &_l)
		{
			Base::ERROR_code ok = ERROR_Done;
			TBuffer::TJ_Buff_position    lp =_l.getPos();
			CItem::_operator_var* first=nullptr;


			if(first = new CItem::_operator_var())
			{
				first->_BreakPointInfo=_p.Detach_BreakPointInfo();
				if((EQMA(CJ_LEX_whiteword2))&&(_l.MINOR()==CJ_OPR_var))
				{
					NEXT_LEX;
					_fenvdecl::build_for_var_oper(_p,_l);
					if(_l.IsObj(CJ_LOBJ_fenvdecl))
					{
						first->_decls=(_fenvdecl*)_l._detachB();
					}else
					ok=ERROR_OPER_VAR_NO_DECLS;
				}else
					ok=ERROR_OPER_VAR_NOWW;
			}else
				ok=ERROR_OPER_VAR_OOM1;
			if (ok==ERROR_Done)
			{	
				_l.clear();// �� ����������� ������� 
				_l.LEXEM_SET_OBJECT(CJ_LOBJ_operator,CJLOpt_NotActive,(TJ_Lex_date_ep)first);// � ������� � ��� ���������� ������
				_l.setPos(lp);
			}
			else
			{	
				if(first) delete first;
				first=nullptr;
				_l.PackLerror(ok,_p.getPosB(lp),&_p);
			};
		};
		// ==========================================================================
	};
};
