/////////////////////////////////////////////////////////////////////////////////////
// Contains simplifier types associations
/////////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "lcommon0.h"



//--------------------------------------------------------------------------

#define NOWARNG			1

#define PROCEDURE      void
#define CONSTRUCTOR
#define FUNCTION
#define DESTRUCTOR
#define OPERATOR       void
#define NULL	     0
#define MINPOOLSIZE		0x0100
//--------------------------------------------------------------------------
//#define USED_POOLTYPE(c,ss,fp) Base::MW::MMNoPools<c>
#define USED_POOLTYPE(c,ss,fp) Base::MW::MMPooled<c,ss,fp>

#define  DECL_NEW_DELETE(c,ss,fp)	  public:\
								static Base::pchar_8 TypeNameStr; \
								typedef	USED_POOLTYPE(c,ss,fp) MyPoolType;\
								static MyPoolType MyPool;\
								void* operator new (Base::int_mem size)\
								{\
									return MyPool._new(size);\
								};\
								void operator delete (void* p)\
								{\
									MyPool._delete(p);\
								};\
								inline static c*	ToInstance(Base::_pvoid ptr)\
								{\
									return MyPool.ToInstance(ptr);\
								};\
								inline static Base::MW::SegmentItemIndex inSegmentIndex(Base::_pvoid ptr)\
								{\
									return MyPool.inSegmentIndex(ptr);\
								};


#define  DECLARE_TEMPLATE_CLASS(c,ss,fp)	class c{				DECL_NEW_DELETE(c,ss,fp);private:
#define  DECLARE_CLASS(c,ss,fp)				class c;class		c{	DECL_NEW_DELETE(c,ss,fp);private:
#define  DECLARE_CLASSB(c,p,ss,fp)			class c:protected   p{	DECL_NEW_DELETE(c,ss,fp);private:
#define  DECLARE_CLASSP(c,p,ss,fp)			class c:public		p{	DECL_NEW_DELETE(c,ss,fp);private:

#define  OBJ_COUNTER_DEC	  MyPool.dec();
#define  OBJ_COUNTER_INC	  MyPool.inc();

#define INIT_OBJ_COUNTER(t)		t::MyPoolType	t::MyPool;\
								Base::pchar_8	t::TypeNameStr=###t;\
								Base::pchar_8	t::MyPoolType::ItemsPool::PoolName=###t;

#define USE_MP
//#define USE_POSIX

#define  MMMalloc(sz,cl)		MyPool.malloc(sz,cl)
#define  MMMfree(p,sz)			MyPool.free(p,sz)
#define  MMMcopyMA(p,sz)		MyPool.copyMA(p,sz)
#define  MMMresize(d,os,ns)		MyPool.resize(d,os,ns)
#define  MMMcopy(dst,src,sz)	MyPool.copy(dst,src,sz)
#define  MMMclear(p,sz)			MyPool.clear(p,sz)
//--------------------------------------------------------------------------


#define  typetostring(t)	(###t)
//#pragma message("dd")
#define  RELEASE_ME(t) ::printf("\n\n\t\t\t%s: PLEASE RELEASE ME. \n\n",t);

#define  FIX_ME(t)     ::printf("\n\n\t\t\t%s: PLEASE FIX ME. \n\n",t);
#define  ERROR_HINT(t) ::printf("\n\n\t\t\tERROR HINT: %s. \n\n",t);
#define  HINT_HINT(t)  ::printf("\n\n\t\t\tHINT: %s. \n\n",t);
#define  I_CRITICAL(t)




//#pragma message("// IMPOTANT !!!")
//#pragma message("// ��������� ����� ������������ ����������� � ���������� �����������")
//#pragma message("// ��� ������� ��� �������� ����� ������� ������� ������ ���� �����.")

namespace Base
{
	//////////////////////////////////////////////
	typedef void            _NULL;
	//////////////////////////////////////////////

	typedef unsigned char   int_8u;     //  (  1 )
	typedef char            int_8;      //  (  1 )
	typedef char            char_8;     //  (  1 )

	typedef unsigned short  int_16u;    //  (  2 )
	typedef short           int_16;     //  (  2 )

	typedef unsigned int    int_32u;    //  (  4 )
	typedef int             int_32;     //  (  4 )
	typedef unsigned int    int_mem;    //  (  4 )
	typedef	int_8u			int_bool;


	typedef int_32u         ERROR_code; //  (  2 ) ��� ������ ��������������� TJ_Lex_minor � _lexem.h
	typedef int_32			int_counter;


	typedef __int64         int_64;     //  (  8 )
	typedef __int64         int_64u;     //  (  8 )
	//   typedef __int128        int_128;    //  ( 16 )   --- �� �������������� ������� ������������, � ���� ��� ��� ���� ���� �� ���������

	typedef float           flt_32;     //  (  4 )
	typedef double          flt_64;     //  (  8 )

	typedef int_64          int_max;    //  (  8 )
	typedef flt_64          flt_max;    //  (  8 )

	//////////////////////////////////////////////

	typedef _NULL*          _pvoid;     //  (  4 )

	typedef int_8u*			pint_8u;    //  (  4 )
	typedef int_8*			pint_8;     //  (  4 )
	typedef char_8*			pchar_8;    //  (  4 )

	typedef int_16u*		pint_16u;   //  (  4 )
	typedef int_16*			pint_16;    //  (  4 )

	typedef int_32u*		pint_32u;   //  (  4 )
	typedef int_32*			pint_32;    //  (  4 )

	typedef int_64*			pint_64;    //  (  4 )

	typedef flt_32*			pflt_32;    //  (  4 )
	typedef flt_64*			pflt_64;    //  (  4 )


	PROCEDURE init();
	PROCEDURE done();

// FOR CONSOLE.H
	typedef struct ICommon
	{
		virtual PROCEDURE __stdcall Release()=0;
	}	ICommon;
	/**/
	//////////////////////////////////////////////
	namespace MW
	{	
		typedef	Base::int_32u	SegmentItemIndex;
	};


static_assert (sizeof(int_8u  )==1,"Wrong  Size of type int_8u");
static_assert (sizeof(int_8   )==1,"Wrong  Size of type int_8");
static_assert (sizeof(char_8  )==1,"Wrong  Size of type char_8");
static_assert (sizeof(int_16u )==2,"Wrong  Size of type int_16u");
static_assert (sizeof(int_16  )==2,"Wrong  Size of type int_16");
static_assert (sizeof(int_32u )==4,"Wrong  Size of type int_32u");
static_assert (sizeof(int_32  )==4,"Wrong  Size of type int_32");
static_assert (sizeof(int_mem )==4,"Wrong  Size of type int_mem");
static_assert (sizeof(int_bool)==1,"Wrong  Size of type int_bool");
static_assert (sizeof(int_64  )==8,"Wrong  Size of type int_64");
static_assert (sizeof(int_64u  )==8,"Wrong  Size of type int_64u");

static_assert (sizeof(flt_32  )==4,"Wrong  Size of type flt_32");
static_assert (sizeof(flt_64  )==8,"Wrong  Size of type flt_64");
static_assert (sizeof(_pvoid)==4,"Wrong  Size of type _pvoid");
static_assert (sizeof(pint_8u)==4,"Wrong  Size of type pint_8u");
static_assert (sizeof(pint_8)==4,"Wrong  Size of type pint_8");
static_assert (sizeof(pchar_8)==4,"Wrong  Size of type pchar_8");
static_assert (sizeof(pint_16u)==4,"Wrong  Size of type pint_16u");
static_assert (sizeof(pint_16)==4,"Wrong  Size of type pint_16");
static_assert (sizeof(pint_32u)==4,"Wrong  Size of type pint_32u");
static_assert (sizeof(pint_32)==4,"Wrong  Size of type pint_32");
static_assert (sizeof(pint_64)==4,"Wrong  Size of type pint_64");
static_assert (sizeof(pflt_32)==4,"Wrong  Size of type pflt_32");
static_assert (sizeof(pflt_64)==4,"Wrong  Size of type pflt_64");


};
namespace Execution
{
	class Context;
};

	#define CODEITEMS_FRIENDS	friend class Execution::Context;


