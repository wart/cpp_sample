/////////////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "lcommonA.h"
#include "MW.h"
#include <functional>
/////////////////////////////////////////////////////////////////////////////////////
namespace Base
{
	namespace Templates
	{
		template <typename T,int SegSize,typename pool> 
		DECLARE_TEMPLATE_CLASS(_st_list,SegSize,pool)
			CODEITEMS_FRIENDS;
			public:
				typedef _st_fifo<T,SegSize,pool>			SelfClass;
				typedef T ItemClass;				
				typedef std::function<Base::int_bool(ItemClass&)>	FindFunc;
				template <typename dT>
				class t_atom
				{
				public:
				typedef		t_atom<dT>	SelfClass;
				typedef		dT			ItemClass;
							DECL_NEW_DELETE(SelfClass,SegSize,pool);
							ItemClass	data;
				volatile	SelfClass*	prev;
				volatile	SelfClass* next;
							CONSTRUCTOR	t_atom<dT>()
							{
								OBJ_COUNTER_INC;
							};
							DESTRUCTOR ~t_atom<dT>()
							{
								OBJ_COUNTER_DEC;
							};
				static		PROCEDURE	MoveOnPop(ItemClass &src, ItemClass &dst)
							{
								dst=src;
							};
				};
				template <typename dT>
				class t_atom<dT*>
				{
				public:
				typedef		t_atom<dT>	SelfClass;
				typedef		dT*			ItemClass;
							DECL_NEW_DELETE(SelfClass,SegSize,pool);
							ItemClass	data;
				volatile	SelfClass*	prev;
							CONSTRUCTOR	t_atom<dT*>():data(nullptr)
							{
								OBJ_COUNTER_INC;
							};
							DESTRUCTOR ~t_atom<dT*>()
							{
								if(data)delete data;
								OBJ_COUNTER_DEC;
							};
				static		PROCEDURE	MoveOnPop(ItemClass&src, ItemClass&dst)
							{
								dst=src;
								src=nullptr;
							};
				};
				typedef		t_atom<T>	AtomClass;
		protected:
//...................................................................................
				volatile AtomClass*		first;
				volatile AtomClass*		last;
				int_mem		COUNT;
//...................................................................................
			public:
//...................................................................................
				AtomClass* getFirst()  // is critical use only in gct foreach, they know how work secure
				{
					return (AtomClass*)first;
				};
//...................................................................................
				PROCEDURE clear()
				{
					AtomClass*		cor=(AtomClass*)last,*prev=nullptr;
					while(cor)
					{
						prev=(AtomClass*)cor->prev;
						delete cor;
						cor=prev;
					};
					first=nullptr;
					last=nullptr;
					COUNT=0;
				};
//...................................................................................
				inline	CONSTRUCTOR	_st_list<T,SegSize,pool>():first(nullptr),last(nullptr),COUNT(0)
				{
					OBJ_COUNTER_INC;
				};
//...................................................................................
				DESTRUCTOR	   ~_st_list()
				{
					clear();
					OBJ_COUNTER_DEC;
				};
//...................................................................................
				int_mem _size()
				{
					return COUNT;
				};
//...................................................................................
				Base::int_bool push(ItemClass &nitm)
				{
					if(AtomClass* k = new AtomClass())
					{
						k->data=nitm;
						k->prev=last;
						if(last)
							last->next=k;
						else
							first=k;
						last=k;
						COUNT++;
						return true;
					};
					return false;
				};
//...................................................................................
				Base::int_bool unshift(ItemClass &nitm)
				{
					if(AtomClass* k = new AtomClass())
					{
						k->data=nitm;
						k->next=first;
						if(first)
							first->prev=k;
						else
							last=k;
						first=k;
						COUNT++;
						return true;
					};
					return false;
				};
//...................................................................................
				Base::int_bool shift(ItemClass& dst)
				{
					if(first)
					{
						auto k=(AtomClass*)first->next;
						AtomClass::MoveOnPop(((AtomClass*)first)->data,dst);
						delete first;
						first=k;
						if(k)
							k->prev=nullptr;
						else
							last=nullptr;
						COUNT--;
						return true;
					};
					return false;
				};
//...................................................................................
				Base::int_bool pop(ItemClass& dst)
				{
					if(last)
					{
						auto k=(AtomClass*)last->prev;
						AtomClass::MoveOnPop(((AtomClass*)last)->data,dst);
						delete last;
						last=k;
						if(k)
							k->next=nullptr;
						else
							first=nullptr;
						COUNT--;
						return true;
					};
					return false;
				};
//...................................................................................
				Base::int_bool pop(ItemClass& dst,FindFunc ff)
				{
					if (ff!=nullptr)
					{
						auto k=(AtomClass*)last;
						while(k)
						{
							if(ff(k->data))
							{
								AtomClass::MoveOnPop(k->data,dst);
								if(k->next){k->next->prev=k->prev;}else{last=k->prev;};
								if(k->prev){k->prev->next=k->next;}else{first=k->next;};
								COUNT--;
								delete k;
								return true;
							};
							k=(AtomClass*)k->prev;
						};
					};
					 return false;
				};
//...................................................................................
				Base::int_bool find(ItemClass& dst,FindFunc ff)
				{
					if (ff!=nullptr)
					{
						auto k=(AtomClass*)last;
						while(k)
						{
							if(ff(k->data))
							{
								dst=k->data;
								return true;
							};
							k=(AtomClass*)k->prev;
						};
					};
					 return false;
				};
//...................................................................................
				_st_list* reverse()
				{
					 auto f= new _st_list();
					 if(f)
					 {
						 auto k=(AtomClass*)last;
						 while(k)
						 {
							 f->push(k->data);
							 k=(AtomClass*)k->prev;
						 };
					 };
					 return f;
				};
//...................................................................................
				Base::int_bool push(ItemClass &nitm,FindFunc ff,Base::int_bool allow_on_notlisted)
				{
					if((ff!=nullptr)&&(COUNT))
					{
						auto k=(AtomClass*)first;
						while(k)
						{
							if(ff(k->data))
								return PlaceNear(k,nitm,false);
							k=(AtomClass*)k->next;
						};
					};
					if(allow_on_notlisted)
						return push(niem);
					return false;
				};
//...................................................................................
			private:
				PROCEDURE swap(AtomClass* k1,AtomClass* k2)
				{
					if( k1 && k2 && k1!=k2)
					{
						AtomClass* tp = (AtomClass*)k1->prev;
						AtomClass* tn = (AtomClass*)k1->next;
						k1->prev = k2->prev;
						k1->next = k2->next;
						k2->prev = tp;
						k2->next = tn;
						if(first==k1)
						{
							first=k2;
						}else
							if(first==k2)
							{
								first=k1;
							};
						if(last==k1)
						{
							last=k2;
						}else
							if(last==k2)
							{
								last=k1;
							};
					};
				};
//...................................................................................
				Base::int_bool PlaceNear(AtomClass* k,ItemClass &nitm,Base::int_bool after=false)
				{
					if(k)
						if(AtomClass* n = new AtomClass())
						{
							n->data=nitm;
							if(after)
							{
								n->next=k->next;
								k->next=n;
								n->prev=k;
								if(n->next)
									n->next->prev=n;
								else
									last=n;
							}
							else
							{
								n->prev=k->prev;
								k->prev=n;
								n->next=k;
								if(n->prev)
									n->prev->next=n;
								else
									first=n;
							};
							COUNT++;
							return true;
						};
					return false;
				};
//...................................................................................
		};
//...................................................................................
	};
};
/////////////////////////////////////////////////////////////////////////////////////
