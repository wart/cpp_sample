//===========================================================
#include "ManagedThread.h"
//===========================================================
namespace Managed
{
	//=======================================================
	CONSTRUCTOR	Thread::Thread():	RefCnt		(0),
									Managed_state(Managed_state_Burn),
									ThreadClass (ThreadClass_Abstract),
									name		(nullptr),
									parentId	(0),
									ManagedFlags(ManagedFlag_AllowPostStatus),
									ActiveWaiters(0),
									killEvent(true),
									SuspendCount(0),
									SleepTime	(0),
									ActiveChilds(0),
									waitingId	(0),
									waitEventCount(0),
									waitEventDesc(nullptr),
									waitEventTime(0),
									TryLockMutex(nullptr)
	{
		GetThreadArray()._add((Reference)this);
	};
	//=======================================================
	PROCEDURE			Thread::PreStart_initVars()
	{
		SuspendCount=0;
		SleepTime	=0;
		ActiveChilds=0;
		waitingId	=0;
		waitEventCount=0;
		Base::_cpchar::StrFree(waitEventDesc);
		waitEventDesc=nullptr;
		waitEventTime=0;
		TryLockMutex=nullptr;
//		killEvent.Reset();		// not needs called in OnThreadBegin()
	};
	//=======================================================
	DESTRUCTOR	Thread::~Thread()
	{
		Base::_cpchar::StrFree(name);
		Base::_cpchar::StrFree(waitEventDesc);
	};
	//=======================================================
	PROCEDURE	Thread::RemoveFromPublic()
	{
		Reference th=this;
		GetThreadArray().ForEach([&](Thread::Reference &s,Base::int_mem idx)->Base::int_16
		{
			if(s==th)
			{
				return ForEachFunc_Flag_Next|ForEachFunc_Flag_Delete;
			};
			return ForEachFunc_Flag_Next;
		});
	};
	//=======================================================
	Base::int_bool		Thread::IsAlive()
	{
		return ((IsRunning())&&(Managed_state!=Managed_state_Finishing));
	};
	//=======================================================
	Base::int_bool		Thread::IsManagedRunning()   //  UnSafe !!! call only when locked
	{
		return ((IsRunning())&&(Managed_state==Managed_state_Run));
	};
	//=======================================================
	Base::int_bool		Thread::IsAliveWithClass(Base::int_16u _class)
	{
		return (IsAlive())&& (_class==ThreadClass);
	};
	//=======================================================
	Base::int_mem		Thread::getManagedFlags ()
	{
		return ManagedFlags;
	};
	//=======================================================
	Base::int_bool		Thread::setManagedFlags (Base::int_mem mask,Base::int_mem val)
	{
		Base::int_mem fmask= mask & ((IsRunning())?(ManagedFlag_ONAIR_SETS):(~ManagedFlag_ONAIR_SETS));
		if(fmask==mask)
		{
			ManagedFlags&= ~ fmask;
			ManagedFlags|=val;
			return true;
		}
		else
			return false;
	};
	//=======================================================
	PROCEDURE			Thread::setManagedState(Base::int_8u ns,Base::pchar_8 desc)//  UnSafe !!! call only when locked
	{
		Managed_state=ns;
		if (ManagedFlags & ManagedFlag_AllowPostStatus )
			CallUpdatedStatusProcedure(GetId(), parentId, ThreadClass, ns, desc);
	};
	//=======================================================
	PROCEDURE	Thread::Sleep	(DWORD msec)
	{
		Reference	ref= getMe();
		if(ref!=nullptr)
		{
			ManagedThreadLockByRef(ref);
			if(ref->SelfCall())
				if(ref->IsManagedRunning())
					ref->SleepTime=msec;
		};
	};
	//=======================================================
	PROCEDURE	Thread::Suspend	(DWORD id)
	{
		Reference	ref= getById(id);
		if(ref!=nullptr)
		{
			ManagedThreadLockByRef(ref);
			if(ref->IsAlive())
				ref->SuspendCount++;
		};
	};
	//=======================================================
	PROCEDURE	Thread::Kill()
	{
		ManagedThreadLockSelf;
		if((IsRunning())&&(Managed_state!=Managed_state_Finishing))
		{
			switch(Managed_state)
			{
				case Managed_state_Run		 :	break;
				case Managed_state_Sleep	 :	break; //when wake up is killed
				case Managed_state_Suspend	 :
												SuspendCount=0;
												((MP::Thread*)(this))->Resume();
												break;
				case Managed_state_wait		 :
												{
													Reference	wt= getById(waitingId);
													if(wt!=nullptr)
													{
														ManagedThreadLockByRef(wt);
														if(wt->IsRunning())
															wt->ActiveWaiters--;
													};
													((MP::Thread*)(this))->Resume();
												};
												break;
				case Managed_state_waitChilds:	ManagedFlags&=~ManagedFlag_WaitingChilds;
												((MP::Thread*)(this))->Resume();
												break;
				case Managed_state_waitEvent :	break; //when wake up by killevent is killed
			};
			OnKill();
			killEvent.Set();
		};
	};
	//=======================================================
	PROCEDURE	Thread::Kill	(DWORD id)
	{
		Reference	ref= getById(id);
		if(ref!=nullptr)
			ref->Kill();
	};
	//=======================================================
	PROCEDURE	Thread::ManagedResume()
	{
		ManagedThreadLockSelf;
		if(IsRunning())
		{
			if(SuspendCount)
			{
				--SuspendCount;
				if((!SuspendCount)&&(Managed_state==Managed_state_Suspend))
					((MP::Thread*)(this))->Resume();
			};
		}else
		{
			if((getState()==Managed_state_Burn)&&(JustCreated()))
			{
				// some prepares for normally start
				PreStart_initVars();
				Start();
			};
		};
	};
	//=======================================================
	PROCEDURE	Thread::Resume	(DWORD id)
	{
		Reference	ref= getById(id);
		if(ref!=nullptr)
			ref->ManagedResume();
	};
	//=======================================================
	PROCEDURE	Thread::waitEvent(MP::Event &ev,DWORD msec,Base::pchar_8 desc)
	{
		MP::Event * e=&ev;
		waitManyEvent(&e,1,msec,desc);
	};
	//=======================================================
	Base::int_bool	Thread::waitManyEvent(MP::Event **ev, Base::int_8u cnt, DWORD msec,Base::pchar_8 desc)
	{
		if(ev && cnt && (cnt<MaxWaitEventCount+1))
		{
			ManagedThreadLockSelf;
			if(IsManagedRunning())
				if(waitEventCount==0)
				{
					for(Base::int_8u i=0;i<cnt;i++)
						if((waitEventptr[i]=ev[i])==nullptr)
							return false;
					waitEventCount=cnt;
					waitEventTime=msec;
					Base::_cpchar::StrFree(this->waitEventDesc);
					this->waitEventDesc=Base::_cpchar::StrCopy(desc);
					return true;
				};
		};
		return false;
	};
	//=======================================================
	Base::int_32u   Thread::waitManyEventInPlace(MP::Event **ev, Base::int_8u cnt, DWORD msec,Base::pchar_8 desc)//=INFINITE
	{
		Base::int_32u res=STATUS_INVALID_PARAMETER;
		ManagedThreadSelfLock_Enter;
		if((IsManagedRunning())&&(waitEventCount==0))
		{
			for(Base::int_8u i=0;i<cnt;i++)	if((waitEventptr[i]=ev[i])==nullptr)return res;
			waitEventptr[cnt]=&killEvent;
			setManagedState(Managed_state_waitEvent,desc);
			ManagedThreadSelfLock_Leave;
			// ---------------------------
			res=MP::Event::WaitManyEvent((MP::Event **)waitEventptr,cnt+1,msec,false);
			if(res!=cnt)  // if not killEvent
			{
				ManagedThreadSelfLock_Enter;
				setManagedState(Managed_state_Run);
				ManagedThreadSelfLock_Leave;
			};
		}else{ManagedThreadSelfLock_Leave;};
		return res;
	};
	//=======================================================
	Base::int_32u       Thread::SelfwaitManyEventInPlace(MP::Event **ev, Base::int_8u cnt, DWORD msec,Base::pchar_8 desc)
	{
		Reference	r=getMe();
		if(r!=nullptr)
			return r->waitManyEventInPlace(ev,cnt,msec,desc);
		return cnt+2;
	};
	//=======================================================
	PROCEDURE		Thread::CancelwaitEvent()
	{
		ManagedThreadLockSelf;
		if(IsManagedRunning())
			waitEventCount=0;
	};
	//=======================================================
	PROCEDURE	Thread::setName	(DWORD id,Base::pchar_8 newname)
	{
		Reference	ref= getById(id);
		if(ref!=nullptr)
			ref->Rename(newname);
	};
	//=======================================================
	PROCEDURE	Thread::print_info(DWORD id)
	{
		Reference	ref= getById(id);
		if(ref!=nullptr)
			ref->print_info();
	};
	//=======================================================
	PROCEDURE	Thread::Rename(Base::pchar_8 newname)
	{
		ManagedThreadLockSelf;
		Base::_cpchar::StrFree(this->name);
		this->name=Base::_cpchar::StrCopy(newname);
		CallUpdatedNameProcedure(GetId(), ThreadClass, this->name);
	};
	//=======================================================
	PROCEDURE	Thread::SetParented	()
	{
		ManagedThreadLockSelf;
		if(parentId==0)
		{
			Reference	ref= getMe();
			if(ref!=nullptr)
			{
				ManagedThreadLockByRef(ref);
				if(ref->GetId()!=GetId())
				{
					parentId=ref->GetId();
					ref->ActiveChilds++;
				};
			};
		};
	};
	//=======================================================
	PROCEDURE	Thread::waitchilds	()
	{
		ManagedThreadLockSelf;
		if(IsManagedRunning())
			if(ActiveChilds>0)
				ManagedFlags|=ManagedFlag_WaitingChilds;
	};
	//=======================================================
	PROCEDURE	Thread::wait(DWORD id)
	{
		ManagedThreadLockSelf;
		if((IsManagedRunning())&&(waitingId==0)&&(GetId()!=id))
		{
			Reference	ref= getById(id);
			if(ref!=nullptr)
			{
				ManagedThreadLockByRef(ref);
				if(ref->IsAlive())
				{
					waitingId=id;
					ref->ActiveWaiters++;
				};
			};
		};
	};
	//=======================================================
};
//===========================================================
/*	PROCEDURE	Thread::SetActionEvent	(Base::int_mem cod)
{
ManagedThreadLockSelf;
if(ActionEventCode==cod)
if(Managed_state==Managed_state_waitEvent)
{
ActionEvent.Set();
ActionEventCode=0;
};
};/**/
//=======================================================
/*	PROCEDURE	Thread::GoWaitActionEvent(Base::int_mem cod,DWORD msec,Base::pchar_8 desc)
{
if(SelfCall() && cod)
{
ActionEventCode=cod;
ActionEvent.Reset();
waitEvent(ActionEvent,msec,desc);
};
};/**/
//=======================================================