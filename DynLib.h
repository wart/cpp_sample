//-------------------------------------------------------
#pragma once
//-------------------------------------------------------
#include "lcommonA.h"
//-------------------------------------------------------
//    http://www.tdoc.ru/c/programming/visual-cpp/visual-cpp-beginners-page29.html
//-------------------------------------------------------
namespace OS
{
	//---------------------------------------------------
	class DynamicLibrary
	{
		public:
			CONSTRUCTOR			DynamicLibrary(Base::pchar_8 path);
			DESTRUCTOR			~DynamicLibrary();
			FARPROC				GetProcAddress(Base::pchar_8 name);
			const Base::pchar_8	GetPath();
			Base::int_bool		IsLoaded();
		//-----------------------------------------------
		private:
			HINSTANCE			inst;
			Base::pchar_8		_path;
	};
	//---------------------------------------------------
};
//-------------------------------------------------------
