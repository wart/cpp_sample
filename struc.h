#pragma once
#include <lcommonA.h>
#include "_ident.h"
#include "GCT.h"
#include "TCharParce.h"
#include "lcommonB.h"
#include "TBasket.h"
#include "StreamReader.h"
#include <functional>
#include "../base/st_darr.hpp"
#include "GCArgWrapper.h"
#include "../base/logger.h"

#define GCTSDDname_RAW		"RAW"
#define GCTSDDname_Mapper	"DataMapper"
#define GCTSDDname_DataDesc	"DataDesc"
#define GCTSDDname_Template	"StructTemplate"

#define GCTSDDname_DataStorage	"DataStorage"


// 512Mb
#define struc_MAX_RAW_SIZE	(1<<25)

namespace struc
{
	// ------------------------------------------------------------
	typedef	Base::int_mem	Size;
	typedef	Base::int_32	Offset;
	typedef	Base::int_mem	Index;
	typedef	Base::_pvoid	Ptr;
	typedef	Base::int_8u	Atom;
	typedef	Parcing::_ident	Identifier;
	// ------------------------------------------------------------
	Ptr			MemMalloc(Size size);
	PROCEDURE	MemFree(Ptr ptr,Size size);
	PROCEDURE	MemCopy(Ptr src,Ptr dst, Size size);
	// ------------------------------------------------------------
	namespace GC
	{
		class DataStorage;
	};
	// ------------------------------------------------------------
	namespace DataDesc
	{
		// --------------------------------------------------------
		namespace Class
		{
			// ----------------------------------------------------
			typedef enum Desc
			{
				Abstract	= 0,
				SimpleType	= 1,
				Array		= 2,
				Struct		= 3,
				Union		= 4,
				Extern		= 5	// allow only in template
			}Desc;
			// ----------------------------------------------------
			typedef enum Simple
			{
				int8u	= 1,
				int8	= 2,
				int16u	= 3,
				int16	= 4,
				int32u	= 5,
				int32	= 6,
				int64u	= 7,
				int64	= 8,
				flt32	= 9,
				flt64	= 10,
				char8	= 11,
				dtime64 = 12,
				ftime64 = 13,
				MAX_SIMPLE=13
			}Simple;
			// ----------------------------------------------------
		};
		class Specializer;
		class TextKeeper;
		// --------------------------------------------------------
		class Abstract:public Parcing::GCTempl::RefCountable
		{
			// ----------------------------------------------------
			friend PROCEDURE init();
			// ----------------------------------------------------
						Class::Desc		desc_type;
			// ----------------------------------------------------
			protected:
						Size 			data_size;
			// ----------------------------------------------------
						CONSTRUCTOR     Abstract(Class::Desc cd);
			// ----------------------------------------------------
				virtual DESTRUCTOR	    ~Abstract();
			// ----------------------------------------------------
		public :
			// ----------------------------------------------------
				typedef Parcing::GCTempl::ReferenceTemplate<Abstract,Identifier>	Reference;
			//-----------------------------------------------------
				virtual Size			getDataSize()=0;
				virtual Reference		getChildTypeByName(Identifier &tn);
				virtual Base::int_bool  GetAttrAsGC(Identifier &arg,GCObjRef &ret);
				virtual Base::int_bool	isTemplate();
				virtual Reference		Specialize(Specializer &spc);
				virtual PROCEDURE		keep_as_text(TextKeeper& tk)=0;
						PROCEDURE		keep_as_text(Parcing::TBasket &b);
				virtual PROCEDURE		Serealize(Parcing::TBasket &b);
				virtual Base::int_bool	isOneLined();
				virtual Base::int_bool  HaveChild(Reference a);
				virtual PROCEDURE       PrnWayToChild(TextKeeper& tk,Reference a);
				virtual Base::int_32	ItemsOffset(Base::int_mem idx);  // -1 not found
				virtual Base::int_32	ItemsOffset(Identifier &itname); // -1 not found
			//-----------------------------------------------------
						Class::Desc     Dtype(){return desc_type;};
			//-----------------------------------------------------
				static  PROCEDURE		SerealizeHash(Reference::Hash &s,Parcing::TBasket &b);
				static  PROCEDURE		Copy(Ptr src, Size srcsz, Ptr dst,Size dstsz);
				static  Base::int_bool	isTemplateArray(Reference::Hash &s);
				static  PROCEDURE		SpecializeArray(Specializer &spc,Reference::Hash &s,Reference::Hash &d);
				static  Reference	    Deserealize(StreamReader* sr);
				static  Base::int_bool	DeserealizeHash(Reference::Hash &s,StreamReader* sr);
			//-----------------------------------------------------
		};
		// --------------------------------------------------------
		typedef	 	Abstract::Reference AbsRef; 
		// --------------------------------------------------------
		class Simple:public Abstract
		{
		public:
						Class::Simple	data_type;
			//  ---------------------------------------------------
						CONSTRUCTOR     Simple();
				virtual DESTRUCTOR     ~Simple();
			//  ---------------------------------------------------
				virtual Size			getDataSize();
				virtual Base::int_bool	isTemplate();
				virtual PROCEDURE		keep_as_text(TextKeeper& tk);
				virtual PROCEDURE		Serealize(Parcing::TBasket &b);
				virtual Base::int_bool	isOneLined();
				virtual Base::int_32	ItemsOffset(Base::int_mem idx);  // -1 not found
				virtual Base::int_32	ItemsOffset(Identifier &itname); // -1 not found
			//  ---------------------------------------------------
						PROCEDURE		settypename(GCObjRef& ret);
			//  ---------------------------------------------------
				static  AbsRef          getSimple(Class::Simple cs);
				static  Base::flt_64	getValue(Ptr p,Class::Simple t);
				static  PROCEDURE 	    setValue(Ptr p,Class::Simple t,Base::flt_64 val);
				static  AbsRef	        Deserealize(StreamReader* sr);
		};
		// --------------------------------------------------------
		class Array:public Abstract
		{
			public:
										DECL_NEW_DELETE(Array,MINPOOLSIZE,Base::MW::CItemPool);
						Size			array_len;
						AbsRef			item_desc;
						Identifier		ExtName;		// for template array len param
			//  ---------------------------------------------------
						CONSTRUCTOR	    Array();
				virtual DESTRUCTOR		~Array();
			//  ---------------------------------------------------
				virtual Size			getDataSize();
				virtual AbsRef			getChildTypeByName(Identifier &tn);
				virtual Base::int_bool  GetAttrAsGC(Identifier &arg,GCObjRef &ret);
				virtual Base::int_bool	isTemplate();
				virtual AbsRef			Specialize(Specializer &spc);
				virtual PROCEDURE		keep_as_text(TextKeeper& tk);
				virtual PROCEDURE		Serealize(Parcing::TBasket &b);
				virtual Base::int_bool	isOneLined();
				virtual Base::int_bool	HaveChild(AbsRef a);
				virtual PROCEDURE		PrnWayToChild(TextKeeper& tk,AbsRef a);
				virtual Base::int_32	ItemsOffset(Base::int_mem idx);
			//  ---------------------------------------------------
						Size			getItemSize();
						AbsRef			Flat();
			//  ---------------------------------------------------
				static  AbsRef			Create1D(AbsRef item,Size len);
				static  AbsRef			Create2D(AbsRef item,Size rows,Size rowlen);
				static  AbsRef			Deserealize(StreamReader* sr);
			//  ---------------------------------------------------
		};
		// --------------------------------------------------------
		class Struct:public Abstract
		{
		public:
										DECL_NEW_DELETE(Struct,MINPOOLSIZE,Base::MW::CItemPool);
						AbsRef::Hash    item_desc;
						AbsRef::Hash 	sub_desc;
					    GCObjRef::Hash  fun_desc;
			//  ---------------------------------------------------
					    CONSTRUCTOR	    Struct();
				virtual DESTRUCTOR	    ~Struct();
			//  ---------------------------------------------------
				virtual Size			getDataSize		  ();
				virtual AbsRef			getChildTypeByName(Identifier &tn);
				virtual	Base::int_bool  GetAttrAsGC       (Identifier &arg,GCObjRef &ret);
				virtual	Base::int_bool	isTemplate		  ();
				virtual AbsRef			Specialize        (Specializer &spc);
				virtual PROCEDURE		keep_as_text      (TextKeeper& tk);
				virtual PROCEDURE		Serealize         (Parcing::TBasket &b);
				virtual Base::int_bool	isOneLined		  ();
				virtual Base::int_bool	HaveChild         (AbsRef a);
				virtual PROCEDURE		PrnWayToChild     (TextKeeper& tk,AbsRef a);
				virtual Base::int_32	ItemsOffset		  (Base::int_mem idx);  // -1 not found
				virtual Base::int_32	ItemsOffset       (Identifier &itname); // -1 not found
			//  ---------------------------------------------------
						Index			getItemIndex	  (Identifier id);
						Base::int_bool	HaveNamedItem     (Identifier id);
						GCObjRef		getConstrcutor	  ();
			//  ---------------------------------------------------
				static  AbsRef			Deserealize       (StreamReader* sr);
			//  ---------------------------------------------------
		};
		// --------------------------------------------------------
		class Union:public Abstract
		{
			public:
									DECL_NEW_DELETE(Union,MINPOOLSIZE,Base::MW::CItemPool);
				AbsRef::Hash 		item_desc;
			//  ---------------------------------------------------
						CONSTRUCTOR	    Union();
				virtual DESTRUCTOR	    ~Union();
			//  ---------------------------------------------------
				virtual Size			getDataSize();
				virtual Base::int_bool  GetAttrAsGC(Identifier &arg,GCObjRef &ret);
				virtual Base::int_bool	isTemplate();
				virtual Reference		Specialize(Specializer &spc);
				virtual PROCEDURE		keep_as_text(TextKeeper& tk);
				virtual PROCEDURE		Serealize(Parcing::TBasket &b);
				virtual Base::int_bool	isOneLined();
				virtual Base::int_32	ItemsOffset(Base::int_mem idx);  // -1 not found
				virtual Base::int_32	ItemsOffset(Identifier &itname); // -1 not found
			//  ---------------------------------------------------
				static  AbsRef			Deserealize(StreamReader* sr);
		};
		// --------------------------------------------------------
		class Extern:public Abstract
		{
			public:
									DECL_NEW_DELETE(Extern,MINPOOLSIZE,Base::MW::CItemPool);
				Identifier			name;
			//  ---------------------------------------------------
						CONSTRUCTOR	    Extern();
				virtual DESTRUCTOR		~Extern();
			//  ---------------------------------------------------
				virtual Size			getDataSize		();
				virtual Base::int_bool  GetAttrAsGC(Identifier &arg,GCObjRef &ret);
				virtual Reference		Specialize(Specializer &spc);
				virtual PROCEDURE		keep_as_text(TextKeeper& tk);
				virtual Base::int_bool	isOneLined();
			//  ---------------------------------------------------
		};
		// --------------------------------------------------------
		class Template
		{
			public:
									DECL_NEW_DELETE(Template,MINPOOLSIZE,Base::MW::CItemPool);
				typedef enum ArgType
				{
					ArgDataDesc =0,
					ArgNum		=1
				}ArgType;
			//  ---------------------------------------------------
				typedef struct ArgDesc
				{
					Identifier name;
					ArgType    typ;
				}ArgDesc;
			//  ---------------------------------------------------
				typedef struct ArgDescWork:public ArgDesc
				{
					Base::int_32 uses;
					AbsRef		 ExtLnk;
				};
			//  ---------------------------------------------------
				typedef	Base::Templates::_st_darr<ArgDesc,MINPOOLSIZE,Base::MW::GarbagePool> ArgList;
				typedef	Base::Templates::_st_darr<ArgDescWork,MINPOOLSIZE,Base::MW::GarbagePool> ArgListWork;
			//  ---------------------------------------------------
				ArgList			args;
				Parcing::CItem::_headArgList* hal;
				AbsRef			body;
			//  ---------------------------------------------------
				CONSTRUCTOR	        Template();
				DESTRUCTOR	        ~Template();
			//  ---------------------------------------------------
				ArgDesc* FindArgDesc(Identifier id);

				static Base::int_bool  prepareSingleArg(ArgListWork &dst,ArgDesc& ad,GCObjRef &gco);

				Parcing::CItem::_headArgList* getHAL();
				Base::int_bool	pushArg(Identifier arg,ArgType t);
				Base::int_bool	HaveArg(Identifier& arg);
				PROCEDURE		pushPartialArgs(ArgList & al, ArgListWork &alw);

				static Base::int_bool	ArgListWork_have(ArgListWork & alw,Identifier id);

				Base::int_bool	prepareArgs(GCObjRef &src,ArgListWork &dst);
				Base::int_bool	prepareDataDescA(GCObjRef &src,AbsRef &dst);
				Base::int_bool	prepareDataDesc(GCObjRef &src,GCObjRef &dst);

				Base::int_bool	partialprepareArgs(GCObjRef &src,ArgListWork &dst);
				Template*		partialSpecialize(GCObjRef &src);
				PROCEDURE		keep_as_text(Parcing::TBasket& b);
		};
		// --------------------------------------------------------
		class Specializer
		{
			friend class Template;

				Template::ArgListWork   args;
				AbsRef::Array src;
				AbsRef::Array dst;
			public:
				DECL_NEW_DELETE(Specializer,MINPOOLSIZE,Base::MW::CItemPool);
			//  ---------------------------------------------------
				CONSTRUCTOR	        Specializer();
				DESTRUCTOR	        ~Specializer();
			//  ---------------------------------------------------
				AbsRef isSpec(AbsRef s);
				PROCEDURE pushSpec(AbsRef s,AbsRef d);
				AbsRef	getClass(Identifier& arg);
				Base::int_32	getNum(Identifier& arg);
		};
		// --------------------------------------------------------
		class TextKeeper
		{
			public:
									DECL_NEW_DELETE(TextKeeper,MINPOOLSIZE,Base::MW::CItemPool);
			//  ---------------------------------------------------
				CONSTRUCTOR	        TextKeeper();
				DESTRUCTOR	        ~TextKeeper();
			//  ---------------------------------------------------
				AbsRef::Array stack;
				AbsRef::Array keept;
				PROCEDURE push(AbsRef a);
				PROCEDURE pop();
				PROCEDURE keeped(AbsRef a);
				Base::int_bool isKeepd(AbsRef a);
				Parcing::TBasket* b;

				Base::int_bool TryFindLinkAndPrint(AbsRef a);

				Base::int_bool isTopLink;
				PROCEDURE PutLnkDiv();

				Base::int_16u BS(Base::int_16u st,Base::pchar_8 str);
				PROCEDURE BE(Base::int_16u i);
				PROCEDURE keepHash(AbsRef::Hash &s,Base::int_bool  td);
		};
		// --------------------------------------------------------
		PROCEDURE	init();
	};
	// ------------------------------------------------------------
	namespace DataContaineer
	{
		// --------------------------------------------------------
		class RAW
		{
			private:
				RAW();	//	never call use only Create
				~RAW();	//	never call use only Free
				Size size;
			public:
				Size getSize();
				Ptr	getDataPtr(Offset ofs=0);
				Offset	getDataOffset(Ptr ptr);
				Base::int_bool	inMyData(Ptr p,Size sz=0);
			static	Ptr			NewPtr(Ptr p,Offset ofs);
			static	RAW*		Create(Size sz);
			static	RAW*		CreateWithData(Ptr p,Size sz);
			static	PROCEDURE	Free(RAW* raw);
		};
		// --------------------------------------------------------
		class Mapper
		{
			public:
				   DECL_NEW_DELETE(Mapper,MINPOOLSIZE,Base::MW::CItemPool);
			//  ---------------------------------------------------
				   CONSTRUCTOR	        Mapper();
				   DESTRUCTOR	        ~Mapper();
			//  ---------------------------------------------------
			typedef std::function<Base::int_16(DataDesc::AbsRef struc, Ptr l,Ptr r,Size strucsize)> CompareFunc;
			//  ---------------------------------------------------
				   Ptr					data;
				   GCObjRef				owner;	// allways	point to RAW viw GC
				   DataDesc::AbsRef		struc;
			//  ---------------------------------------------------
			inline Base::_pvoid			getDataPtr()
				   {
						return data;
				   };
				   Size                	getDataOffset();
				   Size                	getSize()
				   {
						return (struc!=nullptr)?struc->getDataSize():0;
				   };

				   template< typename t> inline t* Cast() { return ((t*)data);};
				   template< typename t> inline Base::int_bool CanCast(){return data && getSize()==sizeof(t);};
				   template< typename t> inline Base::int_bool CanCastAsArray()
				   {
						if(struc!=nullptr)
							if(struc->Dtype()==struc::DataDesc::Class::Array)
								if(((struc::DataDesc::Array*)(struc::DataDesc::Abstract*)struc)->item_desc->getDataSize()==sizeof(t))
								   return true;
						return false;
				   };
			   
				   template< typename t> inline t* CastAsArray(Base::int_mem &sz)
				   {
					   if(struc!=nullptr)
						   if(struc->Dtype()==struc::DataDesc::Class::Array)
							   if(((struc::DataDesc::Array*)(struc::DataDesc::Abstract*)struc)->item_desc->getDataSize()==sizeof(t))
							   {
								   sz=((struc::DataDesc::Array*)(struc::DataDesc::Abstract*)struc)->array_len;
								   return  ((t*)data);
							   };
					   return nullptr;
				   };
		           Mapper*			getArrayRange(Base::int_mem base_index,Base::int_mem count,Base::int_bool newraw=false); 
			       Mapper*          getItemMap(GCObjRef & arg);
				   Mapper*          getArrayItemMap(Base::int_32 idx);
				   Mapper*			reverse();
			static Mapper* 			ReadStruct(Base::pchar_8 fname,DataDesc::AbsRef item);
			static Mapper* 			Read1D(Base::pchar_8 fname,DataDesc::AbsRef item,Size len);
			static Mapper* 			Read2D(Base::pchar_8 fname,DataDesc::AbsRef item,Size rows,Size rowlen);
			static Mapper*          CreateByCopyData(DataDesc::AbsRef str,Ptr p,Size sz);
				   Mapper* 			FlatArray();
				   RAW*				Serealize();
				   PROCEDURE qSortI(CompareFunc cf);
			static Mapper* 			Deserealize(StreamReader* sr);
			static Base::int_bool   getSmpOrMap(GCObjRef &own,Ptr p,struc::DataDesc::AbsRef sd, GCObjRef &ret,Base::int_32u md=0);
		};
		// --------------------------------------------------------
		class DynamicArray
		{
			DECL_NEW_DELETE(DynamicArray,MINPOOLSIZE,Base::MW::CItemPool);
			//  ---------------------------------------------------
			friend class struc::GC::DataStorage;
			protected:
			class Segment
			{
				DECL_NEW_DELETE(Segment,MINPOOLSIZE,Base::MW::CItemPool);
				//  -----------------------------------------------
				Ptr					data;
				GCObjRef			owner;	// allways	point to RAW viw GC
				Size				base;
				Size				count;
				Size				itemSize;
				Segment*			prev;
				Segment*			next;
				//  -----------------------------------------------
				Ptr					getPtr(Size i);
				Base::int_32		getIdx(Ptr ptr);
				//  -----------------------------------------------
				CONSTRUCTOR	        Segment (Size c,Size is);
				DESTRUCTOR	        ~Segment();
				static	Segment*	Create	(Size c,Size is);
				//  -----------------------------------------------
			};
			//  ---------------------------------------------------
				CONSTRUCTOR	        DynamicArray();
				virtual DESTRUCTOR __stdcall ~DynamicArray();
			//  ---------------------------------------------------
				Size				ACOUNT;
				Size				TCOUNT;
				Size				segIC;
				Size				segIS;
				DataDesc::AbsRef	struc;
				Segment*			first;
				Segment*			last;
			//  ---------------------------------------------------
				Segment*			getSeg(Size i);
//				Ptr	                getPtr(Size i);
			//  ---------------------------------------------------
				Segment*			Extend();
				Base::int_bool		Reset();
				PROCEDURE			clear();
				Segment*			popFirstSeg(Size *actcnt);
				PROCEDURE			popFS_as_Mapper(GCObjRef & ret);
				Base::int_bool      push(Ptr p,Size s,Size *idxret=nullptr);
				Ptr					allocate(Size *idxret=nullptr); //reserve item place
				GCObjRef			usr;
			//  ---------------------------------------------------
			public:
			//  ---------------------------------------------------
				inline Size				Count(){return ACOUNT;};
				Base::int_bool		  getGCItem	     (Base::int_32 idx,GCObjRef &ret);
			//  ---------------------------------------------------
				virtual	Base::int_bool	__stdcall	GetChild		(Parcing::_ident &arg, Base::int_32u md, GCObjRef &ret);
				virtual	PROCEDURE		__stdcall	OperateSetId	(Parcing::_ident &trg, Base::int_32u md, GCObjRef &right, GCObjRef &ret);
			//  ---------------------------------------------------
				Base::int_32		 getIdx(Ptr ptr);
			static DynamicArray*	Create(DataDesc::AbsRef item,Size SegSize=256);
		};
		// --------------------------------------------------------
	};
	// ------------------------------------------------------------
	namespace parce
	{
		PROCEDURE build(Parcing::TCharParce &_p,Parcing::_lexem &_l);
		DataDesc::AbsRef	  build(Base::pchar_8 src);
		Base::int_bool		  buildWithMsgs(Base::pchar_8 src,Base::pchar_8 cap,DataDesc::AbsRef &ret,Base::int_mem wsz,Base::int_mem *size);
		DataDesc::Template*	  buildTemplate(Base::pchar_8 src);
		DataDesc::AbsRef	  buildTemplate(Base::pchar_8 src,GCObjRef args);
		Base::int_bool		  buildTemplateWithMsgs(Base::pchar_8 src,Base::pchar_8 cap,DataDesc::AbsRef &ret,GCObjRef args,Base::int_mem wsz,Base::int_mem *size);
	};
	// ------------------------------------------------------------
	namespace GC
	{
		PROCEDURE	init();
		PROCEDURE	setValueDataDesc(DataDesc::AbsRef	&src, GCObjRef &dst);
		PROCEDURE	setValueTemplate(DataDesc::Template* src, GCObjRef &dst);
		Base::int_bool	compileSomeDeclaration(GCObjRef &ret,Base::pchar_8 src);
		Base::_pvoid CreateMapper(DataDesc::AbsRef s,GCObjRef &dst,Base::_pvoid src=nullptr,GCObjRef* gcraw=nullptr);
		
		Base::int_bool CreateMapperInplace(DataDesc::AbsRef s,GCObjRef &dst,Base::_pvoid ep , GCObjRef gcraw);

		PROCEDURE	done();
	};
	// ------------------------------------------------------------
};
// ----------------------------------------------------------------
template<>
class GCAWsingle<struc::DataDesc::AbsRef>
{
	private:
		struc::DataDesc::AbsRef dd;
	public:
		Base::int_bool init(GCObjRef &args, Base::int_mem idx, Parcing::_ident idxn, Base::pchar_8 tname,Base::int_8u imp=1)
		{
			GCObjRef ref;
			if(GCTypescope::getItem(args,idx,idxn,ref))
			{
				GCT::TODLock lock(ref);
				if(struc::Ptr obj=lock.Obj<void>(GCTSDDname_DataDesc))
				{
					dd=*((struc::DataDesc::AbsRef*)&obj);
					return true;
				};
			};
			return !imp;
		};
		Base::int_bool initB(GCObjRef &src)
		{
			GCT::TODLock lock(src);
			if(struc::Ptr obj=lock.Obj<void>(GCTSDDname_DataDesc))
			{
				dd=*((struc::DataDesc::AbsRef*)&obj);
				return true;
			};
			return false;
		};
		inline struc::DataDesc::AbsRef operator ()(){ return dd;};
};
// ---------------------------------------------------------------------
template<> class GCAWsingle<struc::DataContaineer::Mapper>
{
	private:
		GCObjRef ref;
		struc::DataContaineer::Mapper* obj;
	public:
		 GCAWsingle():obj(nullptr){};
		Base::int_bool init(GCObjRef &args, Base::int_mem idx,
          Parcing::_ident idxn, Base::pchar_8 tname,Base::int_8u imp=1)
		{
			GCObjRef tmpref;
			if(GCTypescope::getItem(args,idx,idxn,tmpref))
			{
				if(initB(tmpref))
					return true;
			}else obj=nullptr;
			return !imp;
		};
		Base::int_bool initB(GCObjRef &src)
		{
			obj=nullptr;
			GCT::TODLock lock(src);
			struc::DataContaineer::Mapper* tmp=lock.Obj<struc::DataContaineer::Mapper>(GCTSDDname_Mapper);
			obj=tmp;
			if(tmp)
			{
				ref=src;
				return true;
			};
			return false;
		};
		GCAWsingle(GCObjRef &src):obj(0){initB(src);};
		inline struc::DataContaineer::Mapper* operator ()(){ return obj;};
		GCObjRef	getRef(){return ref;};
};
// ----------------------------------------------------------------------
#define DMTemplateFor(mytype) \
template<> class GCAWsingle<mytype> \
{\
	private:\
		GCObjRef ref;\
		mytype* obj;\
	public:\
		 GCAWsingle():obj(nullptr){};\
		Base::int_bool init(GCObjRef &args, Base::int_mem idx, \
          Parcing::_ident idxn, Base::pchar_8 tname,Base::int_8u imp=1)\
		{\
			GCObjRef tmpref;\
			if(GCTypescope::getItem(args,idx,idxn,tmpref))\
			{\
				if(initB(tmpref))\
					return true;\
			}else obj=nullptr;\
			return !imp;\
		};\
		Base::int_bool initB(GCObjRef &src)\
		{\
			obj=nullptr;\
			GCT::TODLock lock(src);\
			if(struc::DataContaineer::Mapper* tmp=\
			lock.Obj<struc::DataContaineer::Mapper>(GCTSDDname_Mapper))\
			if(tmp->CanCast<mytype>())\
			{\
				obj=tmp->Cast<mytype>();\
				ref=src;\
				return true;\
			};\
			return false;\
		};\
		GCAWsingle(GCObjRef &src):obj(0){initB(src);};\
		inline mytype* operator ()(){ return obj;};\
		GCObjRef	getRef(){return ref;};\
};
// ----------------------------------------------------------------------
