
#pragma once
//-------------------------------------------------------
#include "lcommonA.h"
#include "Internal.h"
#include "MW.h"
//-------------------------------------------------------
#define MP_JOB_INIT		0x0001
#define MP_JOB_INAIR	0x0002
#define MP_JOB_DONE		0x0003
#define MP_INFINITE		INFINITE
#define MP_CREATE_SUSPENDED CREATE_SUSPENDED
//-------------------------------------------------------
namespace MP
{
	//---------------------------------------------------
	class SystemHandle
	{
		public:
			HANDLE	hnd;
			// ----------------------------
			SystemHandle(HANDLE _hnd = 0);
			~SystemHandle();
			PROCEDURE ClearHandle();
			Base::int_bool	WaitHandle(Base::int_32u time = INFINITE);
			// ----------------------------
	};
	//---------------------------------------------------
	#define MPThread_Created	0x00
	#define MPThread_Running	0x01
	#define MPThread_Finished	0x02
	//---------------------------------------------------
	class Thread: public SystemHandle
	{
		private:	volatile Base::int_8u    MPTHREADSTATE;
					DWORD			id;
		protected:
			static	DWORD			__stdcall	EnterPoint(Thread * th);
			virtual	PROCEDURE		code()=0;
			virtual	PROCEDURE		OnThreadBegin();
			virtual	PROCEDURE		OnThreadEnd();
					PROCEDURE       LinkWithMe();
		public:
					CONSTRUCTOR		Thread();
			virtual	DESTRUCTOR		~Thread();
					DWORD			GetId();
					HANDLE			GetHandle();
			static  DWORD			GetCurId();
			static  HANDLE			GetCurHandle();
					PROCEDURE		Start(bool inplace=false);
					DWORD			Suspend();
					DWORD			Resume();
					PROCEDURE		RunInPlace();
					Base::int_bool	JustCreated();
					Base::int_bool	IsRunning();
					Base::int_bool	IsFinished();
					Base::int_bool	Wait(Base::int_32u time=INFINITE);
			static	PROCEDURE		Sleep(DWORD msec);
					int				GetPriority();
					PROCEDURE		SetPriority(int priority);
	};
	//---------------------------------------------------
	#define ThreadSelfDestructor_Decl(n)	\
	class n:public MP::Thread	\
	{\
		virtual	PROCEDURE		code();	\
				CONSTRUCTOR		n(){};\
				virtual	PROCEDURE		OnThreadEnd();\
		public:\
		static Base::int_bool Start();\
	};
	//---------------------------------------------------
	#define ThreadSelfDestructor_Def(n)\
	PROCEDURE		n::OnThreadEnd()\
	{\
		delete this;\
	};\
	Base::int_bool n::Start()\
	{\
		if(auto t= new n())\
		{\
			t->Start();\
			return true;\
		};\
		return false;\
	};
	//---------------------------------------------------
	DECLARE_CLASSP(Mutex,Internal::Mutex,MINPOOLSIZE,Base::MW::CItemPool)
		public:
			Base::int_bool TryEnter();
			inline DWORD			getOwnerId()
			{
				return reinterpret_cast<DWORD>(cs.OwningThread);
			};
	};
	//---------------------------------------------------
	DECLARE_CLASSP(MutexLock,Internal::MutexLock,MINPOOLSIZE,Base::MW::CItemPool)
		private:
			CONSTRUCTOR		MutexLock();
		public:
			CONSTRUCTOR		MutexLock(Mutex & ref);
	};
	//---------------------------------------------------
	DECLARE_CLASSP(Event,SystemHandle,MINPOOLSIZE,Base::MW::CItemPool)
		private:
			CONSTRUCTOR		Event();
		public:
				CONSTRUCTOR		Event(Base::int_bool manual,Base::int_bool state=false);
				DESTRUCTOR		~Event();
				Base::int_bool	Wait(Base::int_32u time=INFINITE);
				PROCEDURE		Set();
				PROCEDURE		Reset();
				PROCEDURE		Pulse();
		static  Base::int_32u	WaitManyEvent(Event** ev, Base::int_8u cnt,Base::int_32u time,Base::int_bool waitall);
	};
	//---------------------------------------------------

	//---------------------------------------------------
};
//-------------------------------------------------------
