
#include "struc.h"
#include "_lexem.h"
#include "TLexManager.h"
#include "error_code.h"
#include "lcommonB.h"
#include "../base/logger.h"
#include "type_scope.h"
#include "_function.h"


#undef NEXT_LEX
#undef EQMA
#undef NEMA
#define NEXT_LEX  _p->scan_LM(*_l)
#define EQMA(t)	  _l->MAJOR()==t
#define PASS_EMPTY while(_l->MAJOR()==CJ_LEX_opr_dv){NEXT_LEX;};

												
												


				
					


#define STORE_POSITION	lp=_l->getPos();
#define CHECK_ERROR		if(ok!=ERROR_NoError)return nullptr;


//			CJ_LEX_CAL_rArgs     dlya vneshnix tipov   @

// ----------------------------------------------------------------
namespace struc
{
	// ------------------------------------------------------------
	namespace parce
	{
		// --------------------------------------------------------
		class strucparcer
		{
			// ----------------------------------------------------
			public:
				// ------------------------------------------------
				Parcing::TCharParce *_p;
				Parcing::_lexem     *_l;
				// ------------------------------------------------
				Parcing::TBuffer::TJ_Buff_position lp;//lp=_l->getPos();
				Base::ERROR_code ok;// =ERROR_NoError;
				// ------------------------------------------------
				Base::int_bool isAllow_fld_name(Identifier &id)
				{
					//long
					if(id=="__size"		)return false;
					if(id=="__type"		)return false;
					if(id=="__item"		)return false;
					if(id=="__itemtype"	)return false;
					if(id=="__itemsize"	)return false;
					if(id=="__desc"		)return false;
					if(id=="__extname"	)return false;
					if(id=="__count"	)return false;
					if(id=="__typecount")return false;
					if(id=="__raw"		)return false;

					// short
					if(id=="__s"		)return false;
					if(id=="__t"		)return false;
					if(id=="__i"		)return false;
					if(id=="__it"		)return false;
					if(id=="__is"		)return false;
					if(id=="__d"		)return false;
					if(id=="__en"		)return false;
					if(id=="__c"		)return false;
					if(id=="__tc"		)return false;
					if(id=="__r"		)return false;

					return true;
				};
				// ------------------------------------------------
				// ---				STACK						---
				// ------------------------------------------------
				DataDesc::AbsRef::Array stack;
				// ------------------------------------------------
				PROCEDURE pop()
				{
					if(stack._size())
						stack._delete(stack._size()-1);
				};
				// ------------------------------------------------
				PROCEDURE push(DataDesc::AbsRef ref)
				{
					stack._add(ref);
				};
				// ------------------------------------------------
				DataDesc::AbsRef getTypeByName(Identifier &si)
				{
					Base::int_32 sz=stack._size();
					for(Base::int_32 i=sz-1;i>=0;--i)
						if(auto sd=(*stack[i])->getChildTypeByName(si))
							return sd;
					return nullptr;
				};
				// ------------------------------------------------
				// ---				TEMPLATE					---
				// ------------------------------------------------
				DataDesc::Template::ArgListWork		tplarg;
				// ------------------------------------------------
				PROCEDURE		FILL(DataDesc::Template::ArgList &al)
				{
					DataDesc::Template::ArgDescWork a;
					for(Base::int_mem i=0;i<al._size();++i)
					{
						a.uses=0;
						a.name=al[i]->name;
						a.typ =al[i]->typ;
						if (al[i]->typ==DataDesc::Template::ArgDataDesc)
						{
							a.ExtLnk= new DataDesc::Extern();
							((DataDesc::Extern*)(DataDesc::Abstract*)a.ExtLnk)->name=a.name;
						}
						else
						{
							a.ExtLnk=nullptr;
						};
						tplarg._add(a);
					};
				};
				// ------------------------------------------------
				DataDesc::AbsRef	getExtern(Identifier &si)
				{
					for(Base::int_mem i=0;i<tplarg._size();++i)
					{
						if(tplarg[i]->name==si)
							if(tplarg[i]->typ==DataDesc::Template::ArgDataDesc)
							{
								tplarg[i]->uses++;
								return tplarg[i]->ExtLnk;
							}
							else
							{
								ok=ERROR_STRUCT_TmplNoClass;
								return nullptr;
							};

					};
					return nullptr;
				};
				// ------------------------------------------------
				Base::int_bool	allowNumSpec(Identifier &si)
				{
					for(Base::int_mem i=0;i<tplarg._size();++i)
					{
						if(tplarg[i]->name==si)
							if(tplarg[i]->typ==DataDesc::Template::ArgNum)
							{
								tplarg[i]->uses++;
								return true;
							}
							else
							{
								ok=ERROR_STRUCT_TmplNoNum;
								return false;
							};
					};
					return false;
				};
				// ------------------------------------------------
				Base::int_bool	AllowTemplation;
				// ------------------------------------------------
				// ---				PARCERS						---
				// ------------------------------------------------
				DataDesc::AbsRef ParceWWType()
				{
					STORE_POSITION
					switch (_l->MAJOR())
					{
						case	CJ_LEX_SimpleType	:
							{
								DataDesc::Class::Simple cs=(DataDesc::Class::Simple)_l->MINOR();
								NEXT_LEX;
								return DataDesc::Simple::getSimple(cs);
							};
						case	CJ_LEX_StrucType	:
							switch ((DataDesc::Class::Desc)_l->MINOR())
							{
								case DataDesc::Class::Struct:		return ParceStruct();
								case DataDesc::Class::Union:		return ParceUnion();
								default:ok=ERROR_STRUCT_unkStrType2;
							};
						break;
						default:ok=ERROR_STRUCT_unkStrType1;
					};
					return nullptr;
				};
				// ------------------------------------------------
				DataDesc::AbsRef ParceStruct()
				{
					STORE_POSITION
					if((_l->MAJOR()==CJ_LEX_StrucType)&&(((DataDesc::Class::Desc)_l->MINOR())==DataDesc::Class::Struct))
					{
						NEXT_LEX;
						STORE_POSITION;
						if (EQMA(CJ_LEX_opr_bs))
						{
							NEXT_LEX;
							PASS_EMPTY;
							DataDesc::AbsRef ret;
							if(ret= new DataDesc::Struct())
							{
								push(ret);
								#define rr ((DataDesc::Struct*)(DataDesc::Abstract*)ret)
								while(ok==ERROR_NoError)
								{
									STORE_POSITION;
									if((EQMA(CJ_LEX_exprifc))&&(_l->MINOR()==CSCAOp_expc_else))
									{
										NEXT_LEX;
										if((_l->MAJOR()==CJ_LEX_ident)||(_l->MAJOR()==CJ_LEX_identWW))
										{
											Identifier funname=_l->IDENT();
											NEXT_LEX;
											Parcing::CItem::_function_prep::build_ruby(*_p,*_l,0);
											if (_l->IsObj(CJ_LOBJ_function_prep))
											{
												if(auto f=(Parcing::CItem::_function_prep*)_l->_detachB())			// �������� ������ �������
												{
													GCObjRef funret;
													GCT::Object_CreateR(funret,GCTTSname_function_prep,(void**)&f);
													if (f)
													{
														delete f;
														ok=ERROR_STRUCT_FuncGCWrap;
													}
													else
														if (!rr->HaveNamedItem(funname))
														{
															rr->fun_desc._add(funname,funret);
														}
														else ok=ERROR_STRUCT_DupNameFun;
												}else ok=ERROR_STRUCT_FuncDetach;
											}else ok=ERROR_STRUCT_FuncCompl;
										}else ok=ERROR_STRUCT_FuncNameExp;
									}else
									{
										Base::int_bool _typedef=false;
										if(_l->MAJOR()==CJ_LEX_typedef)
										{
											NEXT_LEX;
											_typedef=true;
										};
										DataDesc::AbsRef ch=ParceType();
										CHECK_ERROR
										if(ch!=nullptr)
										{
											if((_l->MAJOR()==CJ_LEX_ident)||(_l->MAJOR()==CJ_LEX_identWW))
											{
												Identifier idd=_l->IDENT();
												if(isAllow_fld_name(idd))
												{
													if(_typedef)
													{
														if (!rr->HaveNamedItem(idd))
														{
															rr->sub_desc._add(idd,ch);
														}else ok=ERROR_STRUCT_DupNameSub;
													}else
													{
														if (!rr->HaveNamedItem(idd))
														{
															rr->item_desc._add(idd,ch);
														}else ok=ERROR_STRUCT_DupNameItm;
													};
												}else ok=ERROR_STRUCT_RswIdent;
											}else ok=ERROR_STRUCT_IdntExp;
										}else ok=ERROR_STRUCT_SubDDexp;
									};
									CHECK_ERROR
									NEXT_LEX;
									Base::int_8u don=0;
									if (EQMA(CJ_LEX_opr_dv))
									{
										PASS_EMPTY;
										if ((EQMA(CJ_LEX_opr_be))||(EQMA(CJ_LEX_eof)))
										{
											NEXT_LEX;
											don=1;
										};
									}else
										if ((EQMA(CJ_LEX_opr_be))||(EQMA(CJ_LEX_eof)))
										{
											NEXT_LEX;
											don=1;
										}else ok=ERROR_STRUCT_be_exp1;
									CHECK_ERROR
									if(don)
									{
										pop();
										return ret;
									};
								};
								#undef rr
							}else ok=ERROR_STRUCT_DDMemNoAllc;
						}else ok=ERROR_STRUCT_bs_exp1;
					}else ok=ERROR_STRUCT_WCP_Struct;
					return nullptr;
				};
				// ------------------------------------------------
				DataDesc::AbsRef ParceUnion()
				{
					STORE_POSITION
					if((_l->MAJOR()==CJ_LEX_StrucType)&&(((DataDesc::Class::Desc)_l->MINOR())==DataDesc::Class::Union))
					{
						NEXT_LEX;
						STORE_POSITION;
						if (EQMA(CJ_LEX_opr_bs))
						{
							NEXT_LEX;
							PASS_EMPTY;
							if(DataDesc::AbsRef ret= new DataDesc::Union())
							{
								while(ok==ERROR_NoError)
								{
									STORE_POSITION;
									DataDesc::AbsRef ch=ParceType();
									CHECK_ERROR
									if(ch!=nullptr)
									{
										if((_l->MAJOR()==CJ_LEX_ident)||(_l->MAJOR()==CJ_LEX_identWW))
										{
											Identifier idd=_l->IDENT();
											if(isAllow_fld_name(idd))
											{
												#define uu ((DataDesc::Union*)(DataDesc::Abstract*)ret)
												if (uu->item_desc[idd]==nullptr)
												{
													uu->item_desc._add(idd,ch);
												}else ok=ERROR_STRUCT_DupNameItm2;
											}else ok=ERROR_STRUCT_RswIdent2; 
											#undef uu
											CHECK_ERROR
											NEXT_LEX;
											STORE_POSITION;
											Base::int_8u don=0;
											if (EQMA(CJ_LEX_opr_dv))
											{
												PASS_EMPTY;
												if (EQMA(CJ_LEX_opr_be))
												{
													NEXT_LEX;STORE_POSITION;
													don=1;
												};
											}else
												if (EQMA(CJ_LEX_opr_be))
												{
													NEXT_LEX;STORE_POSITION;
													don=1;
												}else ok=ERROR_STRUCT_be_exp2;

											CHECK_ERROR
											if(don)
												return ret;
										}else ok=ERROR_STRUCT_IdntExp2;
									}else ok=ERROR_STRUCT_SubDDexp2;
								};
							}else ok=ERROR_STRUCT_DDMemNoAllc2;
						}else ok=ERROR_STRUCT_bs_exp2;					
					}else ok=ERROR_STRUCT_WCP_Union;
					return nullptr;
				};
				// ------------------------------------------------
				DataDesc::AbsRef ParceArrayArg()
				{
					STORE_POSITION;
					DataDesc::AbsRef ret=nullptr;
					if(_l->MAJOR()==CJ_LEX_idx_bs)
					{
						if(DataDesc::AbsRef ret= new DataDesc::Array())
						{
							NEXT_LEX;
							STORE_POSITION;
							#define aa	((DataDesc::Array*)(DataDesc::Abstract*)ret)
							switch(_l->MAJOR())
							{
								case CJ_LEX_digit:
									{
										Base::int_32 len=_l->LexToInt();
										if(len>0)
										{
											aa->array_len=len;
										}else ok=ERROR_STRUCT_WrLen;
									};break;
								case CJ_LEX_CAL_rArgs:
									if(AllowTemplation)
									{
										NEXT_LEX;
										STORE_POSITION;
										if((_l->MAJOR()==CJ_LEX_ident)||(_l->MAJOR()==CJ_LEX_identWW))
										{
											Identifier i=_l->IDENT();
											if(allowNumSpec(i))
											{
												aa->ExtName=i;
											}
											else ok=(ok==ERROR_NoError)?ERROR_STRUCT_ExtNoFnd2:ok;
										}
										else ok=ERROR_STRUCT_ExIdnExp2;
									}
									else ok=ERROR_STRUCT_TmplNoAllow2;
									break;
								default: ok=ERROR_STRUCT_unk_stmt2;
							};
							#undef aa
							CHECK_ERROR;
							NEXT_LEX;
							STORE_POSITION;
							if(_l->MAJOR()==CJ_LEX_idx_be)
							{
								NEXT_LEX;
								return ret;
							}else ok=ERROR_STRUCT_ae_exp;
						}else ok=ERROR_STRUCT_DDMemNoAllc3;
					};
					return nullptr;
				};
				// ------------------------------------------------
				DataDesc::AbsRef ParceArray(DataDesc::AbsRef item)
				{
					if(item!=nullptr)
					{
						STORE_POSITION
						DataDesc::AbsRef up;
						DataDesc::AbsRef cor;
						while(ok==ERROR_NoError)
						{
							DataDesc::AbsRef sd=ParceArrayArg();
							CHECK_ERROR;
							if(sd!=nullptr)
							{
								#define aa	((DataDesc::Array*)(DataDesc::Abstract*)cor)
								if(up==nullptr)
								{
									up=sd;
									cor=sd;
								}else
								{
									aa->item_desc=sd;
									cor=sd;
								};
							}else
								if(up==nullptr)
								{
									return item;
								}else
								{
									aa->item_desc=item;
									return up;
								};
								#undef aa
						};
					}else ok=ERROR_STRUCT_WrClArrArg;
					return nullptr;
				};
				// ------------------------------------------------
				DataDesc::AbsRef ParceExt()
				{
					STORE_POSITION
					if(AllowTemplation)
					{
						if(_l->MAJOR()==CJ_LEX_CAL_rArgs)
						{
							NEXT_LEX;
							STORE_POSITION
							if((_l->MAJOR()==CJ_LEX_ident)||
							   (_l->MAJOR()==CJ_LEX_identWW))
							{
								auto ex=getExtern(_l->IDENT());
								if(ex!=nullptr)
								{
									NEXT_LEX;
									return ex;
								}else ok=(ok==ERROR_NoError)?ERROR_STRUCT_ExtNoFnd:ok;
							}else ok=ERROR_STRUCT_ExIdnExp;
						}else ok=ERROR_STRUCT_ExSymExp;
					}else ok=ERROR_STRUCT_TmplNoAllow;
					return nullptr;
				};
				// ------------------------------------------------
				DataDesc::AbsRef ParceLink()
				{
					DataDesc::AbsRef ret=nullptr;
					while(ok==ERROR_NoError)
					{
						STORE_POSITION
						if((_l->MAJOR()==CJ_LEX_ident)||
						   (_l->MAJOR()==CJ_LEX_identWW))
						{
							if(ret==nullptr)
							{
								ret=getTypeByName(_l->IDENT());
							}else
							{
								ret=ret->getChildTypeByName(_l->IDENT());
							};
							if(ret!=nullptr)
							{
								NEXT_LEX;
								if(_l->MAJOR()==CJ_LEX_UseNameSpace)
								{
									NEXT_LEX;
								}else
								{
									return ret;
								};
							}else ok=ERROR_STRUCT_TypeNoFnd;
						}
						else ok=ERROR_STRUCT_WrTypeIdnt;
					};
					return nullptr;
				};
				// ------------------------------------------------
				DataDesc::AbsRef ParceType(Base::int_bool allowLnk=1)
				{
					DataDesc::AbsRef ret;
					switch (_l->MAJOR())
					{
						case	CJ_LEX_SimpleType	:;
						case	CJ_LEX_StrucType	: ret=ParceWWType();
						break;
						case	CJ_LEX_ident		:;
						case	CJ_LEX_identWW		: if(allowLnk)
													  { 
														  ret=ParceLink();
													  }else ok=ERROR_STRUCT_LnkNallow;
						break;
						case	CJ_LEX_CAL_rArgs	: ret=ParceExt();
						break;
						default: ok=ERROR_STRUCT_unk_stmt;
					};
					CHECK_ERROR;

					ret=ParceArray(ret);

					CHECK_ERROR;
					if(ret!=nullptr)
						if(ret->getDataSize())
						{
							return ret;
						}else ok=ERROR_STRUCT_data_size_zero;
					return nullptr;

				};
				// ------------------------------------------------
				#define CHECK_ERROR_T	if(ok!=ERROR_NoError){ delete ret;return nullptr;};
				DataDesc::Template* ParceTemplateHead()
				{
					STORE_POSITION
					if(_l->MAJOR()==CJ_LEX_Template)
					{
						NEXT_LEX;
						STORE_POSITION;
						if ((EQMA(CJ_LEX_binr_op))&&(_l->MINOR()==CSCAOp_lt))
						{
							NEXT_LEX;
							STORE_POSITION;
							if(DataDesc::Template* ret=new DataDesc::Template())
							{
								while(ok==ERROR_NoError)
								{
									DataDesc::Template::ArgType  at= DataDesc::Template::ArgDataDesc;
									if ((EQMA(CJ_LEX_ident))&&(_l->MINOR()==66))
									{
										NEXT_LEX;
										STORE_POSITION;
										at= DataDesc::Template::ArgNum;
									};
									if((_l->MAJOR()==CJ_LEX_ident)||(_l->MAJOR()==CJ_LEX_identWW))
									{
										if(ret->pushArg(_l->IDENT(),at))
										{
											NEXT_LEX;
											STORE_POSITION;
											switch(_l->MAJOR())
											{
												case	CJ_LEX_binr_op:
													if(_l->MINOR()==CSCAOp_gt)
													{
														NEXT_LEX;
														return ret;
													}else ok=ERROR_STRUCT_Tpl_unkSt;
													break;
												case	CJ_LEX_arg_dv:NEXT_LEX;break;
												default: ok=ERROR_STRUCT_Tpl_unkSt;
											};
										}else ok=ERROR_STRUCT_TplDupArg;
									}else ok=ERROR_STRUCT_TplIdnExp;
								};
								CHECK_ERROR_T
							}else ok=ERROR_STRUCT_TplNoMemAlloc;
						}else ok=ERROR_STRUCT_TplLtExp;
					}else ok=ERROR_STRUCT_TplWrCal;
					return nullptr;
				};
				#undef CHECK_ERROR_T	
				// ------------------------------------------------
				PROCEDURE	parce()
				{
					switch(_l->MAJOR())
					{
						case CJ_LEX_StrucType:;
						case CJ_LEX_SimpleType:
								{
								AllowTemplation=false;
								auto p=ParceType(0);
								if(ok==ERROR_NoError)
								{
									p->AddRef();
									_p->returnTo(*_l);
									_l->clear();// �� ����������� ������� 
									_l->LEXEM_SET_OBJECT(CJ_LOBJ_DataDesc,CJLOpt_NotActive,(Parcing::TJ_Lex_date_ep)(DataDesc::Abstract*)p);// � ������� � ��� ���������� ������
									_l->setPos(lp);
									return;
								};
							};
						break;
						case CJ_LEX_Template:
							if(DataDesc::Template* dt=ParceTemplateHead())
							{
								FILL(dt->args);
								AllowTemplation=true;
								dt->body=ParceType(0);
								if(ok==ERROR_NoError)
								{
									_p->returnTo(*_l);
									_l->clear();// �� ����������� ������� 
									_l->LEXEM_SET_OBJECT(CJ_LOBJ_Template,CJLOpt_NotActive,(Parcing::TJ_Lex_date_ep)dt);// � ������� � ��� ���������� ������
									_l->setPos(lp);
									return;
								}else
								{
									delete dt;
								};
							};
						break;
						default:
							_l->PackLerror(ERROR_STRUCT_TplWrClP,_p->getPosB(lp),_p);
							return;
					};
					_l->PackLerror(ok,_p->getPosB(lp),_p);
				};
			// ----------------------------------------------------
		};
		// --------------------------------------------------------
		PROCEDURE build(Parcing::TCharParce &_p,Parcing::_lexem &_l)
		{
			strucparcer st;
			st._p=&_p;
			st._l=&_l;
			st.ok=ERROR_NoError;
			st.parce();
		};
		// --------------------------------------------------------
		DataDesc::AbsRef	  build(Base::pchar_8 src)
		{
			Parcing::TBasket			b(1);
			DataDesc::AbsRef ret=nullptr;

			Parcing::TCharParce::ParceSomeData(src,"build",CJLSCN_DEFAULT_OPTIONS,
							[&](Parcing::TLexManager& lm,Parcing::TCharParce& p,Parcing::_lexem  &l)->Base::int_bool
							{
								b.setLexManager(&lm);
								p.scan_LM(l);
								build(p,l);
								if(l.IsObj(CJ_LOBJ_DataDesc))
								{
									ret=(DataDesc::Abstract*)l._detachB();
									ret->Release();  //  released link in for lexem object incremented manually in strucparcer.parce()
									if(!p.eof())
									{
										p.scan_LM(l);
										l.PackLerror(ERROR_OPER_BLOCK_ODM,p.getPosB(p.getPos()),&p);
										b.printf("struc::parce::build(): Not fully source compiled\n");
									};
								};
								if(ret==nullptr)  l.keep_as_text(b);
								return true;
								},[&](Base::pchar_8 msg){ b.printf(msg);});

			if(ret==nullptr) b.log_output();
			return ret;
		};
		// --------------------------------------------------------
		Base::int_bool	buildWithMsgs(Base::pchar_8 src,Base::pchar_8 cap,DataDesc::AbsRef &ret,Base::int_mem wsz,Base::int_mem *size)
		{
			ret=build(src);
			if(ret==nullptr)
			{
				BL::Log(BL::Error,"Error: %s not compiled: %s\n",cap,src);
				return false;
			};
			Base::int_mem sz=ret->getDataSize();
			if(size) *size=sz;
			if(sz!=wsz)
			{
				BL::Log(BL::Error,"Error: %s struct size differs from the expected (%d != %d)\n",cap,sz,wsz);
				Parcing::TBasket b(1);
				ret->keep_as_text(b);
				b.log_output();
				return false;
			};
			return true;
		};
		// --------------------------------------------------------
		DataDesc::Template*	  buildTemplate(Base::pchar_8 src)
		{
			Parcing::TBasket			b(1);
			DataDesc::Template* ret=nullptr;

			Parcing::TCharParce::ParceSomeData(src,"build",CJLSCN_DEFAULT_OPTIONS,
							[&](Parcing::TLexManager& lm,Parcing::TCharParce& p,Parcing::_lexem  &l)->Base::int_bool
							{
								b.setLexManager(&lm);
								p.scan_LM(l);
								build(p,l);
								if(l.IsObj(CJ_LOBJ_Template))
								{
									ret=(DataDesc::Template*)l._detachB();
									if(!p.eof())
									{
										if(ret)
										{
											delete ret;
											ret=nullptr;
										};
										p.scan_LM(l);
										l.PackLerror(ERROR_OPER_BLOCK_ODM,p.getPosB(p.getPos()),&p);
										b.printf("struc::parce::buildTemplate(): Not fully source compiled\n");
									};
								};
								if(ret==nullptr)  l.keep_as_text(b);
								return true;
							},[&](Base::pchar_8 msg){ b.printf(msg);});

			if(ret==nullptr) b.log_output();
			return ret;
		};
		// --------------------------------------------------------
		DataDesc::AbsRef	  buildTemplate(Base::pchar_8 src,GCObjRef args)
		{
			DataDesc::Template*	  f=buildTemplate(src);
			if(f)
			{
				DataDesc::AbsRef a=nullptr;
				f->prepareDataDescA(args,a);
				delete f;
				return a;
			};
			return nullptr;
		};
		// --------------------------------------------------------
		Base::int_bool		  buildTemplateWithMsgs(Base::pchar_8 src,Base::pchar_8 cap,DataDesc::AbsRef &ret,GCObjRef args,Base::int_mem wsz,Base::int_mem *size)
		{
			ret=buildTemplate(src,args);
			if(ret==nullptr)
			{
				BL::Log(BL::Error,"Error: %s not compiled: %s\n",cap,src);
				return false;
			};
			Base::int_mem sz=ret->getDataSize();
			if(size) *size=sz;
			if(sz!=wsz)
			{
				BL::Log(BL::Error,"Error: %s struct size differs from the expected (%d != %d)\n",cap,sz,wsz);
				Parcing::TBasket b(1);
				ret->keep_as_text(b);
				b.log_output();
				return false;
			};
			return true;
		};
		// --------------------------------------------------------
	};
	// ------------------------------------------------------------
	namespace GC
	{
		Base::int_bool	compileSomeDeclaration(GCObjRef &ret,Base::pchar_8 src)
		{
			Parcing::TBasket	b(1);
			ret=nullptr;
			return Parcing::TCharParce::ParceSomeData(src,"build",CJLSCN_DEFAULT_OPTIONS,
							[&](Parcing::TLexManager& lm,Parcing::TCharParce& p,Parcing::_lexem  &l)->Base::int_bool
							{
								Parcing::_lexem ltmp;
								b.setLexManager(&lm);
								p.scan_LM(l);

								parce::build(p,l);
								if(!p.eof())
								{
									p.scan_LM(ltmp);
									l.PackLerror(ERROR_OPER_BLOCK_ODM,p.getPosB(p.getPos()),&p);
									b.printf("struc::GC::compileSomeDeclaration(): Not fully source compiled\n");
								};
								if(l.IsObj(CJ_LOBJ_DataDesc))
								{
									DataDesc::AbsRef rrr=(DataDesc::Abstract*)l._detachB();
									if(rrr!=nullptr)
									{
										rrr->Release();  //  released link in for lexem object incremented manually in strucparcer.parce()
										GC::setValueDataDesc(rrr,ret);
										return true;
									};
								};
								if(l.IsObj(CJ_LOBJ_Template))
								{
									if(auto ttt=(DataDesc::Template*)l._detachB())
									{
										GC::setValueTemplate(ttt,ret);
										return true;
									};
								};
								if(ret==nullptr)  l.keep_as_text(b);
								return false;
							},[&](Base::pchar_8 msg){ b.printf(msg);});
		};
	};
};
// ----------------------------------------------------------------
