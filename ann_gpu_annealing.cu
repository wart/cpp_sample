#include "ann_cuda.h"
#include "../Base/utils.h"
#include "ann_gpu_annealing.h"
// --------------------------------------------------------
#include "sigmoid.cu"
#include "dsigmoid.cu"
#include <math.h>
#include "rfis.h"
#define ACC ANN::CUDA::CheckError
// --------------------------------------------------------
#define LAYERMAXRANG		  127
#define AXONMAXSYNAPSIS       (LAYERMAXRANG+1)
#define MAX_LAYERS            64
#define MAX_SHARED_ARRAY      11264
// --------------------------------------------------------
__constant__  ANN::SIGMOID    GPU_SIG;
__constant__  ANN::Rang       GPU_RANG;
__constant__  ANN::Rang       GPU_MAXRANG;
__constant__  ANN::Rang       GPU_MAXRANGPLUSONE;
__constant__  ANN::Int		  GPU_FRAMECOUNT;
__constant__  ANN::Rang       GPU_LR[MAX_LAYERS];
// --------------------------------------------------------
__constant__  ANN::Axon*      GPU_INPUT;
__constant__  ANN::Axon*      GPU_OUTPUT;
__constant__  ANN::Axon*      GPU_LEARNINPUT;
__constant__  ANN::Axon*      GPU_LEARNOUTPUT;
__constant__  ANN::Axon*      GPU_OUTERR;
// --------------------------------------------------------
texture<float, 2, cudaReadModeElementType>  GPU_TEX_SYN;
// --------------------------------------------------------
#define reducesum(T,C,S,G,R) {for(C=S/2;C>0;C>>=1){if(T<C) G[T]+=G[T+C];__syncthreads();};if(T==0)R=G[0];};
#define         I    threadIdx.x
#define         SV   tex2D(GPU_TEX_SYN,S,B)
#define         R    GPU_LR
#define         K    0x01
#define         STP  T=(T&K)^K;P=(T&K)^K;
// --------------------------------------------------------
__global__ void KRNL_annealing_getERROR()
{
	__shared__  ANN::Axon       A[2][LAYERMAXRANG];   //axon
	__shared__  ANN::Axon       E   [LAYERMAXRANG];   // arror
	// ------------------------------------------------------------
	ANN::Int	T,P,L,F,S,B;// step, prevstep,layer,frame,synapse, sybaxofs,
	ANN::Float  M;          // sum
	// ------------------------------------------------------------
	F= blockIdx.x+gridDim.x*blockIdx.y;
	E[I]	= 0.0f;
	T		= 0;
	// ------------------------------------------------------------
	if(F<GPU_FRAMECOUNT){
		if(I<R[0])		A[0][I] = GPU_INPUT[R[0]*F+I];
		__syncthreads();
		for(L=1;L<GPU_RANG;++L){STP
			if(I<R[L])	{
				M=0.0;  B =GPU_MAXRANG*L+I;
				for(S=0; S<R[L-1]; ++S)  M += SV*A[P][S];	M += SV;
				A[T][I]= Sigmoid(GPU_SIG,M);
			};__syncthreads();
		};
		// --------------------------------------------------------
		if(I<R[GPU_RANG-1]) { M=A[T][I]-GPU_OUTPUT[R[GPU_RANG-1]*F+I];	E[I]+=M*M;	};__syncthreads();
		reducesum(I,L,blockDim.x,E,GPU_OUTERR[F]);
	};
};
#undef I
#undef K
#undef R
#undef SV
#undef STP
// --------------------------------------------------------
__global__ void KRNL_reduceError(ANN::Float* ret,ANN::Float* src)
{
	__shared__ ANN::Float data[MAX_SHARED_ARRAY];
	ANN::Int idx = blockDim.x*threadIdx.y+threadIdx.x;
	ANN::Int step  = blockDim.x*blockDim.y;
	data[idx]    = 0.0f;

	for(ANN::Int frm=idx;frm<GPU_FRAMECOUNT;frm+=step)	data[idx]+=src[frm];
	for(ANN::Int C=step/2;C>0;C>>=1)
	{
		if(idx<C)
			data[idx]+=data[idx+C];
		__syncthreads();
	};
	if(idx==0)
		ret[0]=data[0];
};
// ----------------------------------------------------------------
	 #define   new_ptr(p,ofs)  ((ANN::Float*)(&((Base::int_8u*)(p))[ofs]))
	 #define I  threadIdx.x
	 #define K    0x01
	 #define R  GPU_LR
     #define         STP  T=(T&K)^K;P=(T&K)^K;
__global__ void LearnFrame(	ANN::Int frame,
							ANN::Int pitch,
							ANN::Float* syn_in,
							ANN::Float* syn_out,
							ANN::Float niu
							)
{
	__shared__  ANN::Axon       A[MAX_LAYERS][LAYERMAXRANG];
	__shared__  ANN::Axon       E[2]		 [LAYERMAXRANG];
	int L,S,T,P;
	float M,*pS,*pSO;
	if(I<R[0])		A[0][I] = GPU_LEARNINPUT[R[0]*frame+I];__syncthreads();
	for(L=1;L<GPU_RANG;++L)
	{
		if(I<R[L])
		{
			pS =new_ptr(syn_in,pitch*(GPU_MAXRANG*L+I));
			M=0.0;
			for(S=0; S<R[L-1]; ++S)
				M += pS[S]*A[L-1][S];
			M += pS[S];
			A[L][I]= Sigmoid(GPU_SIG,M);
		};
		__syncthreads();
	};
	//-------------------------------------------------------  outer layer
	if(I<R[GPU_RANG-1])
	{
		E[0][I]= ( A[GPU_RANG-1][I]-GPU_LEARNOUTPUT[R[GPU_RANG-1]*frame+I]) * DSigmoid(GPU_SIG,A[GPU_RANG-1][I]);
		M  = niu*E[0][I];
		L=pitch*(GPU_MAXRANG*(GPU_RANG-1)+I);
		pS =new_ptr(syn_in,L);
		pSO=new_ptr(syn_out,L);
		for(S=0;	S<R[GPU_RANG-2];	S++)// �� ��������  k
		{
			pSO[S]=pS[S]-M*A[GPU_RANG-2][S];
		};
			pSO[S]=pS[S]-M;
	};
	T=0;
	__syncthreads();
	//-------------------------------------------------------  inner layer
	for(L=GPU_RANG-2; L>0; --L)
	{STP;
		if(I<R[L])
		{
			M=0.0f;
			for(S=0; S<R[L+1] ; ++S)
				M += E[P][S]* new_ptr(syn_in,pitch*(GPU_MAXRANG*(L+1)+I))[S];
			E[T][I] = M*DSigmoid(GPU_SIG,A[L][I]);
			M  = niu*E[T][I];
			L=pitch*(GPU_MAXRANG*(L-1)+I);
			pS =new_ptr(syn_in,L);
			pSO=new_ptr(syn_out,L);
			for(S=0;	S<R[GPU_RANG-2];	S++)// �� ��������  k
			{
				pSO[S]=pS[S]-M*A[GPU_RANG-2][S];
			};
				pSO[S]=pS[S]-M;
		};
		__syncthreads();
	};
};
#undef I
#undef R
#undef new_ptr
#undef STP
// ----------------------------------------------------------------
	class annealing
	{
		private:
			ANN::CUDA::UTIL::DeviceBuffer DBuf_in;
			ANN::CUDA::UTIL::DeviceBuffer DBuf_out;
			ANN::CUDA::UTIL::DeviceBuffer DBuf_err;
			ANN::Int					  FrameCount;
		// --------------------------------------------------------
			ANN::CUDA::UTIL::DeviceBuffer DBuf_Learnin;
			ANN::CUDA::UTIL::DeviceBuffer DBuf_Learnout;
			ANN::Int					  LearnFrameCount;
			RandomFieldSelector			  rfis;
		// --------------------------------------------------------
			ANN::SIGMOID				  sig;
			ANN::Int					  rang;
			ANN::Rang*					  layer_rang;
			ANN::Rang					  maxRang;
			ANN::Rang					  maxRangplusone;
			ANN::Rang					  inRang ;
			ANN::Rang					  outRang;
		// --------------------------------------------------------
			cudaChannelFormatDesc		  syn_tex_channelDesc;
			cudaArray*					  syn_tex_array;
			ANN::Bool					  syn_switch;
			ANN::Float*					  syn_ptr[2];
			ANN::Int					  syn_pitch;
			ANN::Float					  lastError;
		// --------------------------------------------------------
			ANN::Int					  SynCount;
			ANN::Int					  texSynMemSize;
			ANN::Int					  packSynMemSize;
//			ANN::Int					  texLayerSize;
			ANN::Float*					  texSynMem;
			ANN::Float*					  packSynMem;
		// --------------------------------------------------------
			__host__	Base::int_bool FillInfo(ANN::Rang _rang, ANN::Rang* lrangs,ANN::SIGMOID _sig)
			{
				BL::Log(BL::Info,"annealing::FillInfo...\n");
				//  -----------------------------------------  rangs
				sig           = _sig;
				rang          = _rang;
				layer_rang    = (ANN::Rang*)ANN::NetMemCopyMA(lrangs,sizeof(ANN::Rang)*_rang);
				maxRang       = ANN::maxLayerRang(lrangs,_rang);
				maxRangplusone= maxRang+1;
				inRang        = lrangs[0];
				outRang       = lrangs[_rang-1];
				//  ----------------------------------------- helpers
				ACC(cudaMemcpyToSymbol(GPU_SIG,            &sig           ,sizeof(ANN::SIGMOID)),"cudaMemcpyToSymbol GPU_SIG");
				ACC(cudaMemcpyToSymbol(GPU_RANG,           &_rang         ,sizeof(ANN::Rang)),"cudaMemcpyToSymbol GPU_RANG");
				ACC(cudaMemcpyToSymbol(GPU_LR,             lrangs         ,sizeof(ANN::Rang)*_rang,0),"cudaMemcpyToSymbol GPU_LR");
				ACC(cudaMemcpyToSymbol(GPU_MAXRANG,        &maxRang       ,sizeof(ANN::Rang)),"cudaMemcpyToSymbol GPU_MAXRANG");
				ACC(cudaMemcpyToSymbol(GPU_MAXRANGPLUSONE, &maxRangplusone,sizeof(ANN::Rang)),"cudaMemcpyToSymbol GPU_MAXRANGPLUSONE");
				//  ----------------------------------------- helpers
				SynCount       = ANN::Synapsis_count(lrangs,_rang);
				packSynMemSize = SynCount*sizeof(ANN::Synapsis);
				texSynMemSize  = ANN::CUDA::getTextureMemSynapseSize(lrangs,_rang)*sizeof(ANN::Synapsis);
//				texLayerSize   = maxRang*maxRangplusone;
				//  ----------------------------------------- synapsys
				texSynMem      = (ANN::Float*)ANN::NetMemMalloc(texSynMemSize);
				packSynMem     = (ANN::Float*)ANN::NetMemMalloc(packSynMemSize);

				for(int i=0;i<2;++i)ACC(cudaMallocPitch(&syn_ptr[i],
					                                &syn_pitch,
													maxRangplusone*sizeof(ANN::Float),
													rang*maxRang),"cudaMallocArray syn_ptr[i]");

					ACC(cudaMallocArray(&syn_tex_array, &syn_tex_channelDesc, maxRangplusone, rang*maxRang),"cudaMallocArray syn_tex_array");

				GPU_TEX_SYN.addressMode[0] = cudaAddressModeWrap;
				GPU_TEX_SYN.addressMode[1] = cudaAddressModeWrap;
				GPU_TEX_SYN.filterMode = cudaFilterModePoint;
				GPU_TEX_SYN.normalized = false;
				syn_switch=0;


				return true;
			};
		// --------------------------------------------------------
			PROCEDURE syn_randomToD()
			{
				for(ANN::Int i=0;i<SynCount;++i)
					packSynMem[i]=Base::UTIL::random()*2.0f-0.5f;
				syn_HtoD();
			};
		// --------------------------------------------------------
			PROCEDURE syn_HtoD()
			{
				ANN::CUDA::fillTextureMemSynapse(texSynMem,packSynMem,layer_rang,rang);

				ACC(cudaMemcpy2D	 (syn_ptr[syn_switch],
					                  syn_pitch,
									  texSynMem,
									  maxRangplusone*sizeof(ANN::Float),
									  maxRangplusone*sizeof(ANN::Float),
									  rang*maxRang,
									  cudaMemcpyHostToDevice),"annealing::syn_HtoD cudaMemcpy2D");
				ACC(cudaMemcpyToArray(syn_tex_array,
					                  0, 0, 
									  texSynMem , 
									  rang*maxRang*maxRangplusone*sizeof(ANN::Float),
									  cudaMemcpyHostToDevice),"annealing::syn_HtoD cudaMemcpyToArray");
			};
		// --------------------------------------------------------
			PROCEDURE syn_DtoH()
			{
				ACC(cudaMemcpy2D(texSynMem,
					             maxRangplusone*sizeof(ANN::Float),
								 syn_ptr[syn_switch],
								 syn_pitch,
								 maxRangplusone*sizeof(ANN::Float),
								 rang*maxRang,
								 cudaMemcpyDeviceToHost),"annealing::syn_DtoH cudaMemcpy2D");					
				ANN::CUDA::ReadTextureMemSynapse(packSynMem,texSynMem,layer_rang,rang);
			};
		// --------------------------------------------------------
			PROCEDURE syn_DtoT(int i)
			{
				if((i>=0)&&(i<2))
					ACC(cudaMemcpy2DToArray(syn_tex_array, 0, 0, syn_ptr[i  ],syn_pitch, maxRangplusone*sizeof(ANN::Float),rang*maxRang, cudaMemcpyDeviceToDevice),"syn_DtoT cudaMemcpy2DToArray");
			};
		// --------------------------------------------------------
			PROCEDURE ClearInfo()
			{
				BL::Log(BL::Info,"annealing::ClearInfo...\n");
				//  ----------------------------------------- 
				ANN::NetMemFree(layer_rang,sizeof(ANN::Rang)*rang);
				ANN::NetMemFree(texSynMem,texSynMemSize);
				ANN::NetMemFree(packSynMem,packSynMemSize);
				//  ----------------------------------------- 
				rang            = 0;
				maxRang         = 0;
				inRang          = 0;
				outRang         = 0;
				layer_rang      = nullptr;
				//  ----------------------------------------- 
				SynCount		= 0;
				texSynMemSize   = 0;
				packSynMemSize  = 0;
//				texLayerSize    = 0;
				texSynMem       = nullptr;
				packSynMem      = nullptr;
				//  ----------------------------------------- 
				ACC(cudaFreeArray(syn_tex_array),"annealing::ClearInfo::cudaFreeArray syn_tex_array");
				ACC(cudaFree(syn_ptr[0]),"annealing::ClearInfo::cudaFree syn_ptr[0]");syn_ptr[0]=nullptr;
				ACC(cudaFree(syn_ptr[1]),"annealing::ClearInfo::cudaFree syn_ptr[1]");syn_ptr[1]=nullptr;
			};
		// --------------------------------------------------------
			public:
				ANN::Rang					  getInRang(){return inRang;};
				ANN::Rang					  getOutRang(){return outRang;};
		// --------------------------------------------------------
			CONSTRUCTOR	annealing():rang(0),layer_rang(nullptr),maxRang(0),inRang(0),outRang(0),SynCount(0),FrameCount(0),LearnFrameCount(0),lastError(-1),syn_switch(0),
				texSynMemSize(0),packSynMemSize(0),texSynMem(nullptr),packSynMem(nullptr)
			{
				syn_tex_channelDesc=cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
				syn_ptr[0]=nullptr;
				syn_ptr[1]=nullptr;
			};
		// --------------------------------------------------------
		__host__ ANN::Bool setERRD(ANN::Float* in,ANN::Float* out, ANN::Int Rin, ANN::Int Rout, ANN::Int frames )
		{
			if(in && out && Rin==inRang && Rout==outRang && frames)
			{
				DBuf_in .ReserveSet(in ,sizeof(ANN::Float)*frames*Rin );
				DBuf_out.ReserveSet(out,sizeof(ANN::Float)*frames*Rout);
				DBuf_err.Reserve   (    sizeof(ANN::Float)*frames     );
				FrameCount=frames;
				ANN::Float * 
					
				tmp =DBuf_in.use <ANN::Float>();ACC(cudaMemcpyToSymbol(GPU_INPUT      ,&tmp       ,sizeof(ANN::Float*),0),"annealing::setLD: cudaMemcpyToSymbol GPU_INPUT");
				tmp =DBuf_out.use<ANN::Float>();ACC(cudaMemcpyToSymbol(GPU_OUTPUT     ,&tmp       ,sizeof(ANN::Float*),0),"annealing::setLD: cudaMemcpyToSymbol GPU_OUTPUT");
				tmp =DBuf_err.use<ANN::Float>();ACC(cudaMemcpyToSymbol(GPU_OUTERR     ,&tmp       ,sizeof(ANN::Float*),0),"annealing::setLD: cudaMemcpyToSymbol GPU_OUTERR");
												ACC(cudaMemcpyToSymbol(GPU_FRAMECOUNT ,&FrameCount,sizeof(ANN::Int   ),0),"annealing::setLD: cudaMemcpyToSymbol GPU_MAXRANG");
				return true;
			};
			return false;
		};
		// --------------------------------------------------------
		__host__ ANN::Bool setLD(ANN::Float* in,ANN::Float* out, ANN::Int Rin, ANN::Int Rout, ANN::Int frames )
		{
			if(in && out && Rin==inRang && Rout==outRang && frames)
			{
				DBuf_Learnin .ReserveSet(in ,sizeof(ANN::Float)*frames*Rin );
				DBuf_Learnout.ReserveSet(out,sizeof(ANN::Float)*frames*Rout);
				rfis.resize(frames);
				LearnFrameCount=frames;
				lastError=-1;
				ANN::Float * 
				tmp =DBuf_Learnin.use <ANN::Float>();ACC(cudaMemcpyToSymbol(GPU_LEARNINPUT      ,&tmp       ,sizeof(ANN::Float*),0),"annealing::setLD: cudaMemcpyToSymbol GPU_LEARNINPUT");
				tmp =DBuf_Learnout.use<ANN::Float>();ACC(cudaMemcpyToSymbol(GPU_LEARNOUTPUT     ,&tmp       ,sizeof(ANN::Float*),0),"annealing::setLD: cudaMemcpyToSymbol GPU_LEARNOUTPUT");
				return true;
			};
			return false;
		};
		// --------------------------------------------------------
		#define GT(v)  UINT  v  = timeGetTime();
		__host__ ANN::Float getERROR(ANN::Int var)
		{
			BL::Log(BL::Info,"annealing::getERROR....\n");
			if(FrameCount)
			{
				GT(TimeA);//--------------------------------------------------
				
				ANN::Float ret=0.0f;
				ANN::CUDA::UTIL::DeviceBuffer dret(sizeof(ANN::Float));
				dim3 gCE(FrameCount, 1, 1);
				dim3 bCE(maxRang, 1, 1);
				
				syn_DtoT(var);
				ACC(cudaBindTextureToArray(GPU_TEX_SYN, syn_tex_array, syn_tex_channelDesc),"annealing::getERROR: cudaBindTextureToArray syn_tex_array");
				DBuf_err.Clear();

				if(FrameCount>0x7fff)
				{
					gCE.x=0x7fff;
					gCE.y=(FrameCount / 0x7fff)+1;
				};
				
				KRNL_annealing_getERROR<<<gCE,bCE>>>();
				ACC(cudaUnbindTexture(GPU_TEX_SYN),"annealing::getERROR: cudaUnbindTexture");

				GT(TimeB);//--------------------------------------------------

				bCE.x=1024;
				bCE.y=11;
				if(FrameCount<MAX_SHARED_ARRAY)
					bCE.y=(FrameCount/1024)+1;
				KRNL_reduceError<<<1,bCE>>>(dret.use<ANN::Float>(),DBuf_err.use<ANN::Float>());
				dret.get(&ret,sizeof(ANN::Float));

				GT(TimeC);//--------------------------------------------------

				BL::Log(BL::Info," ... finished(%d,%d):%f\n",TimeB-TimeA,TimeC-TimeB,ret);
				return ret;
			}else
				BL::Log(BL::Info," ... error no input data.\n");
			return -1;
		};
		#undef GT
		// --------------------------------------------------------
		PROCEDURE LearnSingle(ANN::Int  frame,ANN::Int var,ANN::Float niu)
		{
			if(frame<LearnFrameCount)
			{
				dim3 bCE(maxRang, 1, 1);
				LearnFrame<<<1,bCE>>>(frame,syn_pitch,(var==0)?syn_ptr[0]:syn_ptr[1]
													 ,(var==0)?syn_ptr[1]:syn_ptr[0],niu);
			};
		};
		// --------------------------------------------------------
		ANN::Float getPUblERROR()
		{
			if(lastError<0)
				lastError=getERROR(syn_switch);
			return lastError;
		};
		// --------------------------------------------------------
		PROCEDURE LearnStep(ANN::Int  frame,ANN::Float niu,ANN::Float t)
		{
			if(lastError<0)
				lastError=getERROR(syn_switch);
			ANN::Int nswitch=(syn_switch+1) &0x01;

			LearnSingle(frame,syn_switch,niu);
			ANN::Float f=getERROR(nswitch);
			if(f<lastError)
			{
				lastError=f;
				syn_switch=nswitch;
			}
			else
			{
				ANN::Float h = 1.0f / (1.0f + exp(-(lastError-f) / t));
				if(Base::UTIL::random()<h)
				{
					lastError=f;
					syn_switch=nswitch;
				};
			};
		};
		// --------------------------------------------------------
		PROCEDURE optimize(ANN::Float To, 
						   ANN::Float Tend, 
						   ANN::Float dec,
						   ANN::Float NiuBase,
						   ANN::Int   maxk
						   )
		{
			ANN::Float T = To;
			ANN::Float Trange =To-Tend;
			ANN::Int i = 1;
			ANN::Int N = 1;
			Base::int_mem fid;
			while (T > Tend && i<maxk)
			{
				while(!rfis.getNext(fid))
					rfis.clear();

				LearnStep(fid,NiuBase*((T-Tend)/Trange),T);
				T = To * exp(-dec * pow(i, (ANN::Float)(1) / (2*(ANN::Float)N)));
				++i;
			};
		};
		// --------------------------------------------------------
		static annealing*	create(ANN::Rang _rang, ANN::Rang* lrangs,ANN::SIGMOID sig)
		{
			if (_rang<=MAX_LAYERS)
			{
				if(ANN::ChkRangs(lrangs,_rang))
				{
					if(ANN::maxLayerRang(lrangs,_rang)<LAYERMAXRANG)
					{
						if(annealing* f= new annealing())
						{
							if(f->FillInfo(_rang,lrangs,sig))
							{
								return f;
							}else 	BL::Log(BL::Info,"ANN::GPU::annealing::create: FillInfo fail\n");
							delete f;
						}else 	BL::Log(BL::Info,"ANN::GPU::annealing::create: Memory allocation fail\n");
					}else 	BL::Log(BL::Info,"ANN::GPU::annealing::create: MAXLAYERRANG fail\n");
				}else 	BL::Log(BL::Info,"ANN::GPU::annealing::create: ChkRangs fail\n");
			}else 	BL::Log(BL::Info,"ANN::GPU::annealing::create: MAX_LAYERS fail\n");
			return nullptr;
		};
		// --------------------------------------------------------
		static annealing*   Load(Base::FS::File* src)
		{
			if(annealing* ret= new annealing())
			{
				if(ANN::NetStruct* ns=ANN::LoadNetStruct(src,nullptr))
				{
					if(!ret->FillInfo(ns->rang,ns->layer_rang,ns->sigmoid))
					{
						delete ret;
						ret=nullptr;
					};
					delete ns;
					return ret;
				};
				delete ret;
			};
			return nullptr;
		};
		// --------------------------------------------------------
		Base::int_bool Save(Base::FS::File* dst)
		{
			if(dst)
			{
				syn_DtoH();
				ANN::Flags f=0;
				dst->BlockWrite(&sig,sizeof(ANN::SIGMOID));
				dst->BlockWrite(&f,sizeof(ANN::Flags));
				dst->BlockWrite(&rang,sizeof(ANN::Rang));
				dst->BlockWrite(layer_rang,sizeof(ANN::Rang)*rang);
				dst->BlockWrite(packSynMem,sizeof(ANN::Synapsis)*SynCount);
				return true;
			};
			return false;
		};
		// --------------------------------------------------------
	};
// ----------------------------------------------------------------
namespace ANN
{
	// ----------------------------------------------------
	namespace GPU
	{
	namespace ANNEALING
	{
		// ------------------------------------------------
		annealing* net;
		PROCEDURE init()
		{
			net=nullptr;
		};
		PROCEDURE done()
		{
			if(net) delete net;
			net=nullptr;
		};
		// ------------------------------------------------
		ANN::Bool create(ANN::Rang _rang, ANN::Rang* lrangs,ANN::SIGMOID sig)
		{
			if(net)
			{
				BL::Log(BL::Info,"ANN::GPU::annealing::deleting preview annealing\n");
				done();
			};
			net=annealing::create(_rang,lrangs,sig);
			BL::Log(BL::Info,"ANN::GPU::annealing::Create result: %d\n",net!=nullptr);
			return (net)?true:false;
		};
		// ------------------------------------------------
		Base::int_bool Save(Base::FS::File* dst)
		{
			return (net)?net->Save(dst):false;
		};
		// ------------------------------------------------
		Base::int_bool Load(Base::FS::File* src)
		{
			if(src)
			{
				done();
				net=annealing::Load(src);
				return (net!=nullptr);
			};
			return false;
		};
		// ------------------------------------------------
		ANN::Rang       getInRang()
		{
			 return (net)?net->getInRang():0;
		};
		// ------------------------------------------------
		ANN::Rang       getOutRang()
		{
			 return (net)?net->getOutRang():0;
		};
		// ------------------------------------------------
		PROCEDURE optimize(ANN::Float To, 
						   ANN::Float Tend, 
						   ANN::Float dec,
						   ANN::Float NiuBase,
						   ANN::Int   maxk
						   )
		{
			if(net) net->optimize(To,Tend,dec,NiuBase,maxk);
		};
		// ------------------------------------------------
		ANN::Float      getError()
		{
			if(net) return net->getPUblERROR();
			return -1;
		};
		// ------------------------------------------------
		ANN::Bool FillLearnData(ANN::Float *_in,ANN::Float *_out,ANN::Int _inR,ANN::Int _outR,ANN::Int _FrmCnt)
		{
			if(net && _in &&_out && _inR && _outR && _FrmCnt)
				if(getInRang()==_inR && getOutRang()==_outR)
					return net->setLD(_in,_out,_inR,_outR,_FrmCnt);
			return false;
		};
		// ------------------------------------------------
		ANN::Bool FillErrorData(ANN::Float *_in,ANN::Float *_out,ANN::Int _inR,ANN::Int _outR,ANN::Int _FrmCnt)
		{
			if(net && _in &&_out && _inR && _outR && _FrmCnt)
				if(getInRang()==_inR && getOutRang()==_outR)
					return net->setERRD(_in,_out,_inR,_outR,_FrmCnt);
			return false;
		};
		// ------------------------------------------------

		// ------------------------------------------------
	};};
	// ----------------------------------------------------
};
