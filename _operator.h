// ==================================================================================
#pragma once
// ==================================================================================
#include "lcommonB.h"
#include <MW.h>
#include "_expression.h"
#include "_fenvdecl.h"
// ==================================================================================
namespace Parcing
{
	// ==============================================================================
	class TCharParce;
	// ==============================================================================
	namespace CItem
	{
		// ==========================================================================
		#define			CJ_OPR_Null			0x0000
		#define			CJ_OPR_return		0x0001
		#define			CJ_OPR_exit			0x0002
		#define			CJ_OPR_break		0x0003
		#define			CJ_OPR_continue		0x0004
		#define			CJ_OPR_Expr			0x0005
		#define			CJ_OPR_Block		0x0006
		#define			CJ_OPR_If			0x0007
		#define			CJ_OPR_While		0x0008
		#define			CJ_OPR_For			0x0009
		#define			CJ_OPR_Do		    0x000a
		#define			CJ_OPR_switch		0x000b
		#define			CJ_OPR_Foreach		0x000c
		#define			CJ_OPR_Try			0x000d
		#define			CJ_OPR_Throw		0x000e
		#define			CJ_OPR_Syncro		0x000f
		#define			CJ_OPR_yield		0x0010
		#define			CJ_OPR_var			0x0011
//		#define			CJ_OPR_with			0x0012
		#define			CJ_OPR_MASK			0x00ff
		// ==========================================================================
		#define			CJ_OPR_STRUC_ALT	0x1000
		//--------------------------------------
		#define			CJ_OPR_ELSE_Def		0x0000
			//	elsif
		#define        CJ_OPR_ELSE_ALT1		0x2000
			//	elseif
		#define        CJ_OPR_ELSE_ALT2		0x4000
			//	else mask
		#define        CJ_OPR_ELSE_ALTMASK	0x6000
		//--------------------------------------
		#define        CJ_OPR_BLK_ONCE		0x2000
		#define        CJ_OPR_BLK_ONCEPASS	0x4000
		// ==========================================================================
										// ����� ��� ������ �������� ����������
		#define        CJ_OPR_SUB_else		0xff01
		#define        CJ_OPR_SUB_elsif		0xff02
		#define        CJ_OPR_SUB_elseif	0xff03

		#define        CJ_OPR_SUB_case		0xff04
		#define        CJ_OPR_SUB_catch		0xff05
		#define        CJ_OPR_SUB_default	0xff06

		// else for foreach operator
		#define        CJ_OPR_SUB_elsf		0xff07

		// ==========================================================================
		#define        CJ_OPRN_return		"return"
		#define        CJ_OPRNS_return		6
		#define        CJ_OPRN_eval			"eval"
		#define        CJ_OPRNS_eval		4
		#define        CJ_OPRN_exit			"exit"
		#define        CJ_OPRNS_exit		4
		#define        CJ_OPRN_break		"break"
		#define        CJ_OPRNS_break		5
		#define        CJ_OPRN_continue		"continue"
		#define        CJ_OPRNS_continue	8
		#define        CJ_OPRN_yield		"yield"
		#define        CJ_OPRNS_yield		5
		#define        CJ_OPRN_with			"with"
		#define        CJ_OPRNS_with		4
		
		



		#define        CJ_OPRN_try			"try"
		#define        CJ_OPRNS_try			3
		#define        CJ_OPRN_throw		"throw"
		#define        CJ_OPRNS_throw		5

		#define        CJ_OPRN_var			"let"
		#define        CJ_OPRNS_var			3

		#define        CJ_OPRN_If			"if"
		#define        CJ_OPRNS_If			2
		#define        CJ_OPRN_SUB_else		"else"
		#define        CJ_OPRNS_SUB_else	4
		#define        CJ_OPRN_SUB_elseif	"elseif"
		#define        CJ_OPRNS_SUB_elseif	6
		#define        CJ_OPRN_SUB_elsif	"elsif"
		#define        CJ_OPRNS_SUB_elsif	5
		#define        CJ_OPRN_SUB_catch	"catch"
		#define        CJ_OPRNS_SUB_catch	5

		#define        CJ_OPRN_SUB_elsf		"elsf"
		#define        CJ_OPRNS_SUB_elsf	4

		#define        CJ_OPRN_While		"while"
		#define        CJ_OPRNS_While		5

		#define        CJ_OPRN_do			"do"
		#define        CJ_OPRNS_do  		2


		#define        CJ_OPRN_For			"for"
		#define        CJ_OPRNS_For			3
		#define        CJ_OPRN_Foreach		"foreach"
		#define        CJ_OPRNS_Foreach		7

		#define        CJ_OPRN_syncro		"syncro"
		#define        CJ_OPRNS_syncro		6

		#define        CJ_OPRN_using		"using"
		#define        CJ_OPRNS_using		5

		#define        CJ_OPRN_switch		"switch"
		#define        CJ_OPRNS_switch		6
		#define        CJ_OPRN_SUB_case		"case"
		#define        CJ_OPRNS_SUB_case	4

		#define        CJ_OPRN_SUB_default	"default"
		#define        CJ_OPRNS_SUB_default 7


		#define        CJ_OPRN_BLK_ONCE	    "#once"
		#define        CJ_OPRNS_BLK_ONCE    5
		// ==========================================================================
		typedef	Base::int_16u			operator_type;
		// ==========================================================================
		class header_operator
		{
			protected:
			operator_type		type;
			BreakPointInfo*		_BreakPointInfo;
			CONSTRUCTOR			header_operator();
			DESTRUCTOR			~header_operator();
		public:
			BreakPointInfo*		getBreakPointInfo();
			PROCEDURE			keep_as_text(TBasket &b);
			operator_type		TYPE();
		};
		// ==========================================================================
		DECLARE_CLASSP(_operator,header_operator,MINPOOLSIZE,Base::MW::CItemPool)
			friend class _operator_block;
			friend class _operator_while;
			friend class _operator_for;
			friend class _operator_foreach;
			friend class _function_prep;
			CODEITEMS_FRIENDS;
			// ======================================================================
		private:
					_expression*					_data;
			// ======================================================================
					CONSTRUCTOR						_operator();
					DESTRUCTOR						~_operator();
					PROCEDURE						_detach();
					PROCEDURE						keep_as_text(TBasket &b); // ��� �������� � ����� text_keepers.cpp
			// ======================================================================
		public:
			static  PROCEDURE						_clear				(_operator* &o);
			static  PROCEDURE						ResetFC				(_operator* o);
			static  Base::int_bool					HaveSyncBlockMode	(_operator* o);
			static  PROCEDURE						keep_as_text		(TBasket &b,_operator* o); // ��� �������� � ����� text_keepers.cpp
			static  PROCEDURE						keep_as_textSUB		(TBasket &b,_operator* o);
			static  PROCEDURE						build_lexem			(TCharParce &_p, _lexem     &_l,Base::int_bool no_expr=false);
			static  _operator*						clone				(_operator* src);  // ��� �������� � ����� obj_clones.cpp
			static  _operator*						cloneB				(_operator* src);  // ��� �������� � ����� obj_clones.cpp
			static  PROCEDURE						ApplyLetFunArgs		(_operator* src,_headArgList * ha);
		};
		// ==========================================================================
		#define _OPRBAPtr_inc             0x10
		#define _OPRBAPtr_incpercdef      0.5
		// ==========================================================================
		DECLARE_CLASSP(_operator_block,header_operator,MINPOOLSIZE,Base::MW::CItemPool)
			friend class _operator;

		/*			friend class _operator_try;
			friend class _operator_syncro;
			friend class _function;
			friend class _vglink;
			friend class TCharParce;
			friend class _operator_switch;
*/
			CODEITEMS_FRIENDS;
			// ======================================================================
					_operator**						DATA  ;     // ��������� �� ������ ���������
					Base::int_mem					ACOUNT;     // ���������� �������� ���������
					Base::int_mem					TCOUNT;     // ���������� ��������� ��� ����� �������� �����
			// ======================================================================
					CONSTRUCTOR						_operator_block(Base::int_32 ic=_OPRBAPtr_inc);
					DESTRUCTOR						~_operator_block()  ;
			// ======================================================================
					PROCEDURE						_normalize();
					Base::int_bool					_request_volume(Base::int_mem c=1);
					Base::int_bool					_add(Base::_pvoid i) ;
					Base::int_mem					count();
			// ======================================================================
					Base::int_bool					HaveSyncBlockMode();
			// ======================================================================
		public:
					PROCEDURE						Onced				();
					PROCEDURE						ApplyLetFunArgs		(_headArgList * ha);
					PROCEDURE						keep_as_text(TBasket &b); // ��� �������� � ����� text_keepers.cpp
					PROCEDURE						keep_as_textB(TBasket &b);
			static  PROCEDURE						build_lexem	(TCharParce &_p, _lexem     &_l,Base::int_bool once=false);
			static	PROCEDURE						build_lexemB(TCharParce &_p,_lexem     &_l);
			static  _operator_block*				clone(_operator_block* src);  // ��� �������� � ����� obj_clones.cpp
		};
		// ==========================================================================
		#define CJ_OPRSYNC_OBJ				0x01
		DECLARE_CLASSP(_operator_syncro,header_operator,MINPOOLSIZE,Base::MW::CItemPool)
			friend class _operator;
			CODEITEMS_FRIENDS;
			// ======================================================================
					Base::_pvoid					synObj;
					_operator*						body;
					Base::int_8u					flags;
			// ======================================================================
					CONSTRUCTOR						_operator_syncro();
					DESTRUCTOR						~_operator_syncro();
			// ======================================================================
					PROCEDURE						keep_as_text(TBasket &b);	 // ��� �������� � ����� text_keepers.cpp
					Base::int_bool					HaveSyncBlockMode();
			// ======================================================================
		public:
			static  PROCEDURE						build_lexem(TCharParce &_p,_lexem     &_l,_operator* &_b,Base::int_bool funcmode=0);
			static  _operator_syncro*				clone(_operator_syncro* src);  // ��� �������� � ����� obj_clones.cpp
		};
		// ==========================================================================
		DECLARE_CLASSP(_operator_if,header_operator,MINPOOLSIZE,Base::MW::CItemPool)
			friend class _operator;
			CODEITEMS_FRIENDS;
			// ======================================================================
					_expression*					expr;
					_operator*						body;
					_operator*						elsbody;
			// ======================================================================
					CONSTRUCTOR						_operator_if();
					DESTRUCTOR						~_operator_if();
			// ======================================================================
					PROCEDURE						keep_as_text(TBasket &b,Base::int_8u elsif=0); // ��� �������� � ����� text_keepers.cpp
					Base::int_bool					HaveSyncBlockMode();
			// ======================================================================
		public:
			static	PROCEDURE						build_lexem(TCharParce &_p,_lexem &_l, _operator* &_b,Base::int_8u elsif=0);
			static  _operator_if*					clone(_operator_if* src);
		};
		// ==========================================================================
		DECLARE_CLASSP(_operator_var,header_operator,MINPOOLSIZE,Base::MW::CItemPool)
			friend class _operator;
			CODEITEMS_FRIENDS;
			// ======================================================================
					_fenvdecl*						_decls;
			// ======================================================================
					CONSTRUCTOR						_operator_var();
					DESTRUCTOR						~_operator_var();
			// ======================================================================
					PROCEDURE						keep_as_text(TBasket &b); // ��� �������� � ����� text_keepers.cpp
			// ======================================================================
		public:
			static	PROCEDURE						build_lexem(TCharParce &_p,_lexem &_l);
			static  _operator_var*					clone(_operator_var* src);
		};

		// ==========================================================================
		DECLARE_CLASSP(_operator_for,header_operator,MINPOOLSIZE,Base::MW::CItemPool)
			friend class _operator;
			CODEITEMS_FRIENDS;
			// ======================================================================
					_operator*						init;
					_expression*					expr;
					_operator*						turn;
					_operator*						body;
			// ======================================================================
					CONSTRUCTOR						_operator_for();
					DESTRUCTOR						~_operator_for();
			// ======================================================================
					PROCEDURE						keep_as_text(TBasket &b);
					Base::int_bool					HaveSyncBlockMode();
			// ======================================================================
		public:
			static	PROCEDURE						build_lexem(TCharParce &_p, _lexem     &_l, _operator* &_b);
			static  _operator_for*					clone(_operator_for* src);
		};
		// ==========================================================================
		DECLARE_CLASSP(_operator_while,header_operator,MINPOOLSIZE,Base::MW::CItemPool)
			friend class _operator;
			CODEITEMS_FRIENDS;
			// ======================================================================
					_expression*					expr;
					_operator*						body;
			// ======================================================================
					CONSTRUCTOR						_operator_while(Base::int_bool DoMode=false);
					DESTRUCTOR						~_operator_while();
			// ======================================================================
					PROCEDURE						keep_as_text(TBasket &b);
					Base::int_bool					HaveSyncBlockMode();
			// ======================================================================
		public:
			static	PROCEDURE						build_lexem(TCharParce &_p,_lexem     &_l,_operator* &_b);
			static	PROCEDURE						build_lexem_Do(TCharParce &_p,_lexem     &_l,_operator* &_b);
			static  _operator_while*				clone(_operator_while* src);
		};
		// ==========================================================================
		DECLARE_CLASSP(_operator_foreach,header_operator,MINPOOLSIZE,Base::MW::CItemPool)
			friend class _operator;
			CODEITEMS_FRIENDS;
			// ======================================================================
					_headArgList*					storage;
					_expression*					expr;
					_operator*						body;
					_operator*						elsbody;
			// ======================================================================
					CONSTRUCTOR						_operator_foreach();
					DESTRUCTOR						~_operator_foreach();
			// ======================================================================
					PROCEDURE						keep_as_text(TBasket &b); // ��� �������� � ����� text_keepers.cpp
					Base::int_bool					HaveSyncBlockMode();
			// ======================================================================
		public:
					PROCEDURE						ApplyLetFunArgs		(_headArgList * ha);
			static	PROCEDURE						build_lexem(TCharParce &_p, _lexem &_l, _operator* &_b);
			static  _operator_foreach*				clone(_operator_foreach* src);  // ��� �������� � ����� obj_clones.cpp
		};		
		// ==========================================================================
		DECLARE_CLASSP(_operator_try,header_operator,MINPOOLSIZE,Base::MW::CItemPool)
			friend class _operator;
			CODEITEMS_FRIENDS;
			// ======================================================================
					_ident							as;
					_operator*						body;
					_operator*						_catch;
			// ======================================================================
					CONSTRUCTOR						_operator_try();
					DESTRUCTOR						~_operator_try();
			// ======================================================================
					PROCEDURE						keep_as_text(TBasket &b);
					Base::int_bool					HaveSyncBlockMode();
			// ======================================================================
		public:
			static	PROCEDURE						build_lexem(TCharParce &_p, _lexem &_l);
			static  _operator_try*					clone(_operator_try* src);
		};
		// ==========================================================================
		DECLARE_CLASSP(_operator_switch,header_operator,MINPOOLSIZE,Base::MW::CItemPool)
			friend class _operator;
			CODEITEMS_FRIENDS;
			// ======================================================================
		public:
					DECLARE_CLASS(switch_case,MINPOOLSIZE,Base::MW::CItemPool)
					public:
								GCObjRef			ident;
								_operator*			oper;
								CONSTRUCTOR			switch_case();
								DESTRUCTOR			~switch_case();
						static  switch_case*		clone(switch_case* src);
					};
			// ======================================================================
		private:
					switch_case**					DATA;     // ��������� �� ������ ���������
					Base::int_mem					ACOUNT;     // ���������� �������� ���������
					Base::int_mem					TCOUNT;     // ���������� ��������� ��� ����� �������� �����
					_operator*						_default;
					_expression*					_arg;
					_expression::_EXPRSN_Flags		mode;
			// ======================================================================
					PROCEDURE						_normalize();
					Base::int_bool					_request_volume(Base::int_mem c=1);
					Base::int_bool					_add(switch_case* i) ;
					Base::int_mem					count();
			// ======================================================================
   					CONSTRUCTOR						_operator_switch(Base::int_32 ic=_OPRBAPtr_inc);
			// ======================================================================
					Base::int_bool					HaveSyncBlockMode();
					PROCEDURE						ApplyLetFunArgs		(_headArgList * ha);
			// ======================================================================
			static	switch_case*					handle_case(TCharParce &_p, _lexem  &_l,Base::int_bool skip_ww=0);
			// ======================================================================
			public:
					DESTRUCTOR						~_operator_switch();
			// ======================================================================
					PROCEDURE						keep_as_text(TBasket &b);
			// ======================================================================
			static	PROCEDURE						build_lexem(TCharParce &_p, _lexem     &_l);
			static  _operator_switch*				clone(_operator_switch* src);
		};
		// ==========================================================================
	};	//	CItem
	// ==============================================================================
};	//	Parcing
// ==================================================================================
