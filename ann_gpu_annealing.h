
#include "ann_base.h"


namespace ANN
{
	// ----------------------------------------------------
	namespace GPU
	{
	namespace ANNEALING
	{
		// ------------------------------------------------
		PROCEDURE init();
		PROCEDURE done();
		// ------------------------------------------------
		ANN::Bool create(ANN::Rang _rang, ANN::Rang* lrangs,ANN::SIGMOID sig);
		// ------------------------------------------------
		Base::int_bool Save(Base::FS::File* dst);
		Base::int_bool Load(Base::FS::File* src);
		ANN::Rang       getInRang();
		ANN::Rang       getOutRang();
		ANN::Float      getError();
		// ------------------------------------------------
		ANN::Bool FillLearnData(ANN::Float *_in,ANN::Float *_out,ANN::Int _inR,ANN::Int _outR,ANN::Int _FrmCnt);
		ANN::Bool FillErrorData(ANN::Float *_in,ANN::Float *_out,ANN::Int _inR,ANN::Int _outR,ANN::Int _FrmCnt);
		// ------------------------------------------------
		PROCEDURE optimize(ANN::Float To, 
						   ANN::Float Tend, 
						   ANN::Float dec,
						   ANN::Float NiuBase,
						   ANN::Int   maxk
						   );
		// ------------------------------------------------
	};};
	// ----------------------------------------------------
};
// --------------------------------------------------------
