//-------------------------------------------------------
#include "DynLib.h"
#include "_cpchar.h"
//-------------------------------------------------------
CONSTRUCTOR OS::DynamicLibrary::DynamicLibrary(Base::pchar_8 path):inst(nullptr),_path(nullptr)
{
	if(inst=LoadLibrary(path))
		_path=Base::_cpchar::StrCopy(path);
};
//-------------------------------------------------------
DESTRUCTOR  OS::DynamicLibrary::~DynamicLibrary()
{
	FreeLibrary(inst);
	inst=0;
	Base::_cpchar::StrFree(_path);
	_path=nullptr;
};
//-------------------------------------------------------
FARPROC OS::DynamicLibrary::GetProcAddress(Base::pchar_8 name)
{
	return ::GetProcAddress(inst,name);
};
//-------------------------------------------------------
const Base::pchar_8 OS::DynamicLibrary::GetPath()
{
	return _path;
};
//-------------------------------------------------------
Base::int_bool OS::DynamicLibrary::IsLoaded()
{
	return inst!=nullptr;
};
//-------------------------------------------------------