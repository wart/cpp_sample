#include "MP.h"
#include "utils.h"
#include "Logger.h"
//-------------------------------------------------------
namespace MP
{
	//  =================================================
	CONSTRUCTOR	SystemHandle::SystemHandle(HANDLE _hnd) :hnd(_hnd)
	{
			
	};
	//  -------------------------------------------------
	Base::int_bool	SystemHandle::WaitHandle(Base::int_32u time)
	{
		return WAIT_OBJECT_0 == WaitForMultipleObjects(1, &hnd, 0, time);
	};
	//  -------------------------------------------------
	PROCEDURE SystemHandle::ClearHandle()
	{
		if(hnd)
		{
			::CloseHandle(hnd);
			hnd=0;
		};
	};
	//  -------------------------------------------------
	DESTRUCTOR	SystemHandle::~SystemHandle()
	{
		//  ClearHandle();
	};
	//  =================================================
	DWORD	__stdcall	Thread::EnterPoint(Thread * th)
	{
		if(th)
		{
			if(SUCCEEDED(CoInitializeEx(NULL, COINIT_MULTITHREADED)))
			{
				Base::UTIL::RandomInit();
				th->OnThreadBegin();
				th->code();
				th->OnThreadEnd();
//				SetErrorInfo(0, NULL);
				CoUninitialize();
			}else
				BL::Log(BL::Critical,"MP::Thread::EnterPoint():Error CoInitializeEx()\n");
		};
		return 0;
	};
	//  -------------------------------------------------
	PROCEDURE		Thread::RunInPlace()
	{
		EnterPoint(this);
	};
	//  -------------------------------------------------
	PROCEDURE		Thread::OnThreadBegin()
	{
		MPTHREADSTATE=MPThread_Running;
	};
	//  -------------------------------------------------
	PROCEDURE		Thread::OnThreadEnd()
	{
		MPTHREADSTATE=MPThread_Finished;
	};
	//  -------------------------------------------------
	Base::int_bool	Thread::IsRunning()
	{
		return MPTHREADSTATE==MPThread_Running;
	};
	//  -------------------------------------------------
	Base::int_bool	Thread::IsFinished()
	{
		return MPTHREADSTATE==MPThread_Finished;
	};
	//  -------------------------------------------------
	Base::int_bool	Thread::JustCreated()
	{
		return MPTHREADSTATE==MPThread_Created;
	};
	//  -------------------------------------------------
	CONSTRUCTOR		Thread::Thread():MPTHREADSTATE(MPThread_Created),id(0)
	{
		
	};
	//  -------------------------------------------------
	DESTRUCTOR		Thread::~Thread()
	{
	};
	//  -------------------------------------------------
	DWORD				Thread::GetId()
	{
		return id;
	};
	//  -------------------------------------------------
	HANDLE				Thread::GetHandle()
	{
		return hnd;
	};
	//  -------------------------------------------------
	DWORD				Thread::GetCurId()
	{
		return GetCurrentThreadId();
	};
	//  -------------------------------------------------
	HANDLE				Thread::GetCurHandle()
	{
		return GetCurrentThread();
	};
	//  -------------------------------------------------
	PROCEDURE           Thread::LinkWithMe()
	{
		if(! IsRunning() && hnd==0 && id==0)
		{
			id=GetCurId();
			hnd=GetCurHandle();
		};
	};
	//  -------------------------------------------------
	PROCEDURE			Thread::Start(bool inplace)
	{
		if(! IsRunning())
		{
			ClearHandle();
			if (inplace)
			{
				LinkWithMe();
				SetThreadPriority(hnd, THREAD_PRIORITY_BELOW_NORMAL);
				RunInPlace();
			}
			else
			{
				hnd = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)EnterPoint, (LPVOID)this, 0, &id);
				SetThreadPriority(hnd, THREAD_PRIORITY_BELOW_NORMAL);
			};
		};
	};
	//  -------------------------------------------------
	DWORD				Thread::Suspend()
	{
		return SuspendThread(hnd);
	};
	//  -------------------------------------------------
	DWORD				Thread::Resume()
	{
		return ResumeThread(hnd);
	};
	//  -------------------------------------------------
	PROCEDURE			Thread::Sleep(DWORD msec)
	{
		::Sleep(msec);
	};
	//  -------------------------------------------------
	int				Thread::GetPriority()
	{
		return GetThreadPriority(hnd);
	};
	//  -------------------------------------------------
	PROCEDURE		Thread::SetPriority(int priority)
	{
		SetThreadPriority(hnd,priority);
	};
	//  -------------------------------------------------
	Base::int_bool	Thread::Wait(Base::int_32u time)
	{
		return WAIT_OBJECT_0 == WaitForMultipleObjects(1, &hnd, 0, time);//  MAXIMUM_WAIT_OBJECTS 
	};
	//  -------------------------------------------------
	//  =================================================
	//  -------------------------------------------------
	Base::int_bool Mutex::TryEnter()
	{
		return TryEnterCriticalSection(&cs);
	};
	//---------------------------------------------------
	CONSTRUCTOR		MutexLock::MutexLock(){};
	//---------------------------------------------------
	CONSTRUCTOR		MutexLock::MutexLock(Mutex & ref)
	{
		if(mutex= &ref) mutex->enter();
	};
	//===================================================
	CONSTRUCTOR		Event::Event(Base::int_bool manual,Base::int_bool state)
	{
		if(!(hnd=CreateEvent(nullptr, manual, state, nullptr)))
			BL::Log(BL::Critical,"MP::Event not created\n");
	};
	//---------------------------------------------------
	DESTRUCTOR		Event::~Event()
	{
		CloseHandle(hnd);
	};
	//---------------------------------------------------
	Base::int_bool	Event::Wait(Base::int_32u time)
	{
		return WAIT_OBJECT_0==WaitForSingleObject(hnd,time);//  MAXIMUM_WAIT_OBJECTS 
	};
	//---------------------------------------------------
	PROCEDURE		Event::Set()
	{
		SetEvent(hnd);
	};
	//---------------------------------------------------
	PROCEDURE		Event::Reset()
	{
		ResetEvent(hnd);
	};
	//---------------------------------------------------
	PROCEDURE		Event::Pulse()
	{
		PulseEvent(hnd);
	};
	//---------------------------------------------------
	Base::int_32u	Event::WaitManyEvent(Event** ev, Base::int_8u cnt,Base::int_32u time,Base::int_bool waitall)
	{
		if( cnt && ev && cnt<=MAXIMUM_WAIT_OBJECTS)
		{
			HANDLE evs[MAXIMUM_WAIT_OBJECTS];
			for (Base::int_8u i=0;i<cnt;i++)
				evs[i]=ev[i]?ev[i]->hnd:0;
			return WaitForMultipleObjects(cnt,evs,waitall,time);
		};
		return STATUS_INVALID_PARAMETER;
	};
	//===================================================
};
//-------------------------------------------------------
INIT_OBJ_COUNTER(MP::MutexLock);
INIT_OBJ_COUNTER(MP::Mutex);
INIT_OBJ_COUNTER(MP::Event);






/*
	DWORD	Thread::WaitFor(HANDLE hnd,DWORD msec)
	{
		return	WaitForSingleObject (hnd, msec);
	};
/**/



/*  
	HANDLE			hWaits[2];
	hWaits[0]		= hKillEvent;
	hWaits[1]		= hSocketEvent;
	while(true)
	{
		// Wait for either the kill event or  an accept event :
		DWORD dwRet = WSAWaitForMultipleEvents(2, hWaits, FALSE, INFINITE, FALSE);


*/

